﻿using System;
using System.Diagnostics;
using affs.Common;
using affs.Model;
using affs.ViewModel;
using affs.View.Dialogs;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using System.Windows;
using System.Collections.Generic;
using System.Linq;

namespace affs.ViewModel
{
    public class MultiForecastViewModel : ViewModelBase
    {
        private ObservableCollection<Variable> variables;
        private MultiForecast multiForecast;

        private RelayCommand addVariable;               // Добавление переменной в модель
        private RelayCommand trainModels;               // Обучение всех моделей
        private RelayCommand runForecast;               // Выполнение прогноза
        private RelayCommand correlation;               // Посмотреть коррелацию
        private RelayCommand settings;                  // Настройки

        public MultiForecastViewModel(ObservableCollection<Variable> variables, MultiForecast multiForecast)
        {
            this.variables = variables;
            this.multiForecast = multiForecast;
        }

        public ObservableCollection<Variable> Variables { get => variables; set { variables = value; OnProperyChanged(); } }

        public MultiForecast MultiForecast { get => multiForecast; set { multiForecast = value; OnProperyChanged(); } }
        public bool IsAllTrain {
            get {
                bool isAllTrain = true;
                foreach (IMultiForecast imf in Methods)
                {
                    isAllTrain &= imf.IsTrain;
                }
                return isAllTrain;
            }
        }

        public ObservableCollection<IMultiForecast> Methods { get => multiForecast.Methods; set { multiForecast.Methods = value; OnProperyChanged(); } }

        /// <summary>
        /// Добавляем переменные
        /// </summary>
        public RelayCommand AddVariable => addVariable ?? (addVariable = new RelayCommand(obj => {
            AddMultiVariable amv = new AddMultiVariable(variables);
            if (amv.ShowDialog() == true) {
                foreach (IMultiForecast imf in Methods) {
                    if (imf.Variable == amv.SelectVariable || imf.Compare == amv.SelectVariable) {
                        MessageBox.Show("Такая переменная уже есть.", "Ошибка выбора", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                Console.WriteLine("Добавляем переменную обучения");
                Forecast f = new Forecast(amv.SelectVariable.Name, new ForecastANFIS(_radii: 0.95));
                Methods.Add(new FuzzyCognitiveMap(amv.SelectVariable, f, amv.Threshold, amv.SelectCombine));
                // Перезаписывем значения переменных в данных приготовления
                ObservableCollection<Variable> vars = new ObservableCollection<Variable>();
                foreach (IMultiForecast mf in Methods)
                {
                    vars.Add(mf.Variable);
                }
                foreach (IMultiForecast mf in Methods)
                {
                    mf.DataPreparation.Variables = vars;
                }
                Trace.WriteLine(string.Format("Добавили переменную {0}", amv.SelectVariable.Name));
            }
        }));
        /// <summary>
        /// Обучаем модель
        /// </summary>
        public RelayCommand TrainModels => trainModels ?? (trainModels = new RelayCommand(obj => {
            View.TrainMultiForecast tmf = new View.TrainMultiForecast(MultiForecast);
            tmf.ShowDialog();
        }, obj => Methods.Count > 0));
        /// <summary>
        /// Запускаем модели если больше чем один концепт и все модели обучены
        /// </summary>
        public RelayCommand RunForecast => runForecast ?? (runForecast = new RelayCommand(obj => {
            //int? horizont = MultiForecast.Methods.FirstOrDefault(x => x.Compare != null).Compare.Range.Count;
            //if (horizont == null) horizont = 1;
            //foreach(IMultiForecast imf in MultiForecast.Methods) {
            //    if (imf.Compare != null)
            //    {
            //        horizont = Math.Min((int)horizont, imf.Compare.Range.Count);
            //    }
            //}
            ChangeHorizont ch = new ChangeHorizont(-1);
            if (ch.ShowDialog() == true)
            {
                MultiForecasting mf = new MultiForecasting(multiForecast, ch.Horizont);
                mf.ShowDialog();
            }
        }, obj => Methods.Count > 0 && IsAllTrain));
        /// <summary>
        /// Построенеи зависимости на основе корреляционного анализа
        /// </summary>
        public RelayCommand Correlation => correlation ?? (correlation = new RelayCommand(obj => {
            IMultiForecast imf = obj as IMultiForecast;
            if (obj != null) {
                (new MFCorrelationView(imf, Methods)).ShowDialog();
                Trace.WriteLine(string.Format("Количество входов: {0}, горизонт: {1}", imf.ForecastModel.DataPreparation.CountInput, imf.ForecastModel.DataPreparation.Horizont));
            }
        }));
        /// <summary>
        /// Настройки модели прогнозирования выбранного концепта
        /// </summary>
        public RelayCommand Settings => settings ?? (settings = new RelayCommand(obj => {
            IMultiForecast imf = obj as IMultiForecast;
            if (obj != null) {
                (new SettingsFCM((FuzzyCognitiveMap)imf)).ShowDialog();
            }
        }));

    }
}