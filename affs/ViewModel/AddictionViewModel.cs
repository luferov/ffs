﻿using affs.Common;
using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace affs.ViewModel
{
    public class AddictionViewModel : ViewModelBase
    {
       

        private Dictionary<Variable, List<int>> autoregressInput;           // Параметры авторегресии входные
        private Dictionary<Variable, List<int>> autoregressOutput;          // Параметры авторегресии выходные

        private ObservableCollection<autoRegression> inputAutoregress;
        private ObservableCollection<autoRegression> outputAutoregress;
        private autoRegression selectIA;                                    // Выбранная регрессия входной переменной
        private autoRegression selectOA;                                    // Выбранная регрессия выходной переменной
        private ObservableCollection<Variable> variables;                   // Переменные
        private Variable selectInput;                                       // Выбранная входящая
        private Variable selectOutput;                                      // Выбранная выходная
        private int inputAR;                                                // Авторегрессия входная
        private int outputAR;                                               // Авторегрессия выходная


        private RelayCommand addIAR;
        private RelayCommand addOAR;
        private RelayCommand deleteIA;
        private RelayCommand deleteOA;

        #region Конструктор
        public AddictionViewModel(ObservableCollection<Variable> _variables,
                                 Dictionary<Variable, List<int>> ai,
                                 Dictionary<Variable, List<int>> ao)
        {
            variables = _variables;
            autoregressInput = ai;
            autoregressOutput = ao;
            InputAutoregress = new ObservableCollection<autoRegression>();
            OutputAutoregress = new ObservableCollection<autoRegression>();
            BuildRegression();
        }

        
        #endregion
        #region Свойства
        /// <summary>
        /// Входные авторегрессионные параметры
        /// </summary>
        public ObservableCollection<autoRegression> InputAutoregress
        {
            get { return inputAutoregress; }
            set{
                inputAutoregress = value;
                OnProperyChanged("InputAutoregress");
            }
        }
        /// <summary>
        /// Выходные авторегрессионные параметры
        /// </summary>
        public ObservableCollection<autoRegression> OutputAutoregress
        {
            get { return outputAutoregress; }
            set {
                outputAutoregress = value;
                OnProperyChanged("OutputAutoregress");
            }
        }
        /// <summary>
        /// Выбранная входная переменная 
        /// </summary>
        public Variable SelectInput
        {
            get { return selectInput; }
            set{
                selectInput = value;
                OnProperyChanged("SelectInput");
            }
        }
        /// <summary>
        /// Выбранная выходная переменная
        /// </summary>
        public Variable SelectOutput
        {
            get { return selectOutput; }
            set {
                selectOutput = value;
                OnProperyChanged("SelectOutput");
            }
        }
        /// <summary>
        /// Выбранный параметр авторегресии входной переменной
        /// </summary>
        public int InputAR
        {
            get { return inputAR; }
            set {
                inputAR = value;
                OnProperyChanged("InputAR");
            }
        }
        /// <summary>
        /// Выбранный параметр авторегресии выходной переменной
        /// </summary>
        public int OutputAR
        {
            get { return outputAR; }
            set {
                outputAR = value;
                OnProperyChanged("OutputAR");
            }
        }
        /// <summary>
        /// Параметры модели входных переменных
        /// </summary>
        public Dictionary<Variable, List<int>> AutoregressInput
        {
            get { return autoregressInput; }
        }
        /// <summary>
        /// Параметры модели выходных переменных
        /// </summary>
        public Dictionary<Variable, List<int>> AutoregressOutput
        {
            get { return autoregressOutput; }
        }
        /// <summary>
        /// Переменные
        /// </summary>
        public ObservableCollection<Variable> Variables
        {
            get { return variables; }
        }
        /// <summary>
        /// Регрессия входной переменной
        /// </summary>
        public autoRegression SelectIA
        {
            get { return selectIA; }
            set {
                selectIA = value;
                OnProperyChanged("SelectIA");
            }
        }
        /// <summary>
        /// Регрессия выходной переменной
        /// </summary>
        public autoRegression SelectOA
        {
            get { return selectOA; }
            set {
                selectOA = value;
                OnProperyChanged("SelectOA");
            }
        }

        /// <summary>
        /// Добавление входящей авторегрессии
        /// </summary>
        public RelayCommand AddIAR
        {
            get {
                return addIAR ??
                    (addIAR = new RelayCommand(obj=> {
                        Variable v = SelectInput;
                        // Перебор, если такой параметр есть
                        foreach (autoRegression ar in InputAutoregress)
                        {
                            if (ar.Name == v.Name && ar.Ar == InputAR)
                            {
                                MessageBox.Show("Такой элемент уже существует!","Ошибка",MessageBoxButton.OK,MessageBoxImage.Error);
                                return;
                            }
                        }
                        // Если переменная существует
                        if (isVariableOutput(v.Name))
                        {
                            int minRegress = findMinByNameOutput(v.Name);
                            if (InputAR >= minRegress)
                            {
                                MessageBox.Show("Входной параметр авторегрессии не может быть больше или равен выходному!",
                                   "Ошибка",
                                   MessageBoxButton.OK,
                                   MessageBoxImage.Error);
                                return;
                            }
                        }
                        autoRegression _ar = new autoRegression() { Ar = InputAR, Name = v.Name };
                        InputAutoregress.Add(_ar);
                    },obj => SelectInput != null));
            }
        }
        /// <summary>
        /// Добавление выходящей авторегрессии
        /// </summary>
        public RelayCommand AddOAR
        {
            get {
                return addOAR ??
                    (addOAR = new RelayCommand(obj => {
                        Variable v = SelectOutput;
                        // Перебор, если такой параметр есть
                        foreach (autoRegression ar in OutputAutoregress)
                        {
                            if (ar.Name == v.Name && ar.Ar == OutputAR)
                            {
                                MessageBox.Show("Такой элемент уже существует!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                                return;
                            }
                        }
                        // Если такие переменные есть в списке
                        if (isVariableInput(v.Name))
                        {
                            int MaxRegress = findMaxByNameInput(v.Name);
                            if (OutputAR <= MaxRegress)
                            {
                                MessageBox.Show("Выходной параметр авторегрессии не может быть меньше или равен входному!", 
                                    "Ошибка",
                                    MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                                return;
                            }
                        }
                        // Если нет, смело добавляем
                        autoRegression _ar = new autoRegression() { Ar = OutputAR, Name = v.Name };
                        OutputAutoregress.Add(_ar);
                    },obj => SelectOutput != null && OutputAutoregress.Count == 0));
            }
        }
        /// <summary>
        /// Удаление выходящей авторегрессии
        /// </summary>
        public RelayCommand DeleteOA
        {
            get {
                return deleteOA ??
                    (deleteOA = new RelayCommand(obj=> {
                        OutputAutoregress.Remove(SelectOA);
                    }, obj => SelectOA != null));
            }
        }
        /// <summary>
        /// Удаление входящей авторегрессии
        /// </summary>
        public RelayCommand DeleteIA
        {
            get {
                return deleteIA ?? 
                    (deleteIA = new RelayCommand(obj => {
                        InputAutoregress.Remove(SelectIA);
                    }, obj=> SelectIA != null));
            }
        }

        #endregion

        #region Методы
        private void BuildRegression()
        {
            inputAutoregress.Clear();
            outputAutoregress.Clear();
            foreach(KeyValuePair<Variable,List<int>> elem in autoregressInput)
            {
                foreach (int regress in elem.Value)
                {
                    autoRegression aregress = new autoRegression() { Name = elem.Key.Name, Ar = regress };
                    inputAutoregress.Add(aregress);
                }
            }
            foreach (KeyValuePair<Variable,List<int>> elem in autoregressOutput)
            {
                foreach (int regress in elem.Value)
                {
                    autoRegression aregress = new autoRegression() { Name = elem.Key.Name, Ar = regress };
                    outputAutoregress.Add(aregress);
                }
            }
        }
        public bool Validate(out string msg)
        {
            msg = string.Empty;
            if (inputAutoregress.Count == 0)
            {
                msg = "Входные параметры не определены.";
                return false;
            }
            if (outputAutoregress.Count == 0)
            {
                msg = "Выходные параметры не определены";
                return false;
            }
            return true;
        }

        public void BuildAutoRegression()
        {
            autoregressInput.Clear();
            autoregressOutput.Clear();
            // Входные переменные
            foreach (autoRegression ar in inputAutoregress)
            {
                Variable variable = findByName(ar.Name);
                if (autoregressInput.ContainsKey(variable))     // Если ключ есть
                {
                    autoregressInput[variable].Add(ar.Ar);
                }else
                {
                    autoregressInput.Add(variable, new List<int>());
                    autoregressInput[variable].Add(ar.Ar);
                }
            }
            // Выходные переменные
            foreach (autoRegression ar in outputAutoregress)
            {
                Variable variable = findByName(ar.Name);
                if (autoregressOutput.ContainsKey(variable))     // Если ключ есть
                {
                    autoregressOutput[variable].Add(ar.Ar);
                }
                else
                {
                    autoregressOutput.Add(variable, new List<int>());
                    autoregressOutput[variable].Add(ar.Ar);
                }
            }
        }
        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <returns>Переменная</returns>
        private Variable findByName(string name)
        {
            foreach (Variable v in variables)
            {
                if (v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }
        /// <summary>
        /// Существует входная переменная
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool isVariableInput(string name)
        {
            foreach (autoRegression ar in inputAutoregress)
            {
                if (ar.Name == name)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Существует выходная переменная
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool isVariableOutput(string name)
        {
            foreach (autoRegression ar in outputAutoregress)
            {
                if (ar.Name == name)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Поиск максимума
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private int findMaxByNameInput(string name)
        {
            int result = inputAutoregress.FirstOrDefault(o => o.Name == name).Ar;
            foreach (autoRegression ar in inputAutoregress) {
                if (ar.Name == name)
                {
                    result = Math.Max(result, ar.Ar);
                }
            }
            return result;
        }
        /// <summary>
        /// Поиск минимума выходной функции
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private int findMinByNameOutput(string name)
        {
            int result = outputAutoregress.FirstOrDefault(o => o.Name == name).Ar;
            foreach (autoRegression ar in outputAutoregress)
            {
                if (ar.Name == name)
                {
                    result = Math.Min(result, ar.Ar);
                }
            }
            return result;
        }
        #endregion
    }

    public class autoRegression : ViewModelBase
    {
        private string name;
        private int ar;

        public string Name
        {
            get { return name; }
            set {
                name = value;
                OnProperyChanged("Name");
            }
        }
        public int Ar
        {
            get { return ar; }
            set {
                ar = value;
                OnProperyChanged("Ar");
            }
        }

        public autoRegression() {}
        public autoRegression(string _name, int _ar)
        {
            Name = _name;
            ar = _ar;
        }
    }

}
