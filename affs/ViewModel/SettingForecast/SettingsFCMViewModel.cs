﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.Model;

namespace affs.ViewModel
{
    public class SettingsFCMViewModel : ViewModelBase
    {
        private FuzzyCognitiveMap fuzzyCognitiveMap;
        public SettingsFCMViewModel(FuzzyCognitiveMap fcm)
        {
            FuzzyCognitiveMap = fcm;
        }
        /// <summary>
        /// Нейронечеткая модель прогнозирования
        /// </summary>
        public FuzzyCognitiveMap FuzzyCognitiveMap { get => fuzzyCognitiveMap; set { fuzzyCognitiveMap = value; OnProperyChanged(); } }

        /// <summary>
        /// Заголовок страницы
        /// </summary>
        public string Title => string.Format("Настроки модели прогнозирования: {0}", fuzzyCognitiveMap.Variable.Name);

    }
}
