﻿using FuzzyLogic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.Model;
using affs.Common;

namespace affs.ViewModel.SettingForecast
{
    /// <summary>
    /// Класс производящий настройку Anfis
    /// </summary>
    public class SettingANFISViewModel : ViewModelBase
    {
        ForecastANFIS fm;                                           // Модель
        /// Настриваемые свойства модели
        private double nu;                                          // Коэффициент обучения
        private double nuStep;                                      // Коэффициент изменения коэффициента nu
        private Dictionary<string, OrMethod> dorMethod;             // Словарь методов ИЛИ
        private OrMethod orMethod;                                  // Метод или
        private KeyValuePair<string, OrMethod> selectOrMethod;      // Выбранный ИЛИ метод
        private Dictionary<string, AndMethod> dandMethod;           // Словарь методов И
        private AndMethod andMethod;                                // Метод и
        private KeyValuePair<string, AndMethod> selectAndMethod;    // Выбранный И метод
        private double radii;                                       // Радиус кластеров
        private double sqshFactor;                                  // Коэффициент подавления
        private double acceptRatio;                                 // Коэффициент принятия
        private double rejectRatio;                                 // Коэффициент отторжения

        private string selectInputVariable;                         // Выбранная входная переменная
        private string selectOutputVariable;                        // Выбранная выходная переменная


        private RelayCommand showLinguisticTerm;                    // Показать графики лингвистических термов
        private RelayCommand showSugenoVaribales;                   // Показать сугено переменные

        #region Свойства
        /// <summary>
        /// Модель для прогнозирования
        /// </summary>
        public ForecastANFIS Fm
        {
            get { return fm; }
        }
        /// <summary>
        /// Есть ли анфиска или нет
        /// </summary>
        public bool IsAnfis {
            get { return (fm.Anfis == null) ? false : true; }

        }
        /// <summary>
        /// Коэффициент обучения
        /// </summary>
        public double Nu
        {
            get { return nu; }
            set {
                nu = value;
                OnProperyChanged("Nu");
            }
        }
        /// <summary>
        /// Коэффициент изменения коэффициента обучения
        /// </summary>
        public double NuStep
        {
            get { return nuStep; }
            set {
                nuStep = value;
                OnProperyChanged("NuStep");
            }
        }
        /// <summary>
        /// Выбор метода ИЛИ
        /// </summary>
        public OrMethod OrMethod
        {
            get { return orMethod; }
        }
        /// <summary>
        /// Или метод
        /// </summary>
        public KeyValuePair<string, OrMethod> SelectOrMethod
        {
            get { return selectOrMethod; }
            set {
                selectOrMethod = value;
                orMethod = value.Value;
                OnProperyChanged("SelectOrMethod");
            }
        }
        /// <summary>
        /// И метод
        /// </summary>
        public AndMethod AndMethod
        {
            get { return andMethod; }
        }
        /// <summary>
        /// Выбранный И метод
        /// </summary>
        public KeyValuePair<string, AndMethod> SelectAndMethod
        {
            get { return selectAndMethod; }
            set {
                selectAndMethod = value;
                andMethod = value.Value;
                OnProperyChanged("SelectAndMethod");
            }
        }
        /// <summary>
        /// Радиус кластеров
        /// </summary>
        public double Radii
        {
            get { return radii; }
            set {
                if (0 <= value && value <= 1)
                {
                    radii = value;
                    OnProperyChanged("Radii");
                }
            }
        }
        /// <summary>
        /// Коэффициент подаления
        /// </summary>
        public double SqshFactor
        {
            get { return sqshFactor; }
            set {
                sqshFactor = value;
                OnProperyChanged("SqshFactor");
            }
        }
        /// <summary>
        /// Коэффициент принятия
        /// </summary>
        public double AcceptRatio
        {
            get { return acceptRatio; }
            set {
                acceptRatio = value;
                OnProperyChanged("AcceptRatio");
            }
        }
        /// <summary>
        /// Коэффициент отторжения
        /// </summary>
        public double RejectRatio
        {
            get { return rejectRatio; }
            set {
                rejectRatio = value;
                OnProperyChanged("RejectRatio");
            }
        }
        /// <summary>
        /// Список нечетких правил
        /// </summary>
        public List<string> Rules
        {
            get {
                if (fm.Anfis != null)
                {
                    return new List<string>(fm.Anfis.RulesText);
                }
                return null;
            }
        }
        /// <summary>
        /// Входные переменные
        /// </summary>
        public List<string> InputVariables
        {
            get {
                if (fm.Anfis != null)
                {
                    List<string> iv = new List<string>();
                    List<FuzzyVariable> fvs = fm.Anfis.Input;
                    foreach (FuzzyVariable fv in fvs)
                    {
                        iv.Add(fv.Name);
                    }
                    return iv;
                }
                return null;
            }
        }
        /// <summary>
        /// Выходные переменные
        /// </summary>
        public List<string> OutputVariables
        {
            get {
                if (fm.Anfis != null)
                {
                    List<string> ov = new List<string>();
                    List<SugenoVariable> svs = fm.Anfis.Output;
                    foreach (SugenoVariable sv in svs)
                    {
                        ov.Add(sv.Name);
                    }
                    return ov;
                }
                return null;
            }
        }
        /// <summary>
        /// Словарь или метода
        /// </summary>
        public Dictionary<string, OrMethod> DorMethod
        {
            get { return dorMethod;}
        }
        /// <summary>
        /// Словарь и метода
        /// </summary>
        public Dictionary<string, AndMethod> DandMethod
        {
            get { return dandMethod; }
        }
        /// <summary>
        /// Выбранная входная переменная
        /// </summary>
        public string SelectInputVariable
        {
            get { return selectInputVariable; }
            set {
                selectInputVariable = value;
                OnProperyChanged("SelectInputVariable");
            }
        }
        /// <summary>
        /// Выбранная выходная переменная
        /// </summary>
        public string SelectOutputVariable
        {
            get { return selectOutputVariable; }
            set {
                selectOutputVariable = value;
                OnProperyChanged("SelectOutputVariable");
            }
        }
        /// <summary>
        /// Показать лингвистические термы
        /// </summary>
        public RelayCommand ShowLinguisticTerm
        {
            get {
                return showLinguisticTerm ?? 
                    (showLinguisticTerm = new RelayCommand(obj => {
                        FuzzyVariable fv = fm.Anfis.InputByName(SelectInputVariable);
                        // Если переменная найдена
                        if (fv != null)
                        {
                            View.Charts.ChartLinguisticTerms clt = new View.Charts.ChartLinguisticTerms(fv);
                            clt.ShowDialog();
                        }
                    },obj=> SelectInputVariable != null));
            }
        }
        /// <summary>
        /// Вывод информации по обученной системы сугено
        /// </summary>
        public RelayCommand ShowSugenoVaribales
        {
            get {
                return showSugenoVaribales ?? 
                    (showSugenoVaribales = new RelayCommand(obj => {
                        SugenoVariable sv = fm.Anfis.OutputByName(SelectOutputVariable);
                        if (sv != null)
                        {
                            View.Dialogs.ShowSugenoVariables ssv = new View.Dialogs.ShowSugenoVariables(fm.Anfis.Input, sv);
                            ssv.ShowDialog();
                        }
                    }, obj => SelectOutputVariable != null));
            }
        }
        
        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="fm"></param>
        public SettingANFISViewModel(Model.ForecastANFIS _fm)
        {
            fm = _fm;

            // Метод и
            dandMethod = new Dictionary<string, AndMethod>();
            dandMethod.Add("Минимум", AndMethod.Min);
            dandMethod.Add("Произведение", AndMethod.Production);
            foreach (KeyValuePair<string,AndMethod> elem in dandMethod)
            {
                if (fm.AndMethod == elem.Value)
                {
                    selectAndMethod = elem;
                }
            }
            
            // Метод или
            dorMethod = new Dictionary<string, OrMethod>();
            dorMethod.Add("Максимум", OrMethod.Max);
            dorMethod.Add("Ограниченная сумма", OrMethod.Probabilistic);
            foreach(KeyValuePair<string,OrMethod> elem in dorMethod)
            {
                if (fm.OrMethod == elem.Value)
                {
                    selectOrMethod = elem;
                }
            }
            
            // Параметры ANFIS
            Nu = fm.Nu;
            NuStep = fm.NuStep;
            orMethod = fm.OrMethod;
            andMethod = fm.AndMethod;
            radii = fm.Radii;
            sqshFactor = fm.SqshFactor;
            acceptRatio = fm.AcceptRatio;
            rejectRatio = fm.RejectRatio;
        }

    }
}
