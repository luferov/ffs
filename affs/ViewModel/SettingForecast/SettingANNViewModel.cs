﻿using affs.Common;
using affs.Model;
using FANNCSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace affs.ViewModel.SettingForecast
{
    /// <summary>
    /// Класс настройки системы прогнозирования ИНС
    /// </summary>
    class SettingANNViewModel : ViewModelBase
    {
        private Dictionary<string, ActivationFunction> afd;
        private ObservableCollection<Layer> hiddenLayers;
        private Layer selectLayer;
        private uint countInput;
        private uint countOutput;
        private uint countHiddenLayer;

        private KeyValuePair<string, ActivationFunction> selectAfInput;
        private KeyValuePair<string, ActivationFunction> selectAfOutput;
        private KeyValuePair<string, ActivationFunction> selectAfHidden;


        private RelayCommand addLayer;
        private RelayCommand removeLayer;
        

        #region Методы
        /// <summary>
        /// Скрытые слои
        /// </summary>
        public ObservableCollection<Layer> HiddenLayers
        {
            get { return hiddenLayers; }
            set {
                hiddenLayers = value;
                OnProperyChanged("HiddenLayers");
            }
        }
        /// <summary>
        /// Количество входных нейронов
        /// </summary>
        public uint CountInput
        {
            get { return countInput; }
            set {
                countInput = value;
                OnProperyChanged("CountInput");
            }
        }
        /// <summary>
        /// Количество выходных нейронов
        /// </summary>
        public uint CountOutput
        {
            get { return countOutput; }
            set {
                countOutput = value;
                OnProperyChanged("CountOutput");
            }
        }
        /// <summary>
        /// Функции принадлежности
        /// </summary>
        public Dictionary<string, ActivationFunction> Afd
        {
            get { return afd; }
        }

        public Layer SelectLayer
        {
            get { return selectLayer;}
            set {
                selectLayer = value;
                OnProperyChanged("SelectLayer");
            }
        }
        /// <summary>
        /// Выбор функции принадлежность 
        /// </summary>
        public KeyValuePair<string, ActivationFunction> SelectAfInput
        {
            get { return selectAfInput; }
            set { selectAfInput = value; }
        }
        /// <summary>
        /// Выбор функции активации скрытого слоя
        /// </summary>
        public KeyValuePair<string, ActivationFunction> SelectAfHidden
        {
            get { return selectAfHidden; }
            set {
                selectAfHidden = value;
                OnProperyChanged("SelectAfHidden");
            }
        }
        /// <summary>
        /// Выбор функции активации выходного слоя
        /// </summary>
        public KeyValuePair<string, ActivationFunction> SelectAfOutput
        {
            get { return selectAfOutput; }
            set {
                selectAfOutput = value;
                OnProperyChanged("SelectAfOutput");
            }
        }
        /// <summary>
        /// Количество нейронов скрытого слоя
        /// </summary>
        public uint CountHiddenLayer
        {
            get { return countHiddenLayer;}
            set
            {
                if (value > 0)
                {
                    countHiddenLayer = value;
                    OnProperyChanged("CountHiddenLayer");
                }
            }
        }
        /// <summary>
        /// Удаление скрытого слоя
        /// </summary>
        public RelayCommand RemoveLayer
        {
            get {
                return removeLayer ??
                    (removeLayer = new RelayCommand(obj => {
                        HiddenLayers.Remove(SelectLayer);
                    }, obj => SelectLayer != null));
            }
        }
        /// <summary>
        /// Добавление скрытого слоя
        /// </summary>
        public RelayCommand AddLayer
        {
            get {
                return addLayer ??
                    (addLayer = new RelayCommand(obj=> {
                        uint _cl = CountHiddenLayer;
                        ActivationFunction _af = SelectAfHidden.Value;
                        if (_cl > 0)
                        {
                            hiddenLayers.Add(new Layer() { CountNeural = _cl, Af = _af });
                        }
                    }));
            }
        }
        #endregion
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_layers">Слои ИНС</param>
        public SettingANNViewModel(List<Layer> _layers)
        {
            afd = new Dictionary<string, ActivationFunction>();
            afd.Add("Сигмоида", ActivationFunction.SIGMOID);
            afd.Add("Линейная", ActivationFunction.LINEAR);
            afd.Add("Шаговая", ActivationFunction.SIGMOID_STEPWISE);
            afd.Add("Тангенс гиперболический", ActivationFunction.SIGMOID_SYMMETRIC);
            hiddenLayers = new ObservableCollection<Layer>();
            // Входные и выходные слои
            CountInput = _layers[0].CountNeural;
            CountOutput = _layers[_layers.Count - 1].CountNeural;
            foreach (KeyValuePair<string,ActivationFunction> elem in afd)
            {
                if (elem.Value == _layers[0].Af) {
                    SelectAfInput = elem;
                }
                if (elem.Value == _layers[_layers.Count - 1].Af) {
                    SelectAfOutput = elem;
                }
            }
            // Скрытые нейроны
            for (int i = 1; i < _layers.Count - 1; i++) {
                hiddenLayers.Add(_layers[i]);
            }
            
        }

        public List<Layer> buildLayer() {
            List<Layer> l = new List<Layer>();
            // Входной слой
            l.Add(new Layer() {
                CountNeural = CountInput,
                Af = SelectAfInput.Value
            });
            foreach (Layer _layer in hiddenLayers)
            {
                l.Add(_layer);
            }
            //Выходной слой
            l.Add(new Layer() {
                CountNeural = CountOutput,
                Af = SelectAfOutput.Value
            });
            return l;
        }
        
    }

    

}
