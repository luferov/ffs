﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using affs.Common;
using affs.Model;
using affs.View.Dialogs;


namespace affs.ViewModel.SettingForecast
{
    public class MFCorrelationViewModel : ViewModelBase
    {
        private IMultiForecast method;                              // Метод прогнозирования
        private ObservableCollection<IMultiForecast> methods;       // Методы прогнозирования - для каждого переменной/концепта
        private DataTable variableDataTable;                        // Таблица для сохранения

        private int deepCorrelation = 3;                            // Глубина прохождения регресии для каждой переменной

        private RelayCommand changeDependence;                      // Изменить зависимость
        private RelayCommand buildAutoCorrelation;                  // Построить автоматическую корреляцию
        private RelayCommand showTrainData;                         // Вывод обучающей выборки
        private RelayCommand clusterAnalysis;                       // Настройка параметров горной кластеризации при формировании базы нечетких правил

        private List<Window> childrenWindow = new List<Window>();             // Дочерние формы


        public MFCorrelationViewModel(IMultiForecast method, ObservableCollection<IMultiForecast> methods)
        {
            Method = method;
            Methods = methods;
            VariableDataTable = CalculationCorrelation();
        }
        /// <summary>
        /// Текущая модель прогнозирования
        /// </summary>
        public IMultiForecast Method { get => method; set { method = value; OnProperyChanged(); } }
        public string Title => "Корреляция переменной " + Method.Variable.Name;
        /// <summary>
        /// Методы, представленные в модели прогнозирования
        /// </summary>
        public ObservableCollection<IMultiForecast> Methods { get => methods; set { methods = value; OnProperyChanged(); } }
        /// <summary>
        /// Глубина выполняемой корреляции
        /// </summary>
        public int DeepCorrelation { get => deepCorrelation; set {
                if (value < 1) {
                    deepCorrelation = 1;
                } else {
                    deepCorrelation = value;
                }
                OnProperyChanged();
                VariableDataTable = CalculationCorrelation();
            } }
        /// <summary>
        /// Таблица для отрисовки
        /// </summary>
        public DataTable VariableDataTable { get => variableDataTable; set { variableDataTable = value; OnProperyChanged(); } }
        /// <summary>
        /// Вывод коррелаци в таблицу
        /// </summary>
        /// <returns></returns>
        private DataTable CalculationCorrelation() {
            DataTable dt = new DataTable();
            // Добавляем autoIncrement
            dt.Columns.Add(new DataColumn("#", typeof(int)) {
                AutoIncrement = true,
                AutoIncrementSeed = 0,
                AutoIncrementStep = 1
            });
            // Добавляем колонки
            foreach (IMultiForecast imf in Methods) {
                dt.Columns.Add(new DataColumn(imf.Variable.Name, typeof(double)));
            }
            // Добавляем строки
            for (int i = 0; i < deepCorrelation; i++) {
                DataRow row = dt.NewRow();
                foreach (IMultiForecast imf in Methods)
                {
                    row[imf.Variable.Name] = Correlation(method.Variable, imf.Variable, i);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
        /// <summary>
        /// Вычисление алгоритма коррелации
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private double Correlation(Variable a, Variable b, int depth)
        {

            int variableLen = Math.Min(a.Range.Count, b.Range.Count) - depth;
            if (variableLen < 2)
            {
                return 0.0;
            }
            double[] k = new double[variableLen];
            double[] y = new double[variableLen];
            for (int i = 0; i < variableLen; i++)
            {
                k[i] = a.Range[i + depth];
                y[i] = b.Range[i];
            }
            double y1Sred = 0;
            double y2Sred = 0;
            for (int i = depth; i < k.Length; i++)
            {
                y1Sred += k[i];
                y2Sred += y[i];
            }
            y1Sred /= variableLen - depth;
            y2Sred /= variableLen - depth;
            Task<double> chislitelTask = new Task<double>(() => {
                double c = 0;
                for (int i = depth; i < k.Length; i++)
                {
                    c += (k[i] - y1Sred) * (y[i] - y2Sred);
                }
                return c;
            });
            Task<double> znamenatelTask = new Task<double>(() => {
                double z1 = 0;
                double z2 = 0;
                for (int i = depth; i < k.Length; i++)
                {
                    z1 += Math.Pow(k[i] - y1Sred, 2);
                    z2 += Math.Pow(y[i] - y2Sred, 2);
                }

                return Math.Sqrt(z1 * z2);
            });

            chislitelTask.Start();
            znamenatelTask.Start();
            Task.WaitAll(chislitelTask, znamenatelTask);
            return Math.Round(chislitelTask.Result / znamenatelTask.Result, 4);
        }

        public RelayCommand ChangeDependence => changeDependence ?? (changeDependence = new RelayCommand(obj => {
            AddictionMF amf = new AddictionMF(Method, Methods);
            childrenWindow.Add(amf);
            amf.Show();
        }));

        /// <summary>
        /// Построене автоматической корреляции, по сути берем редыдущие значеня и все
        /// </summary>
        public RelayCommand BuildAutoCorrelation => buildAutoCorrelation ?? (buildAutoCorrelation = new RelayCommand(obj => {
            Dictionary<IMultiForecast, List<int>> result = new Dictionary<IMultiForecast, List<int>>();
            foreach (IMultiForecast m in Methods)
            {
                result[m] = new List<int> { 0 };
            }
            Method.Depts = result;
            BuildTrainData();
        }));
        /// <summary>
        /// Копия функции в AdditionMFView
        /// </summary>
        private void BuildTrainData()
        {
            Dictionary<Variable, List<int>> d = new Dictionary<Variable, List<int>>();
            foreach (KeyValuePair<IMultiForecast, List<int>> kv in Method.Depts)
            {
                d.Add(kv.Key.Variable, kv.Value);
            }
            Method.ForecastModel.DataPreparation.InputRegress = d;
            Method.ForecastModel.DataPreparation.OutputRegress = new Dictionary<Variable, List<int>>
                {
                    { Method.Variable, new List<int> { 1 } }
                };
            Method.ForecastModel.DataPreparation.buildTrainData();
            Method.ForecastModel.IsTrain = false;
        }
        /// <summary>
        /// Дочерние формы, которые открыты
        /// </summary>
        public List<Window> ChildrenWindow => childrenWindow;

        public RelayCommand ShowTrainData => showTrainData ?? (showTrainData = new RelayCommand(obj => {
            View.Charts.AFDialog ad = new View.Charts.AFDialog(method.DataPreparation);
            ad.ShowDialog();
        }));

        public RelayCommand ClusterAnalysis => clusterAnalysis ?? (clusterAnalysis = new RelayCommand(obj => {
            ClusterAnalysis ca = new ClusterAnalysis(Method);
            ca.ShowDialog();
        }, obj => Method.ForecastModel.DataPreparation.InputRegress.Count > 0 && Method.ForecastModel.DataPreparation.OutputRegress.Count > 0));
    }
}
