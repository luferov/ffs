﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using affs.Common;
using System.Windows.Controls;
using System.Windows;
using affs.Model;
using affs.View;
using System.Windows.Data;
using affs.View.TabView;
using System.Windows.Forms;

namespace affs.ViewModel.Project
{

    /// <summary>
    /// Содержит сборник команд
    /// </summary>
    public partial class ProjectViewModel : ViewModelBase
    {

        private RelayCommand closeTabControl;       // Закрытие таба
        private RelayCommand addTabTimeSeries;      // Вывод переменных ряда в таб
        private RelayCommand showVariablesRange;    // Вывод таблицей ряды данных переменных
        private RelayCommand showAllVariables;      // Показать все переменные
        private RelayCommand showVariableChart;     // Показ графика
        private RelayCommand showVariablesChart;    // Вывод графика зависимостей от переменных
        private RelayCommand addIterator;           // Добавление итератора
        private RelayCommand addRandomVariable;     // Добавить случайный ряд
        private RelayCommand autoCorrelation;       // Параметры авторегрессии переменной
        private RelayCommand loadFromExcel;         // Загрузка переменных из Excel
        private RelayCommand loadFromCsv;           // Загрузить данные из CSV
        private RelayCommand renameVariable;        // Изменение имени ряда
        private RelayCommand compareVariables;      // Сравнение переменных
        private RelayCommand deleteVariable;        // Удаление ряда
        private RelayCommand deleteAllVariables;    // Удаление всех переменных
        
        private RelayCommand addForecast;           // Добавление одномерного прогноза
        private RelayCommand openForecast;          // Открытие прогноза
        private RelayCommand renameForecast;        // Переименование прогноза
        private RelayCommand deleteForecast;        // Удаление прогноза

        private RelayCommand addMultiForecast;      // Добавление многомерного прогноза
        private RelayCommand openMultiForecast;          // Открытие многомерного прогноза
        private RelayCommand renameMultiForecast;        // Переименование многомерного прогноза
        private RelayCommand deleteMultiForecast;        // Удаление многомерного прогноза


        #region Описание команд
        /// <summary>
        /// Закрываем окошко
        /// </summary>
        public RelayCommand CloseTabControl
        {
            get
            {
                return closeTabControl ??
                    (closeTabControl = new RelayCommand(obj => {
                        TabItem tabItem = obj as TabItem;
                        if (tabItem != null)
                        {
                            tabItem.IsSelected = false;
                            tabItems.Remove(tabItem);
                        }
                    }));
            }
        }
        /// <summary>
        /// Выводим ряд данных в таб
        /// </summary>
        public RelayCommand AddTabTimeSeries
        {
            get {
                return addTabTimeSeries ?? 
                    (addTabTimeSeries = new RelayCommand(obj=> {
                        Variable variable = obj as Variable;
                        View.TabView.SeriesData timeSeries = new View.TabView.SeriesData(this, variable);
                        foreach (TabItem ti in TabItems)
                        {
                            if (ti.Tag == variable)
                            {
                                SelectTabControl = ti;
                                return;
                            }
                        }
                        TabItem item = new TabItem() {Tag = variable, Content = timeSeries};
                        // Создаем привязку к данным
                        System.Windows.Data.Binding bindHeader = new System.Windows.Data.Binding("Name");
                        bindHeader.Source = variable;
                        item.SetBinding(HeaderedContentControl.HeaderProperty, bindHeader);
                        TabItems.Add(item);
                        SelectTabControl = item;
                    }));
            }
        }

        public RelayCommand ShowAllVariables
        {
            get {
                return showAllVariables ?? 
                    (showAllVariables = new RelayCommand(obj=> {
                        // Перебираем, вкладка с переменными
                        foreach (TabItem item in TabItems)
                        {
                            string itemTag = item.Tag.ToString();
                            if (itemTag == "DataVariable")
                            {
                                SelectTabControl = item;
                                return;
                            }
                        }
                        TabItem newTabItem = new TabItem() { Header = "Переменные", Tag = "DataVariable", Content = new DataVariable(this) };
                        TabItems.Add(newTabItem);
                        SelectTabControl = newTabItem;

                    }, obj => Variables.Count > 0));
            }
        }
        /// <summary>
        /// Вывод графика
        /// </summary>
        public RelayCommand ShowVariableChart
        {
            get {
                return showVariableChart ??
                    (showVariableChart = new RelayCommand(obj => {
                        Variable v = obj as Variable;
                        if (v != null)
                        {
                            View.Charts.ChartDialog chartDialog = new View.Charts.ChartDialog(v);
                            chartDialog.ShowDialog();
                        }
                    }));
            }
        }
        /// <summary>
        /// Переименовать переменную
        /// </summary>
        public RelayCommand RenameVariable
        {
            get
            {
                return renameVariable ??
                    (renameVariable = new RelayCommand(obj => {
                        Variable v = obj as Variable;
                        if (v != null)
                        {
                            View.Dialogs.Rename rename = new View.Dialogs.Rename(v.Name);
                            if (rename.ShowDialog() == true)
                            {
                                // Проверяем есть ли такое имя
                                foreach (Variable variable in variables)
                                {
                                    if (variable != v && variable.Name == rename.NameElement)
                                    {
                                        System.Windows.MessageBox.Show("Такое имя уже существует, выберете новое!",
                                            "Ошибка переименования",
                                            MessageBoxButton.OK,
                                            MessageBoxImage.Error);
                                        return;
                                    }
                                }
                                v.Name = rename.NameElement;
                            }
                        }
                    }));
            }
        }
        /// <summary>
        /// Удаление переменной
        /// </summary>
        public RelayCommand DeleteVariable
        {
            get
            {
                return deleteVariable ??
                    (deleteVariable = new RelayCommand(obj => {
                        Variable variable = obj as Variable;
                        if (variable != null)
                        {
                            foreach (TabItem item in TabItems)
                            {
                                if (item.Tag == variable)
                                {
                                    SelectTabControl = null;
                                    TabItems.Remove(item);
                                    break;
                                }
                            }
                            Variables.Remove(variable);
                        }
                    }));
            }
        }
        /// <summary>
        /// Добавление итератора
        /// </summary>
                                
        public RelayCommand AddIterator
        {
            get {
                return addIterator ??
                    (addIterator = new RelayCommand(obj => {
                        View.Dialogs.Iterator iterator = new View.Dialogs.Iterator();
                        if (iterator.ShowDialog() == true)
                        {
                            Variable v = iterator.Variable;
                            int k = 1;
                            string name = "Итератор " + k.ToString();
                            while (findVariableByName(name) != null)
                            {
                                k++;
                                name = "Итератор " + k.ToString();
                            }
                            v.Name = name;
                            Variables.Add(v);
                        }
                    }));
            }
        }
        /// <summary>
        /// Добавление случайного ряда
        /// </summary>
        public RelayCommand AddRandomVariable
        {
            get {
                return addRandomVariable ??
                    (addRandomVariable = new RelayCommand(obj => {
                        View.Dialogs.AddRandomVariable arv = new View.Dialogs.AddRandomVariable();
                        if (arv.ShowDialog() == true)
                        {
                            Variable v = arv.Variable;
                            int k = 1;
                            string name = "Случайное число " + k.ToString();
                            while (findVariableByName(name) != null)
                            {
                                k++;
                                name = "Случайное число " + k.ToString();
                            }
                            v.Name = name;
                            variables.Add(v);
                        }
                    }));
            }
        }
        /// <summary>
        /// Загрузка данных из Excel
        /// </summary>
        public RelayCommand LoadFromExcel
        {
            get {
                return loadFromExcel ??
                    (loadFromExcel = new RelayCommand(obj => {
                        OpenFileDialog dialog = new OpenFileDialog();
                        dialog.Filter = Excel.Filter;
                        if (dialog.ShowDialog() == DialogResult.OK)
                        {
                            View.Dialogs.WaitBar loadFromExcelBar = new View.Dialogs.WaitBar(dialog.FileName);
                            loadFromExcelBar.ShowDialog();
                            // Перебираем прочитанные элементы
                            foreach (Variable v in loadFromExcelBar.DataFromExcel)
                            {
                                string nameVariable = v.Name;                   // Забираем имя
                                int i = 0;                                      // Итератор переменных
                                while (findVariableByName(nameVariable) != null)        // Ищем переменные с такими именами
                                {
                                    nameVariable = v.Name + " ("+ ++i + ")";    // Добавляем итератор если найдено
                                }
                                v.Name = nameVariable;                          // Присваиваем значение
                                v.upDate();                                     // Обновляем
                                Variables.Add(v);                               // Добавляем в проект
                            }
                        }
                    }));
            }
        }
        public RelayCommand LoadFromCsv => loadFromCsv ?? (loadFromCsv = new RelayCommand(obj =>
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = CSV.Filter;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                CSV csv = new CSV(ofd.FileName);
                foreach (Variable v in csv.ReadToVariable())
                {
                    Variables.Add(v);
                }
            }
        }));

        public RelayCommand ShowVariablesRange
        {
            get {
                return showVariablesRange ??
                    (showVariablesRange = new RelayCommand(obj => {
                        View.Dialogs.SelectVariables sv = new View.Dialogs.SelectVariables(Variables);
                        if (sv.ShowDialog() == true)
                        {
                            View.TabView.SeriesData timeSeries = new View.TabView.SeriesData(sv.SelectedVariables);
                            foreach (TabItem ti in TabItems)
                            {
                                if (ti.Tag.ToString() == "ShowVariablesRange")
                                {
                                    SelectTabControl = null;
                                    TabItems.Remove(ti);
                                    break;
                                }
                            }
                            TabItem item = new TabItem() {Header = "Элементы переменных", Tag = "ShowVariablesRange", Content = timeSeries };
                            TabItems.Add(item);
                            SelectTabControl = item;
                        }
                    },obj => Variables.Count > 0));
            }
        }
        /// <summary>
        /// Вывод графика переменных
        /// Данные формируется после диалогового окна Select2Variables
        /// </summary>
        public RelayCommand ShowVariablesChart
        {
            get
            {
                return showVariablesChart??
                    (showVariablesChart = new RelayCommand(obj=> {
                        View.Dialogs.Select2Variables selectDialog = new View.Dialogs.Select2Variables(Variables);
                        selectDialog.CountX = 1;
                        if (selectDialog.ShowDialog() == true)
                        {
                            List<Variable> x = selectDialog.SelectedX;
                            List<Variable> y = selectDialog.SelectedY;
                            try
                            {
                                View.Charts.ChartDialog chartDialog = new View.Charts.ChartDialog(x, y);
                                chartDialog.ShowDialog();
                            }catch(Exception ex)
                            {
                                System.Windows.MessageBox.Show(ex.Message,"Ошибка");
                            }
                        }
                    }, obj => Variables.Count > 0));
            }
        }

        public RelayCommand DeleteAllVariables
        {
            get {
                return deleteAllVariables ?? 
                    (deleteAllVariables = new RelayCommand(obj=> {
                        if (System.Windows.Forms.MessageBox.Show(
                            "Вы действительно хотите удалить все переменные?",
                            "Удаление",
                            MessageBoxButtons.OKCancel) == DialogResult.OK)
                        {
                            variables.Clear();
                        }
                    },obj=> Variables.Count > 0));
            }
        }
        /// <summary>
        /// Создание одномерного прогноза
        /// </summary>
        public RelayCommand AddForecast
        {
            get {
                return addForecast ??
                    (addForecast = new RelayCommand(obj => {
                        View.Dialogs.CreateForecast cf = new View.Dialogs.CreateForecast();
                        if (cf.ShowDialog() == true) {
                            foreach (Forecast f in Forecasts) {
                                if (f.Name == cf.NameForecast) {
                                    System.Windows.MessageBox.Show("Имя уже существует, задайте другое!", "Ошибка создания прогноза", MessageBoxButton.OK);
                                    return;
                                }
                            }
                            Forecast forecast;
                            switch (cf.TypeOfMethod)
                            {
                                case "FANN":            // Искусственные нейронные сети
                                    forecast = new Forecast(cf.NameForecast, ChangeForecastMethod(cf.TypeOfMethod));
                                    forecasts.Add(forecast);
                                    FuncOpenForecast(forecast);
                                    break;
                                case "ANFIS":           // Нейронечеткая система прогнозирования
                                    forecast = new Forecast(cf.NameForecast, ChangeForecastMethod(cf.TypeOfMethod));
                                    forecasts.Add(forecast);
                                    FuncOpenForecast(forecast);
                                    break;
                                case "DECOMPOSITION":
                                    IForecast forecastTM = ChangeForecastMethod(cf.TrendMethod);
                                    IForecast forecastRM = ChangeForecastMethod(cf.ResiduleMethod);
                                    if (forecastRM != null && forecastTM != null)
                                    {
                                        ForecastCombine fc = new ForecastCombine(forecastTM, forecastRM, Properties.Settings.Default.fuzzyStep);
                                        forecast = new Forecast(cf.NameForecast, fc);
                                        forecasts.Add(forecast);
                                        FuncOpenForecast(forecast);
                                    }
                                    else
                                    {
                                        System.Windows.MessageBox.Show("Способ прогнозирования не найден!",
                                            "Ошибка создания прогноза",
                                            MessageBoxButton.OK);
                                    }
                                    
                                    break;
                                default:
                                    System.Windows.MessageBox.Show("Способ прогнозирования не найден!",
                                        "Ошибка создания прогноза", 
                                        MessageBoxButton.OK);
                                    break;
                            }

                        }
                    }, obj => Variables.Count > 0));  // Если есть переменные
            }
        }
        /// <summary>
        /// Создание многомерного прогноза
        /// </summary>
        public RelayCommand AddMultiForecast { get => addMultiForecast ?? (addMultiForecast = new RelayCommand(obj =>
        {
            View.Dialogs.CreateMultiForecast cmf = new View.Dialogs.CreateMultiForecast();
            if (cmf.ShowDialog() == true)
            {
                
                foreach (MultiForecast mf in MultiForecasts)
                {
                    if (mf.Name == cmf.NameMultiForecast) {
                        System.Windows.MessageBox.Show("Имя уже существует, задайте другое!", "Ошибка создания прогноза", MessageBoxButton.OK);
                        return;
                    }
                }
                switch (cmf.Method) {
                    case "FuzzyCognitiveMap":
                        MultiForecast mf = new MultiForecast(cmf.NameMultiForecast);
                        multiForecasts.Add(mf);
                        FuncOpenMultiForecast(mf);
                        break;
                    default:
                        System.Windows.MessageBox.Show("Способ прогнозирования не найден!",
                                       "Ошибка создания прогноза",
                                       MessageBoxButton.OK);
                        break;
                }
            }

        }, obj => Variables.Count > 0));
        }  // Создаем переменные 
        /// <summary>
        /// Открытие прогноза во вкладке
        /// </summary>
        public RelayCommand OpenForecast
        {
            get {
                return openForecast ??
                    (openForecast = new RelayCommand(obj=> {
                        if (obj is Forecast f)
                        {
                            FuncOpenForecast(f);
                        }
                    }));
            }
        }
        /// <summary>
        /// Изменение имя прогноза
        /// </summary>
        public RelayCommand RenameForecast
        {
            get {
                return renameForecast ?? 
                    (renameForecast = new RelayCommand(obj=> {
                        if (obj is Forecast f)
                        {
                            View.Dialogs.Rename rename = new View.Dialogs.Rename(f.Name);
                            if (rename.ShowDialog() == true)
                            {
                                // Проверяем есть ли такое имя
                                foreach (Forecast forecast in Forecasts)
                                {
                                    if (forecast != f && forecast.Name == rename.NameElement)
                                    {
                                        System.Windows.MessageBox.Show("Такое имя уже существует, выберете новое!",
                                            "Ошибка переименования",
                                            MessageBoxButton.OK,
                                            MessageBoxImage.Error);
                                        return;
                                    }
                                }
                                f.Name = rename.NameElement;
                            }
                        }
                    }));
            }
        }
        /// <summary>
        /// Удаление прогноза
        /// </summary>
        public RelayCommand DeleteForecast
        {
            get{
                return deleteForecast ??
                    (deleteForecast = new RelayCommand(obj=> {
                        if (obj is Forecast f)
                        {
                            foreach (TabItem item in TabItems)
                            {
                                if (item.Tag == f)
                                {
                                    SelectTabControl = null;
                                    TabItems.Remove(item);
                                    break;
                                }
                            }
                            Forecasts.Remove(f);
                        }

                    }));
            }
        }
        /// <summary>
        /// Открыть многомерных 
        /// </summary>
        public RelayCommand OpenMultiForecast { get => openMultiForecast ?? (openMultiForecast = new RelayCommand(obj => {
            if (obj is MultiForecast mf)
            {
                FuncOpenMultiForecast(mf);
            }
        })); }
        /// <summary>
        /// Переименовать многомерный отчет
        /// </summary>
        public RelayCommand RenameMultiForecast { get => renameMultiForecast ?? (renameMultiForecast = new RelayCommand(obj => {
            if (obj is MultiForecast mf)
            {
                View.Dialogs.Rename rename = new View.Dialogs.Rename(mf.Name);
                if (rename.ShowDialog() == true)
                {
                    foreach (MultiForecast _mf in MultiForecasts)
                    {
                        if (_mf != mf && _mf.Name == rename.NameElement)
                        {
                            System.Windows.MessageBox.Show("Такое имя уже существует, выберете новое!",
                                            "Ошибка переименования",
                                            MessageBoxButton.OK,
                                            MessageBoxImage.Error);
                            return;
                        }
                    }
                    mf.Name = rename.NameElement;
                }
            }
        })); }
        /// <summary>
        /// Удалить многомерный прогноз
        /// </summary>
        public RelayCommand DeleteMultiForecast { get => deleteMultiForecast ?? (deleteMultiForecast = new RelayCommand(obj => {
            if (obj is MultiForecast mf)
            {
                foreach (TabItem item in TabItems)
                {
                    if (item.Tag == mf)
                    {
                        SelectTabControl = null;
                        TabItems.Remove(item);
                        break;
                    }
                }
                MultiForecasts.Remove(mf);
            }
        })); }
        /// <summary>
        /// Сравнение переменных
        /// По нескольким показателям
        /// </summary>
        public RelayCommand CompareVariables
        {
            get {
                return compareVariables ??
                    (compareVariables = new RelayCommand(o => {
                        View.Dialogs.Select2Variables selectDialog = new View.Dialogs.Select2Variables(Variables);
                        selectDialog.CountX = 1;
                        selectDialog.tbX.Text = "Эталон";
                        selectDialog.tbY.Text = "Переменные для сравнения";
                        if (selectDialog.ShowDialog() == true)
                        {
                            List<Variable> x = selectDialog.SelectedX;
                            List<Variable> y = selectDialog.SelectedY;
                            Estimation est = new Estimation(x.First(), y);
                            string tag = est.Indicator;
                            foreach (TabItem ti in TabItems)
                            {
                                if (ti.Tag.ToString() == tag)
                                {
                                    SelectTabControl = null;
                                    TabItems.Remove(ti);
                                    break;
                                }
                            }
                            TabItem item = new TabItem() { Header = "Сравнение "+x.First().Name, Tag = tag, Content = est };
                            TabItems.Add(item);
                            SelectTabControl = item;
                        }

                    }, o => true));
            }
        }

        public RelayCommand AutoCorrelation
        {
            get
            {
                return autoCorrelation ?? (autoCorrelation = new RelayCommand(obj=> {
                    Variable v = obj as Variable;
                    if (obj == null) return;
                    View.Dialogs.SelectDepth sd = new View.Dialogs.SelectDepth(3, v.Range.Count / 2 - 1);
                    if (sd.ShowDialog() == true)
                    {
                        AutoCorrelations ac = new AutoCorrelations(v, sd.Depth);
                        string tag = ac.Indicator;
                        foreach (TabItem ti in TabItems)
                        {
                            if (ti.Tag.ToString() == tag)
                            {
                                SelectTabControl = null;
                                TabItems.Remove(ti);
                                break;
                            }
                        }
                        TabItem item = new TabItem() { Header = "Автокорреляция " + v.Name, Tag = tag, Content = ac };
                        TabItems.Add(item);
                        SelectTabControl = item;
                    }
                }));
            }
        }



        #endregion

        #region Функции
        /// <summary>
        /// Открытие прогноза в табе
        /// </summary>
        /// <param name="f"></param>
        private void FuncOpenForecast(Forecast f)
        {
            foreach (TabItem ti in TabItems)
            {
                if (ti.Tag == f)
                {
                    SelectTabControl = ti;
                    return;
                }
            }
            ForecastTab ft = new ForecastTab(Variables, f);
            TabItem tabItem = new TabItem() { Tag = f, Content = ft };
            System.Windows.Data.Binding bindHeader = new System.Windows.Data.Binding("Name")
            {
                Source = f
            };
            tabItem.SetBinding(HeaderedContentControl.HeaderProperty, bindHeader);
            TabItems.Add(tabItem);
            SelectTabControl = tabItem;
        }

        private IForecast ChangeForecastMethod(string method)
        {
            switch (method)
            {
                case "FANN":            // Искусственные нейронные сети
                    return new ForecastANN();
                case "ANFIS":           // Нейронечеткая система прогнозирования
                    return new ForecastANFIS(0.5);
                default:
                    System.Windows.MessageBox.Show("Способ прогнозирования не найден!", "Ошибка создания прогноза", MessageBoxButton.OK);
                    return null;
            }
        }
        private void FuncOpenMultiForecast(MultiForecast f)
        {
            foreach (TabItem ti in TabItems)
            {
                if (ti.Tag == f)
                {
                    SelectTabControl = ti;
                    return;
                }
            }
            MultiForecastTab mft = new MultiForecastTab(Variables, f);
            TabItem tabItem = new TabItem() { Tag = f, Content = mft };
            System.Windows.Data.Binding bindHeader = new System.Windows.Data.Binding("Name")
            {
                Source = f
            };
            tabItem.SetBinding(HeaderedContentControl.HeaderProperty, bindHeader);
            TabItems.Add(tabItem);
            SelectTabControl = tabItem;
        }
        #endregion
    }
}
