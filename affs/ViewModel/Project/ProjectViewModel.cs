﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using affs.View.TabView;
using affs.Model;
using System.Windows;
using FuzzyLogic;

namespace affs.ViewModel.Project
{
    public partial class ProjectViewModel : ViewModelBase
    {
        private User user;
        private ObservableCollection<TabItem> tabItems;             // Набор табов
        private TabItem selectTabControl;                               // Активный таб проекта
        private ObservableCollection<Variable> variables;               // Массив рядов данных
        private ObservableCollection<Forecast> forecasts;               // Массив прогнозных моделей
        private ObservableCollection<MultiForecast> multiForecasts;      // Массив многомерных прогнозных моделей


        /// <summary>
        /// Конструктор, инициализация проекта
        /// </summary>
        public ProjectViewModel() {
            tabItems = new ObservableCollection<TabItem>();
            variables = new ObservableCollection<Variable>();
            forecasts = new ObservableCollection<Forecast>();
            multiForecasts = new ObservableCollection<MultiForecast>();
            tabItems.Add(new TabItem() { Header = "Стартовая страничка",Tag="StartPage", Content = new StartWindowTab() });
            selectTabControl = tabItems[0];
        }

        public ProjectViewModel(User u) :this()
        {
            user = u;
        }


        #region Свойства проекта
        /// <summary>
        /// Ссылки табов
        /// </summary>
        public ObservableCollection<TabItem> TabItems
        {
            get { return tabItems; }
            set {
                tabItems = value;
                OnProperyChanged("TabItems");
            }
        }
        /// <summary>
        /// Активный таб
        /// </summary>
        public TabItem SelectTabControl
        {
            get => selectTabControl;
            set { selectTabControl = value; OnProperyChanged("SelectTabControl");}
        }

        public ObservableCollection<Variable> Variables
        {
            get => variables;
            set {variables = value; OnProperyChanged("Variables");}
        }
        

        public User User
        {
            get => user;
            set { user = value; OnProperyChanged(); }
        }

        public ObservableCollection<Forecast> Forecasts
        {
            get => forecasts;
            set { forecasts = value; OnProperyChanged();}
        }

        public ObservableCollection<MultiForecast> MultiForecasts { get => multiForecasts; set { multiForecasts = value; OnProperyChanged(); } }
        #endregion
        #region Вспомогательные методы
        public Variable findVariableByName(string name)
        {
            foreach (Variable v in variables)
            {
                if (v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }
        
        
        #endregion
    }
}
