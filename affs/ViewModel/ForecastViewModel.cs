﻿using affs.Common;
using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace affs.ViewModel
{
    public class ForecastViewModel : ViewModelBase
    {
        private Forecast f;
        private ObservableCollection<Variable> variables;

        // Параметры класса DataPreparation
        private string stringModel;                 // Строковое представление зависимости
        private int horizont;                       // Горизонт прогнозирования
        private int trainDataCount;                 // Количество обучающей выборки
        private int countInput;                     // Количество входов
        private int countOutput;                    // Количество выходов
        
        private Variable selectFVariable;           // Выбранная переменная
        
        // Команды модели прогнозирования
        private RelayCommand runForecast;           // Выполнение прогноза
        private RelayCommand settingFS;             // Настройки системы обучения
        private RelayCommand addiction;             // Задание или изменение зависимости
        private RelayCommand showTrainData;         // Вывод обучающей выборки
        private RelayCommand train;                 // Обучение модели прогнозирования
        private RelayCommand showErrorTrain;        // Показать ошибку обучения

        // Команды прогнозов
        private RelayCommand showForecastChart;     // Построить график прогнозного значения
        private RelayCommand showFullChart;         // Построить график с ретроспективными данными
        private RelayCommand moveToVariables;       // Перенести прогноз в переменные
        private RelayCommand addToVariable;         // Добавить к переменной
        private RelayCommand removeForecast;        // Удалить прогноз

        public ForecastViewModel(ObservableCollection<Variable> _variables, Forecast _forecast) {
            f = _forecast;
            variables = _variables;
            upDateDataPreparation();
        }
        
        #region Свойства
        /// <summary>
        /// Прогнозная модель
        /// </summary>
        public Forecast F
        {
            get { return f; }
            set {
                f = value;
                OnProperyChanged("F");
            }
        }
        /// <summary>
        /// Прогнозные значения
        /// </summary>
        public ObservableCollection<Variable> ForecastVariables
        {
            get { return f.ForecastVariables; }
            set {
                f.ForecastVariables = value;
                OnProperyChanged("ForecastVariables");
            }
        }
        /// <summary>
        /// Выбранная переменная
        /// </summary>
        public Variable SelectFVariable
        {
            get { return selectFVariable; }
            set {
                selectFVariable = value;
                OnProperyChanged("SelectFVariable");
            }
        }
        /// <summary>
        /// Строковая зависимость
        /// </summary>
        public string StringModel {
            get { return stringModel; }
            set {
                stringModel = value;
                OnProperyChanged("StringModel");
            }
        }
        /// <summary>
        /// Горизонт прогнозирования
        /// </summary>
        public int Horizont
        {
            get { return horizont; }
            set {
                horizont = value;
                OnProperyChanged("Horizont");
            }
        }
        /// <summary>
        /// Количество элементов обучающей выборки
        /// </summary>
        public int TrainDataCount
        {
            get { return trainDataCount; }
            set {
                trainDataCount = value;
                OnProperyChanged("TrainDataCount");
            }
        }

        /// <summary>
        /// Количество входных переменных
        /// </summary>
        public int CountInput
        {
            get { return countInput; }
            set
            {
                if (value > 0)
                {
                    countInput = value;
                    OnProperyChanged("CountInput");
                }
            }
        }
        /// <summary>
        /// Количество выходных переменных
        /// </summary>
        public int CountOutput
        {
            get { return countOutput; }
            set
            {
                if (value > 0)
                {
                    countOutput = value;
                    OnProperyChanged("CountOutput");
                }
            }
        }
        /// <summary>
        /// Максимальное количество эпох обучения
        /// </summary>
        public int CountEpochs
        {
            get { return f.ForecastMethod.Epochs; }
            set
            {
                if (value > 0)
                {
                    f.ForecastMethod.Epochs = value;
                    OnProperyChanged("CountEpochs");
                }
            }
        }
        /// <summary>
        /// Желательная ошибка обучения
        /// </summary>
        public double WantError
        {
            get { return f.ForecastMethod.Error; }
            set
            {
                f.ForecastMethod.Error = value;
                OnProperyChanged("WantError");
            }
        }
        /// <summary>
        /// Отчет между эпохами
        /// </summary>
        public int Report {
            get { return f.ForecastMethod.Report; }
            set
            {
                if (value > 0)
                {
                    f.ForecastMethod.Report = value;
                    OnProperyChanged("Report");
                }
            }
        }
        /// <summary>
        /// Коэффициент нормализации
        /// </summary>
        public double K
        {
            get { return f.K; }
            set {
                if (value >= 1)
                {
                    f.K = value;
                    OnProperyChanged("K");
                }
            }
        }
        /// <summary>
        /// Разрешено ли использовать нормализацию
        /// </summary>
        public bool UseNormalization {
            get { return f.UseNoralization; }
            set {
                f.UseNoralization = value;
                OnProperyChanged("UseNormalization");
            }
        }
        /// <summary>
        /// Разрешено ли изменять нормализацию
        /// </summary>
        public bool ChangeNormalization
        {
            get { return !f.IsTrain; }
        }

        #endregion
        #region Команды модели прогнозирования
        /// <summary>
        /// Выполнение прогноза
        /// Прогноз выполняется пока допустим горизонт
        /// </summary>
        public RelayCommand RunForecast
        {
            get {
                return runForecast ??
                    (runForecast = new RelayCommand(obj => {
                        View.Dialogs.ChangeHorizont ch = new View.Dialogs.ChangeHorizont(Horizont);
                        if (ch.ShowDialog() == true)
                        {
                            int letHorizont = ch.Horizont;              // Прогнозируем на горизонт
                            
                            // Выделение прогноза в новое окно
                            View.Dialogs.Forecasting fDialog = new View.Dialogs.Forecasting(F, UseNormalization, letHorizont);
                            fDialog.ShowDialog();
                            ForecastVariables = fDialog.ForecastVariables;
                            F.DataPreparation.removeNormalization();
                            upDateDataPreparation();                    // Обновляем параметры всей системы
                        }
                    }, obj => f.IsTrain));
            }
        }
        /// <summary>
        /// Задание или изменение зависимости
        /// </summary>
        public RelayCommand Addiction
        {
            get{
                return addiction ?? 
                    (addiction = new RelayCommand(obj=> {
                        View.Dialogs.Addiction addt = new View.Dialogs.Addiction(variables,
                            f.DataPreparation.InputRegress,
                            f.DataPreparation.OutputRegress);
                        if (addt.ShowDialog() == true)
                        {
                            f.DataPreparation.InputRegress = addt.InputAutoRegression;
                            f.DataPreparation.OutputRegress = addt.OutputAutoRegression;
                            upDateDataPreparation();
                            // Инициализация модели
                            f.ForecastMethod = initModelForecasting(f.ForecastMethod);
                        }
                    }));
            }
        }
        /// <summary>
        /// Вывод обучающей выборки
        /// </summary>
        public RelayCommand ShowTrainData
        {
            get {
                return showTrainData ??
                    (showTrainData = new RelayCommand(obj=> {
                        View.Charts.AFDialog ad = new View.Charts.AFDialog(f.DataPreparation);
                        ad.ShowDialog();
                    },obj=> TrainDataCount > 0));
            }
        }
        /// <summary>
        /// Обучение нейронечеткой системы
        /// </summary>
        public RelayCommand Train
        {
            get {
                return train??
                    (train = new RelayCommand(obj=> {
                        if (validate(out string msg))
                        {
                            // Если валидация прошла успешно
                            View.Train tr = new View.Train(f);
                            tr.ShowDialog();
                            OnProperyChanged("ChangeNormalization");
                        }
                        else
                        {
                            MessageBox.Show(msg, "Ошибка обучения", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                    }, obj => TrainDataCount > 0));
            }
        }
        /// <summary>
        /// Вывод информации об ошибки обучения
        /// </summary>
        public RelayCommand ShowErrorTrain
        {
            get {
                return showErrorTrain ??
                    (showErrorTrain = new RelayCommand(obj=>{
                        View.Charts.ChartForecastError cfe = new View.Charts.ChartForecastError(F);
                        cfe.ShowDialog();
                    },obj => f.IsTrain));
            }
        }
        
        /// <summary>
        /// Настройки прогнозных моделей
        /// </summary>
        public RelayCommand SettingFS
        {
            get {
                return settingFS ??
                    (settingFS = new RelayCommand(obj => {
                        settingForecastSystem(f.ForecastMethod);
                    }, obj => TrainDataCount > 0));
            }
        }
        #endregion
        #region Команды прогнозов
        /// <summary>
        /// Показать график получившегося прогноза
        /// </summary>
        public RelayCommand ShowForecastChart
        {
            get {
                return showForecastChart??
                    (showForecastChart = new RelayCommand(obj => {
                        View.Charts.ChartDialog cd = new View.Charts.ChartDialog(SelectFVariable);
                        cd.ShowDialog();
                    },obj => SelectFVariable != null));
            }
        }
        /// <summary>
        /// Показать график с ретроспективными данными
        /// </summary>
        public RelayCommand ShowFullChart
        {
            get {
                return showFullChart??
                    (showFullChart = new RelayCommand(obj=> {
                        Variable v = new Variable(SelectFVariable.Name + " с прогнозом");
                        Variable rd = variables.FirstOrDefault(x => x.Name == SelectFVariable.Name);
                        if (rd == null)
                        {
                            MessageBox.Show("Не удалось найти переменную по имени: "+SelectFVariable.Name);
                            return;
                        }
                        foreach (double el in rd.Range)
                        {
                            v.Range.Add(el);
                        }
                        foreach (double el in SelectFVariable.Range)
                        {
                            v.Range.Add(el);
                        }
                        v.upDate();
                        View.Charts.ChartDialog cd = new View.Charts.ChartDialog(v);
                        cd.ShowDialog();
                    },obj => SelectFVariable != null));
            }
        }
        /// <summary>
        /// Переместить прогноз к переменной
        /// </summary>
        public RelayCommand MoveToVariables
        {
            get {
                return moveToVariables ??
                    (moveToVariables = new RelayCommand(obj => {
                        string sname = "Прогноз " + SelectFVariable.Name;
                        string vname = (string)sname.Clone();
                        int i = 0;
                        while (variables.FirstOrDefault(x => x.Name == vname) != null)
                        {
                            vname = sname + " " + (++i).ToString();
                        }
                        Variable vToVariables = new Variable(vname);
                        vToVariables.Range = SelectFVariable.Range;     // Переносим
                        variables.Add(vToVariables);                    // Заносим в переменные
                    }, obj => SelectFVariable != null));
            }
        }
        /// <summary>
        /// Переместить прогноз в переменные
        /// </summary>
        public RelayCommand AddToVariable
        {
            get {
                return addToVariable ??
                    (addToVariable = new RelayCommand(obj=> {
                        // Выбрать переменную
                        View.Dialogs.SelectVariable sv = new View.Dialogs.SelectVariable(variables);
                        if (sv.ShowDialog() == true)
                        {
                            Variable vr = sv.SelectVariableOutput;
                            foreach (double el in SelectFVariable.Range)
                            {
                                vr.Range.Add(el);
                            }
                            vr.upDate();
                        }
                    }, obj => SelectFVariable != null));
            }
        }
        /// <summary>
        /// Удаление прогноза
        /// </summary>
        public RelayCommand RemoveForecast
        {
            get {
                return removeForecast ??
                    (removeForecast = new RelayCommand(obj => {
                        if (f.ForecastVariables.Contains(SelectFVariable))
                        {
                            f.ForecastVariables.Remove(SelectFVariable);
                        }
                    }, obj => SelectFVariable != null));
            }
        }
        #endregion
        #region Методы модели
        /// <summary>
        /// Обновленеи данных моделей
        /// </summary>
        private void upDateDataPreparation()
        {
            f.DataPreparation.Variables = variables;
            f.DataPreparation.buildTrainData();
            StringModel = f.DataPreparation.model();
            Horizont = f.DataPreparation.Horizont;
            TrainDataCount = f.DataPreparation.TrainDataCount;
            CountInput = f.DataPreparation.CountInput;
            CountOutput = f.DataPreparation.CountOutput;
            OnProperyChanged("ChangeNormalization");
        }

        private bool validate(out string msg)
        {
            msg = string.Empty;


            return true;
        }
        
        /// <summary>
        /// Инициализация модели в зависимости от выбранной конфигурации
        /// </summary>
        /// <param name="fm"></param>
        private IForecast initModelForecasting(IForecast fm)
        {

            f.IsTrain = false;
            OnProperyChanged("ChangeNormalization");
            if (fm is ForecastANN)
            {
                // Создаем двухслойную нейронную сеть
                if (((ForecastANN)fm).Layers.Count < 2) {
                    // Если слоев нет
                    List<Layer> _layers = new List<Layer>();
                    _layers.Add(new Layer() { CountNeural = (uint)CountInput, Af = FANNCSharp.ActivationFunction.SIGMOID });
                    _layers.Add(new Layer() { CountNeural = (uint)CountOutput, Af = FANNCSharp.ActivationFunction.SIGMOID });
                    ((ForecastANN)fm).Layers = _layers;
                }else
                {
                    // Если слои уже есть, меняем первый и последний
                    ((ForecastANN)fm).Layers[0].CountNeural = (uint)CountInput;
                    ((ForecastANN)fm).Layers[((ForecastANN)fm).Layers.Count - 1].CountNeural = (uint)CountOutput;
                }
                return fm;  
            }
            else if (fm is ForecastANFIS)
            {
                // Настройка системы на основе ANFIS
                return fm;
            }
            else if (fm is ForecastCombine)
            {
                // Настройка системы на основе декомпозиции
                fm = new ForecastCombine(
                    initModelForecasting(((ForecastCombine)fm).TrendForecast),      // Модель для прогнозирования тренда
                    initModelForecasting(((ForecastCombine)fm).ResidualForecast),   // Модель для прогнозирования остаточной составляющей
                    ((ForecastCombine)fm).FuzzyStep                                 // Шаг нечеткого разбиения
                );
                return fm;
            }
            else
            {
                MessageBox.Show("Метод прогнозирования не найден.");
                return null;
            }
        }


        /// <summary>
        /// Настройки системы прогнозирования
        /// </summary>
        /// <param name="fm"></param>
        private void settingForecastSystem(IForecast fm)
        {
            if (fm is ForecastANN)
            {
                // Настройки системы на основе ИНС

                List<Layer> l = new List<Layer>(((ForecastANN)fm).Layers);
                View.Dialogs.SettingANN setting = new View.Dialogs.SettingANN(l);
                if (setting.ShowDialog() == true)
                {
                    ((ForecastANN)f.ForecastMethod).Layers = setting.Layers;
                    f.IsTrain = false;
                    OnProperyChanged("ChangeNormalization");
                }
            }
            else if (fm is ForecastANFIS)
            {
                // Настройка системы на основе ANFIS
                View.Dialogs.SettingANFIS setting = new View.Dialogs.SettingANFIS((ForecastANFIS)fm);
                if (setting.ShowDialog() == true)
                {
                    setting.setSettings();
                }
            }
            else if (fm is ForecastCombine)
            {
                // Настройка системы на основе декомпозиции
                View.Dialogs.SettingCombine setting = new View.Dialogs.SettingCombine(f.DataPreparation.TrainDataOutput,(ForecastCombine)fm);
                if (setting.ShowDialog() == true)
                {
                    ((ForecastCombine)fm).FuzzyStep = setting.Fs;
                }
            }
            else
            {
                MessageBox.Show("Метод прогнозирования не найден.");
                return;
            }
        }
        #endregion
    }

}
