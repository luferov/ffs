﻿using affs.Model;
using affs.Common;
using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace affs.ViewModel
{
    public class MultiForecastingViewModel : ViewModelBase
    {
        private int horizont = 1;
        private MultiForecast forecast;
        private ObservableCollection<MultiForecastResult> forecastResult;
        Dictionary<IMultiForecast, ObservableCollection<double>> forecatVariable;

        public MultiForecastingViewModel(MultiForecast forecast, int horizont)
        {
            Forecast = forecast;
            Horizont = horizont;
            ForecastResult = new ObservableCollection<MultiForecastResult>();
            ForecatVariable = new Dictionary<IMultiForecast, ObservableCollection<double>>();
            foreach (IMultiForecast imf in forecast.Methods)
            {
                ForecatVariable.Add(imf, new ObservableCollection<double>());
            }
        }

        public string Title => string.Format("Прогнозирование {0}", Forecast.Name);
        public MultiForecast Forecast { get => forecast; set { forecast = value; OnProperyChanged(); } }

        public ObservableCollection<MultiForecastResult> ForecastResult { get => forecastResult; set { forecastResult = value; OnProperyChanged(); OnProperyChanged("ForecastResultTable"); } }
        public DataTable ForecastResultTable {
            get {
                DataTable dt = new DataTable();
                double maxRange = 0;
                DataColumn autoIncrement = new DataColumn("#", typeof(int))
                {
                    AutoIncrement = true,
                    AutoIncrementSeed = 1,
                    AutoIncrementStep = 1,
                };
                dt.Columns.Add(autoIncrement);
                foreach (MultiForecastResult mfr in ForecastResult)
                {
                    maxRange = Math.Max(maxRange, mfr.Variable.Range.Count);
                    dt.Columns.Add(new DataColumn(mfr.Variable.Name, typeof(double)));
                }
                for (int j = 0; j < maxRange; j++)
                {
                    DataRow dr = dt.NewRow();
                    // c 1, т.к. нулевая автоинкремент
                    for (int i = 1; i < dt.Columns.Count; i++)
                    {
                        if (ForecastResult[i-1].Variable.Range.Count > j)
                        {
                            dr[dt.Columns[i]] = ForecastResult[i - 1].Variable.Range[j];
                        }
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }

        public int Horizont { get => horizont; set { horizont = value; OnProperyChanged(); } }

        public Dictionary<IMultiForecast, ObservableCollection<double>> ForecatVariable { get => forecatVariable; set { forecatVariable = value; OnProperyChanged(); } }

        public void RunForecast()
        {
            foreach (IMultiForecast imf in forecast.Methods) {
                // imf.DataPreparation.removeNormalization();
                imf.DataPreparation.Init();
                imf.DataPreparation.buildTrainData();            
            }
            // Прогнозруем
            while (horizont > 0) {
                Dictionary<IMultiForecast, double> rd = new Dictionary<IMultiForecast, double>();
                // Выполняем прогноз
                foreach (IMultiForecast imf in forecast.Methods) {
                    double[] runData;
                    if (imf.ForecastModel.UseNoralization)
                    {
                        runData = imf.ForecastModel.DataPreparation.getNDataForForecating();
                    } else {
                        runData = imf.ForecastModel.DataPreparation.getDataForForecasting();
                    }
                    // Если нет обучающей выборки, выходим из цикла прогнозирования
                    if (runData == null)
                    {
                        break;
                    }
                    // Получаем результат модели
                    double? resultFm = imf.ForecastModel.ForecastMethod.Run(runData)[0];
                    // TODO: Собираем в модель
                    if (resultFm == null)
                    {
                        rd.Add(imf, 0.0);
                    } else {
                        rd.Add(imf, Math.Round((double)resultFm, 2));
                    }
                }
                // Добавляем ряды
                foreach(IMultiForecast imf in forecast.Methods)
                {
                    double fr;
                    if (imf.ForecastModel.UseNoralization)
                    {
                        // Добавляем в каждый ряд
                        foreach (KeyValuePair<IMultiForecast, double> r in rd) {
                            imf.ForecastModel.DataPreparation.addNVariableRange(imf.ForecastModel.DataPreparation.Normalization.findByName(r.Key.Variable.Name), r.Value);
                        }
                        fr = imf.ForecastModel.DataPreparation.Normalization.getNumberByNormalization(imf.ForecastModel.DataPreparation.Normalization.findByName(imf.Variable.Name), rd[imf]);
                    } else {
                        foreach (KeyValuePair<IMultiForecast, double> r in rd) {
                            imf.ForecastModel.DataPreparation.addVariableRange(imf.ForecastModel.DataPreparation.Normalization.findByName(r.Key.Variable.Name), r.Value);
                        }
                        fr = rd[imf];
                    }
                    ForecatVariable[imf].Add(fr);
                    imf.DataPreparation.buildTrainData();
                }
                horizont--;
            }
                // Записываем результаты
            ForecastResult.Clear();
            foreach (IMultiForecast imf in forecast.Methods)
            {
                Variable vf = new Variable()
                {
                    Name = string.Format("{0}_forecast", imf.Variable.Name),
                    Range = ForecatVariable[imf]
                };
                ForecastResult.Add(new MultiForecastResult(vf, imf.Compare));
            }
            OnProperyChanged("ForecastResultTable");
        }
    }
    public class MultiForecastResult : ViewModelBase
    {
        private Variable variable;
        private Variable compare;

        public MultiForecastResult(Variable variable, Variable compare)
        {
            Variable = variable;
            Compare = compare;
        }

        public Variable Variable { get => variable; set { variable = value; UpdateMape();  } }
        public Variable Compare { get => compare; set { compare = value; UpdateMape(); } }

        public void UpdateMape()
        {
            OnProperyChanged("Variable");
            OnProperyChanged("Compare");
            OnProperyChanged("MAPE");
        }
        public double MAPE
        {
            get
            {
                if (Compare == null)
                {
                    return 0.0;
                }
                Correlation correlation = new Correlation(Variable, Compare);
                return correlation.Mape;
            }
        }
    }
    
}
