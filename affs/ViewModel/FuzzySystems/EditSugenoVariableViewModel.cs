﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using affs.Common;
using affs.View.FuzzySystems;
using FuzzyLogic;

namespace affs.ViewModel.FuzzySystems
{
    class EditSugenoVariableViewModel : ViewModelBase
    {
        private SugenoFuzzySystem sugeno;

        private string name;
        private SugenoVariable sugenoVariable;

        private RelayCommand addMF;                     // Добавляем функцию принадлежности
        private RelayCommand editMF;                    // Редактируем функцию принадлежности
        private RelayCommand deleteMF;                  // Удаляем функцию принадлежности

        public EditSugenoVariableViewModel(SugenoVariable sv, SugenoFuzzySystem sugeno)
        {
            Sugeno = sugeno;
            SugenoVariable = sv;
            name = SugenoVariable.Name;
        }

        /// <summary>
        /// Переменная сугено
        /// </summary>
        public SugenoVariable SugenoVariable { get => sugenoVariable; set => sugenoVariable = value; }
        /// <summary>
        /// Функции принадлежности
        /// </summary>
        public ObservableCollection<INamedValue> Values => new ObservableCollection<INamedValue>(SugenoVariable.Values);
        public string Name
        {
            get => name;
            set
            {
                if (value.Length >= 3)
                {
                    name = value;
                    SugenoVariable.Name = value;
                    OnProperyChanged("Name");
                }
            }
        }
        /// <summary>
        /// Модель суегно
        /// </summary>
        public SugenoFuzzySystem Sugeno { get => sugeno; set => sugeno = value; }
        /// <summary>
        /// Добавляем функцию принадлежности
        /// </summary>
        public RelayCommand AddMF => addMF ?? (addMF = new RelayCommand(obj => {
            UpdateSugenoFunction usf = new UpdateSugenoFunction(Sugeno);
            if (usf.ShowDialog() == true)
            {
                foreach (LinearSugenoFunction lsf in Values)
                {
                    if (lsf.Name == usf.NameMF)
                    {
                        MessageBox.Show(
                            "Функция принадлежности с таким именем существует, повторите действие изменив имя.",
                            "Ошибка добавления", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }
                SugenoVariable.Functions.Add(Sugeno.CreateSugenoFunction(usf.NameMF, usf.CoeffsDictionary, usf.ConstValue));
                OnProperyChanged("Values");
            }
        }));
        /// <summary>
        /// Редактируем функцию принадлежности
        /// </summary>
        public RelayCommand EditMF => editMF ?? (editMF = new RelayCommand(obj => {
            if (obj is LinearSugenoFunction lsf)
            {
                UpdateSugenoFunction usf = new UpdateSugenoFunction(Sugeno, lsf);
                if (usf.ShowDialog() == true)
                {
                    lsf.Name = usf.NameMF;
                    foreach (LinearSugenoFunction lf in Values)
                    {
                        if (lf.Name == lsf.Name && lf != lsf)
                        {
                            MessageBox.Show(
                                "Функция принадлежности с таким именем существует. Функции принадлежности присвоено старое имя.",
                                "Ошибка изменения",
                                MessageBoxButton.OK,
                                MessageBoxImage.Asterisk);
                            lsf.Name = usf.OldName;
                            break;
                        }
                    }
                    lsf.ConstValue = usf.ConstValue;
                    foreach (FuzzyVariable fv in Sugeno.Input)
                    {
                        lsf.SetCoefficient(fv, usf.CoeffsDictionary[fv]);
                    }
                    OnProperyChanged("Values");
                }
            }
        }));
        public RelayCommand DeleteMF => deleteMF ?? (deleteMF = new RelayCommand(obj => {
            if (obj is LinearSugenoFunction lsf)
            {
                SugenoVariable.Functions.Remove(lsf);
                OnProperyChanged("Values");
            }
        }));


    }
}
