﻿using affs.Common;
using affs.Model;
using System.Linq;
using FuzzyLogic;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Collections.Generic;
using affs.View.FuzzySystems;

namespace affs.ViewModel.FuzzySystems
{
    public class EditFuzzyVariableViewModel : ViewModelBase
    {
        private FuzzyVariable fuzzyVariable;
        private RelayCommand addTerm;
        private RelayCommand editTerm;
        private RelayCommand deleteTerm;


        private SeriesCollection seriesCollection = new SeriesCollection();

        public EditFuzzyVariableViewModel(FuzzyVariable fv)
        {
            FuzzyVariable = fv;
            BuildSeriesCollection();
        }

        public FuzzyVariable FuzzyVariable { get => fuzzyVariable; set => fuzzyVariable = value; }
        public double Max => FuzzyVariable.Max;
        public double Min => FuzzyVariable.Min;
        public string Name
        {
            get => FuzzyVariable.Name;
            set
            {
                if (value.Length >= 3)
                {
                    FuzzyVariable.Name = value;
                    OnProperyChanged("Name");
                }
            }
        }

        public List<FuzzyTerm> Terms => FuzzyVariable.Terms;
        public SeriesCollection SeriesCollection { get => seriesCollection; set => seriesCollection = value; }
        /// <summary>
        /// Добавление терма
        /// </summary>
        public RelayCommand AddTerm => addTerm ?? (addTerm = new RelayCommand(obj =>
        {
            UpdateFuzzyTerm updateFuzzyTerm = new UpdateFuzzyTerm();
            if (updateFuzzyTerm.ShowDialog() == true)
            {
                foreach (FuzzyTerm ft in Terms)
                {
                    if (ft.Name == updateFuzzyTerm.NameTerm)
                    {
                        MessageBox.Show("Терм с таким именем уже существует, повторите операцию изменив имя.",
                            "Ошибка добавления", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        return;
                    }
                }
                FuzzyTerm ftNew = new FuzzyTerm(updateFuzzyTerm.NameTerm, updateFuzzyTerm.Imf);
                FuzzyVariable.Terms.Add(ftNew);
                SeriesCollection.Add(AddLineSeries(ftNew));
                OnProperyChanged("Terms");
            }
        }));
        /// <summary>
        /// Редактирование терма
        /// </summary>
        public RelayCommand EditTerm => editTerm ?? (editTerm = new RelayCommand(obj => {
            if (obj is FuzzyTerm ft)
            {
                UpdateFuzzyTerm updateFuzzyTerm = new UpdateFuzzyTerm(ft);
                if (updateFuzzyTerm.ShowDialog() == true)
                {
                    //ft.Name = updateFuzzyTerm.NameTerm; - запрет на изменение имени
                    foreach (FuzzyTerm ftExists in Terms)
                    {
                        if (ftExists.Name == ft.Name && ftExists != ft)
                        {
                            MessageBox.Show("Терм с таким именем уже существует, имя терма изменено на прежнее.",
                                "Ошибка изменения", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                            ft.Name = updateFuzzyTerm.OldName;
                        }
                    }
                    ft.MembershipFunction = updateFuzzyTerm.Imf;

                    SeriesCollection.FirstOrDefault(el => el.Title == ft.Name).Values = GetValues(ft.MembershipFunction);
                    OnProperyChanged("Terms");
                }
            }
        }));
        /// <summary>
        /// Удаление терма
        /// </summary>
        public RelayCommand DeleteTerm => deleteTerm ?? (deleteTerm = new RelayCommand(obj => {
            if (obj is FuzzyTerm ft)
            {
                SeriesCollection.Remove(SeriesCollection.FirstOrDefault(el => el.Title == ft.Name));
                FuzzyVariable.Terms.Remove(ft);
                OnProperyChanged("Terms");
            }
        }));


        #region Вспомогательные функции
        /// <summary>
        /// Формируем под графики коллекцию
        /// </summary>
        private void BuildSeriesCollection()
        {
            SeriesCollection.Clear();
            foreach (FuzzyTerm ft in FuzzyVariable.Terms)
            {
                SeriesCollection.Add(AddLineSeries(ft));
            }
        }
        private LineSeries AddLineSeries(FuzzyTerm ft)
        {
            return new LineSeries
            {
                Title = ft.Name,
                Name = ft.Name,
                Values = GetValues(ft.MembershipFunction),
                PointGeometrySize = 0,
                LineSmoothness = 0,
            };
        }
        private IChartValues GetValues(IMembershipFunction imf)
        {
            ChartValues<ObservablePoint> icv = new ChartValues<ObservablePoint>();
            double step = (Max - Min) / 200;
            for (double val = Min; val <= Max; val += step)
            {
                icv.Add(new ObservablePoint(val, imf.GetValue(val)));
            }
            return icv;
        }
        #endregion
    }
}
