﻿using System;
using affs.Model;
using affs.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FuzzyLogic;
using System.Windows;
using affs.View.FuzzySystems;
using affs.View.Dialogs;
using System.Collections.ObjectModel;

namespace affs.ViewModel.FuzzySystems
{
    public class SettingsFuzzyModelViewModel : ViewModelBase
    {
        private SugenoFuzzySystem fuzzyModel;

        //
        //  Общие настройки
        //
        private Dictionary<string, OrMethod> dorMethod;
        private KeyValuePair<string, OrMethod> selectOrMethod;
        private Dictionary<string, AndMethod> dandMethod;
        private KeyValuePair<string, AndMethod> selectAndMethod;

        private RelayCommand showOutputModel;                       // Выход модели


        private RelayCommand addInputVariable;                      // Добавление входящей переменной
        private RelayCommand addOutputVariable;                     // Добавление исходящей переменной
        private RelayCommand editInput;                             // Редактирование входящей переменной
        private RelayCommand deleteInput;                           // Удаление входящей переменной
        private RelayCommand editOutput;                            // Редактирование исходящей переменной
        private RelayCommand deleteOutput;                          // Удаление исходящей переменной

        //
        //  База нечетких правил
        //
        private string ruleText;                                    // Текст правила
        private string selectRule;                                  // Выбранное правило
        private RelayCommand updateRule;                            // Обновить правило
        private RelayCommand deleteRule;                            // Удаляем правило



        //
        //  Вывод нейро-нечеткой системы 
        //




        public SettingsFuzzyModelViewModel(SugenoFuzzySystem fuzzyModel)
        {

            FuzzyModel = fuzzyModel;
            #region Общие настройки модели
            // Метод и
            dandMethod = new Dictionary<string, AndMethod>
            {
                { "Минимум", AndMethod.Min },
                { "Произведение", AndMethod.Production }
            };
            foreach (KeyValuePair<string, AndMethod> elem in dandMethod)
            {
                if (FuzzyModel.AndMethod == elem.Value)
                {
                    selectAndMethod = elem;
                }
            }

            // Метод или
            dorMethod = new Dictionary<string, OrMethod>
            {
                { "Максимум", OrMethod.Max },
                { "Ограниченная сумма", OrMethod.Probabilistic }
            };
            foreach (KeyValuePair<string, OrMethod> elem in dorMethod)
            {
                if (FuzzyModel.OrMethod == elem.Value)
                {
                    selectOrMethod = elem;
                }
            }
            #endregion
            #region База нечетких правил
            RuleText = "";
            SelectRule = null;
            #endregion

        }

        public SugenoFuzzySystem FuzzyModel { get => fuzzyModel; set { fuzzyModel = value; OnProperyChanged("FuzzyModel"); } }
        #region Общие настройки
        /// <summary>
        /// Если существуют правила
        /// </summary>
        public bool IsExistsRules => FuzzyModel.Rules.Count > 0;
        public string DBFRules => string.Format("База нечетких правил [{0}]", FuzzyModel.Rules.Count);
        /// <summary>
        /// Если существуют нечеткие переменные
        /// </summary>
        public bool IsExistsVariables => FuzzyModel.Input.Count > 0 && FuzzyModel.Output.Count > 0;
        public Dictionary<string, OrMethod> DorMethod => dorMethod;
        public KeyValuePair<string, OrMethod> SelectOrMethod
        {
            get => selectOrMethod;
            set
            {
                selectOrMethod = value;
                FuzzyModel.OrMethod = value.Value;
                OnProperyChanged("SelectOrMethod");
            }
        }
        public Dictionary<string, AndMethod> DandMethod => dandMethod;
        public KeyValuePair<string, AndMethod> SelectAndMethod
        {
            get => selectAndMethod;
            set
            {
                selectAndMethod = value;
                FuzzyModel.AndMethod = value.Value;
                OnProperyChanged("SelectAndMethod");
            }
        }
        /// <summary>
        /// Входные переменные модели
        /// </summary>
        public List<FuzzyVariable> Input => FuzzyModel.Input;
        public List<SugenoVariable> Output => FuzzyModel.Output;


        public RelayCommand AddInputVariable => addInputVariable ?? (addInputVariable = new RelayCommand(obj =>
        {
            AddInputFuzzyVariable afv = new AddInputFuzzyVariable();
            if (afv.ShowDialog() == true)
            {
                foreach (FuzzyVariable fv in Input)
                {
                    if (fv.Name == afv.VariableName)
                    {
                        MessageBox.Show(
                            "Переменная с таким названием уже существует, повторите действие изменив имя.",
                            "Ошибка добавления переменной",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                fuzzyModel.Input.Add(new FuzzyVariable(afv.VariableName, afv.VariableMin, afv.VariableMax));
                OnProperyChanged("Input");
                OnProperyChanged("IsExistsVariables");
            }
        }));

        public RelayCommand AddOutputVariable => addOutputVariable ?? (addOutputVariable = new RelayCommand(obj => {
            
            InputName sn = new InputName();
            if (sn.ShowDialog() == true)
            {
                foreach (SugenoVariable sv in Output)
                {
                    if (sn.ChosenName == sv.Name)
                    {
                        MessageBox.Show(
                            "Переменная с таким названием уже существует, повторите действие изменив имя.",
                            "Ошибка добавления переменной",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                FuzzyModel.Output.Add(new SugenoVariable(sn.ChosenName));
            }
            OnProperyChanged("Output");
            OnProperyChanged("IsExistsVariables");
        }, obj => FuzzyModel.Output.Count < 1));

        public RelayCommand EditInput => editInput ?? (editInput = new RelayCommand(obj => {
            if (obj is FuzzyVariable fv)
            {
                EditFuzzyVariable efv = new EditFuzzyVariable(fv);
                efv.ShowDialog();
                foreach (FuzzyVariable f in Input)
                {
                    // Если мы изменили название, а переменная с таким именем уже есть
                    if (f.Name == fv.Name && f != fv)
                    {
                        MessageBox.Show(
                                    "Переменная с таким названием уже существует. Переменной присвоено старое имя.",
                                    "Ошибка изменения переменной",
                                    MessageBoxButton.OK, MessageBoxImage.Warning);
                        fv.Name = efv.OldName;
                        return;
                    }
                }
            }
        }));
        public RelayCommand DeleteInput => deleteInput ?? (deleteInput = new RelayCommand(obj =>
        {
            if (obj is FuzzyVariable fv)
            {
                Input.Remove(fv);
                OnProperyChanged("Input");
                OnProperyChanged("IsExistsVariables");
            }
        }));
        public RelayCommand EditOutput => editOutput ?? (editOutput = new RelayCommand(obj => {
            // Если редактируем переменную сугено
            if (obj is SugenoVariable sv)
            {
                EditSugenoVariable esv = new EditSugenoVariable(sv, FuzzyModel);
                esv.ShowDialog();
                foreach (SugenoVariable s in Output)
                {
                    if (s.Name == sv.Name && s != sv)
                    {
                        MessageBox.Show(
                                    "Переменная с таким названием уже существует. Переменной присвоено старое имя.",
                                    "Ошибка изменения переменной",
                                    MessageBoxButton.OK, MessageBoxImage.Warning);
                        sv.Name = esv.OldName;
                        return;
                    }
                }
            }
        }));
        public RelayCommand DeleteOutput => deleteOutput ?? (deleteOutput = new RelayCommand(obj => {
            if (obj is NamedVariableImpl nvi)
            {
                FuzzyModel.Output.Remove((SugenoVariable)nvi);
                OnProperyChanged("Output");
                OnProperyChanged("IsExistsVariables");
            }
        }));
        #endregion
        #region База нечетких правил
        /// <summary>
        /// Правила
        /// </summary>
        public ObservableCollection<string> Rules {
            get {
                ObservableCollection<string> rules = new ObservableCollection<string>();
                foreach (SugenoFuzzyRule r in FuzzyModel.Rules)
                {
                    rules.Add(r.ToString());
                }
                return rules;
            }
        }
        /// <summary>
        /// Обновление
        /// </summary>
        public string UpdateAction => SelectRule == null ? "Добавить правило" : "Изменить правило";
        /// <summary>
        /// Текстовая интерпретация текста
        /// </summary>
        public string RuleText
        {
            get => ruleText;
            set
            {
                ruleText = value;
                OnProperyChanged();
                OnProperyChanged("UpdateAction");
            }
        }
        /// <summary>
        /// Выбранное правило
        /// </summary>
        public string SelectRule
        {
            get => selectRule;
            set
            {
                selectRule = value;
                RuleText = value;
                OnProperyChanged();
                OnProperyChanged("UpdateAction");
            }
        }
        public RelayCommand UpdateRule => updateRule ?? (updateRule = new RelayCommand(obj => {
            try
            {
                FuzzyModel.Rules.Add(FuzzyModel.ParseRule(RuleText));
            } catch (Exception e) {
                MessageBox.Show(e.Message, "Ошибка при добавлении правила", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            Update();
        }));

        #endregion


        #region Вспомогательные функции
        private void Update()
        {
            OnProperyChanged("Rules");
            OnProperyChanged("UpdateAction");
            OnProperyChanged("DBFRules");
            OnProperyChanged("IsExistsRules");
        }
        #endregion
    }
}
