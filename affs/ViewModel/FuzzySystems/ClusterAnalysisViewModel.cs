﻿using affs.Common;
using affs.Model;
using FuzzyLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.ViewModel.FuzzySystems
{
    public class ClusterAnalysisViewModel : ViewModelBase
    {
        private readonly IMultiForecast method;
        private DataTable variableDateTable;    // Таблица кластеров

        public ClusterAnalysisViewModel(IMultiForecast method)
        {
            this.method = method;
        }

        public bool UseNormalization
        {
            get => method.ForecastModel.UseNoralization;
            set
            {
                method.ForecastModel.UseNoralization = value;
                OnProperyChanged();
                OnProperyChanged("VariableDateTable");
            }
        }
        /// <summary>
        /// Радиус кластеров
        /// </summary>
        public double Radii
        {
            get => ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).Radii;
            set
            {
                ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).Radii = value;
                OnProperyChanged();
                OnProperyChanged("VariableDateTable");
            }
        }
        /// <summary>
        /// Коэффициент подавления
        /// </summary>
        public double SqshFactor
        {
            get => ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).SqshFactor;
            set
            {
                ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).SqshFactor = value;
                OnProperyChanged();
                OnProperyChanged("VariableDateTable");
            }
        }
        /// <summary>
        /// Коэффициент принятия
        /// </summary>
        public double AcceptRatio
        {
            get => ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).AcceptRatio;
            set
            {
                ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).AcceptRatio = value;
                OnProperyChanged();
                OnProperyChanged("VariableDateTable");
            }
        }
        /// <summary>
        /// Коэффициент принятия
        /// </summary>
        public double RejectRatio
        {
            get => ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).RejectRatio;
            set
            {
                ((ForecastANFIS)(method.ForecastModel.ForecastMethod)).RejectRatio = value;
                OnProperyChanged();
                OnProperyChanged("VariableDateTable");
            }
        }

        public DataTable VariableDateTable
        {
            get
            {
                method.DataPreparation.buildTrainData();
                DataTable dt = new DataTable();
                DataColumn autoIncrement = new DataColumn("#", typeof(int))
                {
                    AutoIncrement = true,
                    AutoIncrementSeed = 1,
                    AutoIncrementStep = 1
                };
                dt.Columns.Add(autoIncrement);
                double[][] x = new double[method.ForecastModel.DataPreparation.TrainDataInput[0].Length + 1][];

                if (method.ForecastModel.UseNoralization)
                {
                    for (int i = 0; i < x.Length - 1; i++)
                    {
                        x[i] = new double[method.ForecastModel.DataPreparation.TrainDataNInput.Length];
                        for (int j = 0; j < x[i].Length; j++)
                        {
                            x[i][j] = method.ForecastModel.DataPreparation.TrainDataNInput[j][i];
                        }
                    }
                    x[x.Length - 1] = new double[method.ForecastModel.DataPreparation.TrainDataNInput.Length];
                    for (int j = 0; j < x[x.Length - 1].Length; j++)
                    {
                        x[x.Length - 1][j] = method.ForecastModel.DataPreparation.TrainDataNOutput[j][0];
                    }
                }
                else
                {
                    for (int i = 0; i < x.Length - 1; i++)
                    {
                        x[i] = new double[method.ForecastModel.DataPreparation.TrainDataInput.Length];
                        for (int j = 0; j < x[i].Length; j++)
                        {
                            x[i][j] = method.ForecastModel.DataPreparation.TrainDataInput[j][i];
                        }
                    }
                    x[x.Length - 1] = new double[method.ForecastModel.DataPreparation.TrainDataInput.Length];
                    for (int j = 0; j < x[x.Length - 1].Length; j++)
                    {
                        x[x.Length - 1][j] = method.ForecastModel.DataPreparation.TrainDataOutput[j][0];
                    }
                }

                SubtractClustesing subclust = new SubtractClustesing(x, Radii, SqshFactor, AcceptRatio, RejectRatio);
                subclust.SubClust();
                double[][] centers = subclust.Centers;          // Центы кластеров
                for (int i = 0; i < centers.Length; i++)
                {
                    dt.Columns.Add(string.Format("{0}", i + 1), typeof(double));
                }
                for (int i = 0; i < centers[0].Length; i++)
                {
                    DataRow dr = dt.NewRow();
                    for (int j = 0; j < centers.Length; j++)
                    {
                        dr[string.Format("{0}", j + 1)] = centers[j][i];
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
        }
    }
}
