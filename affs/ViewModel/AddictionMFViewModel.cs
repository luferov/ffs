﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using affs.Common;
using System.Windows;
using affs.Model;

namespace affs.ViewModel
{
    public class AddictionMFViewModel : ViewModelBase
    {
        private IMultiForecast method;
        private ObservableCollection<RegressionMultiForecast> regressionMultiForecasts;
        private RegressionMultiForecast selectRMF = null;
        private IMultiForecast selectMultiForecast = null;
        private int depenency = 0;

        private RelayCommand addDependency;
        private RelayCommand delDependency;

        /// <summary>
        /// Конструктор класса, по умолчанию для выходной переменной имеем OutVariable(t+1) = ...
        /// </summary>
        public AddictionMFViewModel(IMultiForecast method, ObservableCollection<IMultiForecast> methods)
        {
            Methods = methods;
            Method = method;
            regressionMultiForecasts = BuildRMF();
        }

        public IMultiForecast Method { get => method; set { method = value; OnProperyChanged(); } }
        public string Title => string.Format("Установление зависимости для переменной: {0}", Method.Variable.Name);
        /// <summary>
        /// Методы прогнозирования
        /// </summary>
        public ObservableCollection<IMultiForecast> Methods { get; }
        /// <summary>
        /// Получаение списка зависимостей
        /// </summary>
        public ObservableCollection<RegressionMultiForecast> RegressionMultiForecasts { get => regressionMultiForecasts; set { regressionMultiForecasts = value; OnProperyChanged(); } }
        /// <summary>
        /// Выбранная строк задержки
        /// </summary>
        public RegressionMultiForecast SelectRMF { get => selectRMF; set { selectRMF = value; OnProperyChanged(); } }
        /// <summary>
        /// Добавление задежки ряда
        /// SelectMultiForecast - выбранная переменная
        /// Depenency - задержка зависимост
        /// </summary>
        public RelayCommand AddDependency => addDependency ?? (addDependency = new RelayCommand(obj => {
            foreach (RegressionMultiForecast rmf in RegressionMultiForecasts) {
                if (rmf.Method == SelectMultiForecast && rmf.Dept == Depenency)
                {
                    MessageBox.Show(string.Format("Зависимость {0} есть для переменной {1}.", SelectMultiForecast.Variable.Name, Depenency));
                    return;
                }
            }
            RegressionMultiForecasts.Add(new RegressionMultiForecast(SelectMultiForecast, Depenency));
            Method.Depts = BuildRegression();
            BuildTrainData();
            
        }, obj => SelectMultiForecast != null));
        /// <summary>
        /// Удаление задержки ряда
        /// </summary>
        public RelayCommand DelDependency => delDependency ?? (delDependency = new RelayCommand(obj => {
            RegressionMultiForecasts.Remove(SelectRMF);
            Method.Depts = BuildRegression();
            BuildTrainData();
        }, obj => SelectRMF != null));
        private void BuildTrainData()
        {
            Dictionary<Variable, List<int>> d = new Dictionary<Variable, List<int>>();
            foreach (KeyValuePair<IMultiForecast, List<int>> kv in Method.Depts)
            {
                d.Add(kv.Key.Variable, kv.Value);
            }
            Method.ForecastModel.DataPreparation.InputRegress = d;
            Method.ForecastModel.DataPreparation.OutputRegress = new Dictionary<Variable, List<int>>
                {
                    { Method.Variable, new List<int> { 1 } }
                };
            Method.ForecastModel.DataPreparation.buildTrainData();
            Method.ForecastModel.IsTrain = false;
        }
        /// <summary>
        /// Выбор переменной метода
        /// </summary>
        public IMultiForecast SelectMultiForecast { get => selectMultiForecast; set { selectMultiForecast = value; OnProperyChanged(); } }

        public int Depenency {
            get => depenency;
            set {
                if (value <= 0)
                    depenency = value;
                OnProperyChanged();  
            } 
        }

        private ObservableCollection<RegressionMultiForecast> BuildRMF() {
            ObservableCollection<RegressionMultiForecast> result = new ObservableCollection<RegressionMultiForecast>();
            foreach (KeyValuePair<IMultiForecast, List<int>> dept in Method.Depts)
            {
                foreach (int d in dept.Value)
                {
                    result.Add(new RegressionMultiForecast(dept.Key, d));
                }
            }
            return result;
        }
        /// <summary>
        /// Построение регрессии
        /// </summary>
        /// <returns></returns>
        public Dictionary<IMultiForecast, List<int>> BuildRegression() {
            Dictionary<IMultiForecast, List<int>> result = new Dictionary<IMultiForecast, List<int>>();
            foreach (RegressionMultiForecast rfm in RegressionMultiForecasts)
            {
                if (!result.ContainsKey(rfm.Method)) {
                    result.Add(rfm.Method, new List<int>());
                } 
                result[rfm.Method].Add(rfm.Dept);
            }
            return result;
        }

    }

    public class RegressionMultiForecast : ViewModelBase
    {
        private IMultiForecast method;
        private int dept;

        public RegressionMultiForecast(IMultiForecast method, int dept)
        {
            this.method = method;
            this.dept = dept;
        }

        public int Dept { get => dept; set { dept = value; OnProperyChanged(); } }
        public IMultiForecast Method { get => method; set { method = value; OnProperyChanged(); } }
    }
}
