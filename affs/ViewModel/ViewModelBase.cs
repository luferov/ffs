﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace affs.ViewModel
{
    /// <summary>
    /// Класс реализует INotifyPropertyChanged
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnProperyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
