﻿using affs.Common;
using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows.Forms;

namespace affs.ViewModel
{
    public class SeriesDataViewModel : ViewModelBase
    {
        private Variable variable;                  // Переменная
        private DataTable variableDataTable;        // Таблица для сохранения
        private DataColumn rangeColumn;             // Колонка данных

        private RelayCommand showVariableChart;     // Показ графика
        private RelayCommand saveToExcel;           // Сохранение данных в файл Excel

        public SeriesDataViewModel(Project.ProjectViewModel vm, Variable v)
        {
            Variable = v;
            variableDataTable = new DataTable();
            DataColumn autoIncrement = new DataColumn("#",typeof(int));
            autoIncrement.AutoIncrement = true;
            autoIncrement.AutoIncrementSeed = 0;
            autoIncrement.AutoIncrementStep = 1;
            variableDataTable.Columns.Add(autoIncrement);

            string nameRange;
            if (v.Name == "#")
            {
                nameRange = "#Range";
            }
            else
            {
                nameRange = v.Name;
            }
            rangeColumn = new DataColumn(nameRange, typeof(double));
            variableDataTable.Columns.Add(rangeColumn);
            for (int i = 0; i< Variable.Range.Count; i++)
            {
                DataRow row = variableDataTable.NewRow();
                row[rangeColumn] = Variable.Range[i];
                variableDataTable.Rows.Add(row);
            }
            variableDataTable.RowChanged += VariableDataTable_RowChanged;   // При измененнии строки
            variableDataTable.RowDeleted += VariableDataTable_RowChanged;   // Изменяем после удаления строки
        }

        public SeriesDataViewModel(List<Variable> variables)
        {
            variableDataTable = new DataTable();
            DataColumn autoIncrement = new DataColumn("#", typeof(int))
            {
                AutoIncrement = true,
                AutoIncrementSeed = 0,
                AutoIncrementStep = 1
            };
            variableDataTable.Columns.Add(autoIncrement);

            // Формируем колонки
            int maxRange = 0;       // Максимальное число строк
            foreach (Variable v in variables)
            {
                DataColumn column = new DataColumn(v.Name,typeof(double));
                maxRange = Math.Max(maxRange, v.Range.Count);
                variableDataTable.Columns.Add(column);
            }

            for (int j = 0; j < maxRange; j++)
            {
                DataRow row = variableDataTable.NewRow();
                // c 1, т.к. нулевая автоинкремент
                for (int i = 1; i < variableDataTable.Columns.Count; i++)
                {
                    // Если элемент есть в коллекции
                    if (variables[i - 1].Range.Count > j)
                    {
                        row[variableDataTable.Columns[i]] = variables[i - 1].Range[j];
                    }
                }
                variableDataTable.Rows.Add(row);
            }
        }
       

        public DataTable VariableDataTable
        {
            get { return variableDataTable; }
            set {
                variableDataTable = value;
                OnProperyChanged("VariableDataTable");
            }
        }

        public Variable Variable
        {
            get { return variable; }
            set {
                variable = value;
                OnProperyChanged("Variable");
            }
        }

        #region Команды
        public RelayCommand ShowVariableChart
        {
            get {
                return showVariableChart ?? 
                    (showVariableChart = new RelayCommand(obj=> {
                        DataTable v = obj as DataTable;
                        if (v != null)
                        {
                            View.Charts.ChartDialog chartDialog = new View.Charts.ChartDialog(v);
                            chartDialog.ShowDialog();
                        }


                    }));

            }
        }
        public RelayCommand SaveToExcel
        {
            get {
                return saveToExcel ??
                    (saveToExcel = new RelayCommand(obj=> {
                        DataTable dt = obj as DataTable;
                        if (dt == null)
                        {
                            return;
                        }
                        SaveFileDialog diag = new SaveFileDialog();
                        diag.Filter = Excel.Filter;
                        if (diag.ShowDialog() == DialogResult.OK)
                        {
                            Excel ex = new Excel(diag.FileName);
                            ex.saveFromDataTable(dt);
                            ex.Dispose();
                        }
                    }));
            }
        }


        #endregion
        /// <summary>
        /// Событие при измененнии ряда, добавления или удаления элементов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VariableDataTable_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            ObservableCollection<double> newRange = new ObservableCollection<double>();
            foreach (DataRow row in variableDataTable.Rows)
            {
                newRange.Add((double)row[rangeColumn]);
            }
            Variable.Range = newRange;
        }

    }
}
