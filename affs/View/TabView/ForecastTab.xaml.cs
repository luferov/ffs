﻿using affs.Model;
using affs.ViewModel;
using System.Windows.Controls;
using System.Collections.ObjectModel;

namespace affs.View.TabView
{
    /// <summary>
    /// Логика взаимодействия для ForecastTab.xaml
    /// </summary>
    public partial class ForecastTab : UserControl
    {
        public ForecastTab(ObservableCollection<Variable> variables, Forecast forecast)
        {
            InitializeComponent();
            DataContext = new ForecastViewModel(variables, forecast);
        }
    }
}
