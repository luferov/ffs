﻿using System.Windows.Controls;
using affs.Model;
using System.Collections.Generic;

namespace affs.View.TabView
{
    /// <summary>
    /// Логика взаимодействия для SeriesData.xaml
    /// </summary>
    public partial class SeriesData : UserControl
    {
        public SeriesData(ViewModel.Project.ProjectViewModel vm, Variable v)
        {
            InitializeComponent();
            DataContext = new ViewModel.SeriesDataViewModel(vm, v);
        }
        public SeriesData(List<Variable> variables)
        {
            InitializeComponent();
            DataContext = new ViewModel.SeriesDataViewModel(variables);
        }
    }
}
