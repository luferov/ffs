﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace affs.View.TabView
{
    /// <summary>
    /// Логика взаимодействия для Estimation.xaml
    /// </summary>
    public partial class Estimation : UserControl
    {
        private string indicator;
        private ObservableCollection<Correlation> correlation;

        public Estimation(Variable f, List<Variable> a)
        {
            InitializeComponent();
            correlation = new ObservableCollection<Correlation>();
            indicator = "Estimation"+f.Name + "->";
            foreach (Variable v in a)
            {
                correlation.Add(new Correlation(f,v));
                indicator += v.Name;
            }
            DataContext = correlation;
        }
        /// <summary>
        /// Индикатор
        /// </summary>
        public string Indicator {
            get { return indicator; }

        }
    }
}
