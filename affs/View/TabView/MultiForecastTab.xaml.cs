﻿using affs.Model;
using affs.ViewModel;
using System.Windows.Controls;
using System.Collections.ObjectModel;

namespace affs.View.TabView
{
    /// <summary>
    /// Логика взаимодействия для MultiForecastTab.xaml
    /// </summary>
    public partial class MultiForecastTab : UserControl
    {
        public MultiForecastTab(ObservableCollection<Variable> variables, MultiForecast multiForecast)
        {
            InitializeComponent();
            DataContext = new MultiForecastViewModel(variables, multiForecast);
        }
    }
}
