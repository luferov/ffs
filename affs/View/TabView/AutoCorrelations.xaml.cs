﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace affs.View.TabView
{
    /// <summary>
    /// Логика взаимодействия для AutoCorrelations.xaml
    /// </summary>
    public partial class AutoCorrelations : UserControl
    {
        private ObservableCollection<AutoCorrelation> autoCorrelation;
        private string indicator;
        /// <summary>
        /// Индикатор таба
        /// </summary>
        public string Indicator
        {
            get { return indicator; }
        }

        public AutoCorrelations(Variable vr, int depth)
        {
            InitializeComponent();
            indicator = "AutoCorrelation_" + vr.Name;
            autoCorrelation = new ObservableCollection<AutoCorrelation>();
            for (int i = 1; i<= depth; i++)
            {
                autoCorrelation.Add(new AutoCorrelation(vr.Name, vr.Range.ToArray(), i));
            }
            DataContext = autoCorrelation;
        }

        private void showCollerogram_Click(object sender, RoutedEventArgs e)
        {
            double[] x = new double[autoCorrelation.Count];
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = autoCorrelation[i].R;
            }
            Charts.ChartDialog cd = new Charts.ChartDialog("коррелограмма " + autoCorrelation[0].Name, x, 1);
            cd.ShowDialog();
        }
    }
}
