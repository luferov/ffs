﻿using System.Windows;

namespace affs.View
{
    /// <summary>
    /// Логика взаимодействия для Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
            DataContext = new ViewModel.SettingsViewModel();
        }
    }
}
