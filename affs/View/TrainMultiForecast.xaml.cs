﻿using affs.Model;
using affs.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;


namespace affs.View
{
    /// <summary>
    /// Логика взаимодействия для TrainMultiForecast.xaml
    /// </summary>
    public partial class TrainMultiForecast : Window
    {
        private MultiForecast multiForecast;
        private Dictionary<IMultiForecast, List<double>> errors = new Dictionary<IMultiForecast, List<double>>();
        public TrainMultiForecast(MultiForecast mf)
        {
            InitializeComponent();
            this.multiForecast = mf;
            this.Loaded += TrainMultiForecast_Loaded;
        }

        /// <summary>
        /// Загрузка модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrainMultiForecast_Loaded(object sender, RoutedEventArgs e)
        {
            Dictionary<IMultiForecast, Task<List<double>>> tasks = new Dictionary<IMultiForecast, Task<List<double>>>();
            foreach (IMultiForecast imf in multiForecast.Methods)
            {
                List<double> _errors = new List<double>();
                imf.ForecastModel.DataPreparation.Init();
                imf.ForecastModel.DataPreparation.buildTrainData();
                if (imf.ForecastModel.UseNoralization)
                {
                    imf.DataPreparation.Normalization.setK(imf.K);
                    imf.ForecastModel.ForecastMethod.TrainDataInput = imf.ForecastModel.DataPreparation.TrainDataNInput;
                    imf.ForecastModel.ForecastMethod.TrainDataOutput = imf.ForecastModel.DataPreparation.TrainDataNOutput;
                } else {
                    imf.ForecastModel.ForecastMethod.TrainDataInput = imf.ForecastModel.DataPreparation.TrainDataInput;
                    imf.ForecastModel.ForecastMethod.TrainDataOutput = imf.ForecastModel.DataPreparation.TrainDataOutput;
                }
                if (imf.ForecastModel.ForecastMethod.Validate(out string msg))
                {
                    imf.ForecastModel.ForecastMethod.Init();
                    imf.ForecastModel.ForecastMethod.PrintTrainReport = (double error, int epoch, int maxEpochs) =>
                    {
                        _errors.Add(error);
                    };
                    // imf.ForecastModel.ForecastMethod
                    imf.ForecastModel.ForecastMethod.Train();
                    imf.ForecastModel.ErrorTrain = _errors.ToArray();
                    imf.ForecastModel.IsTrain = true;
                }
                else
                {
                    MessageBox.Show(msg);
                }
            }

            Close();
        }
        
    }
}
