﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.ViewModel.Project;

namespace affs.View
{
    /// <summary>
    /// Логика взаимодействия для Project.xaml
    /// </summary>
    public partial class Project : Window
    {
        public Project()
        {
            InitializeComponent();
            //checkUser();  // Проверка пользователя
            DataContext = new ProjectViewModel(new Model.User() { Name="Виктор Луферов", Login = "Luferov"});
        }

        /// <summary>
        /// Проверка пользователя
        /// </summary>
        private void checkUser() {
            using (StartApp startApp = new StartApp())
            {
                startApp.ShowDialog();
                if (startApp.Block == true) Close();
                startApp.Dispose();
            }
        }
    }
}
