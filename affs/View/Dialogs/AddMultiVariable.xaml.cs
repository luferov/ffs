﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для AddMultiVariable.xaml
    /// </summary>
    public partial class AddMultiVariable : Window
    {
        public AddMultiVariable(ObservableCollection<Variable> vars)
        {
            InitializeComponent();
            Variables = vars;
            DataContext = this;
        }

        public double Threshold { get; set; } = 0.95;
        public ObservableCollection<Variable> Variables { get; set; }
        public Variable SelectCombine { get; set; } = null;
        public Variable SelectVariable { get; set; } = null;

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (SelectVariable == null)
            {
                MessageBox.Show("Выберите переменную", "Ошибка выбора", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (SelectVariable == SelectCombine)
            {
                MessageBox.Show("Нельзя сравнивать переменную саму с собой", "Ошибка выбора", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DialogResult = true;
            Close();
        }

        
    }
}
