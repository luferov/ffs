﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Model;
using affs.ViewModel;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для MultiForecasting.xaml
    /// </summary>
    public partial class MultiForecasting : Window
    {
        private MultiForecastingViewModel mfvm;
        public MultiForecasting(MultiForecast forecast, int horizont)
        {
            InitializeComponent();
            mfvm = new MultiForecastingViewModel(forecast, horizont);
            Loaded += (object sender, RoutedEventArgs e) => {
                mfvm.RunForecast();   
            };
            DataContext = mfvm;
        }

    }
}
