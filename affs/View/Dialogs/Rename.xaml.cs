﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для Rename.xaml
    /// </summary>
    public partial class Rename : Window
    {
        public string NameElement
        {
            get { return nameElement.Text; }
            set { nameElement.Text = value; }
        }

        public Rename()
        {
            InitializeComponent();
        }
        public Rename(string name):this()
        {
            nameElement.Text = name;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            if (nameElement.Text.Length < 1)
            {
                MessageBox.Show("Минимальная длина текста должна быть больше 1 символа","Ошибка переименования",MessageBoxButton.OK);
                return;
            }
            DialogResult = true;
        }   

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
