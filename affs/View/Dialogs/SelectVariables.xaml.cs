﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using affs.Model;
using System.Collections.ObjectModel;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SelectVariables.xaml
    /// </summary>
    public partial class SelectVariables : Window
    {

        // leftVariables    - все переменные
        // rightVariables   - выбранные 
        private List<Variable> allVariables;        // Список всех переменных  
        private List<Variable> selectedVariables;   // Список выбранных переменных
        #region Свойства
        public List<Variable> AllVariables
        {
            get { return allVariables; }
            set { allVariables = value; }
        }

        public List<Variable> SelectedVariables
        {
            get { return selectedVariables; }
            set { selectedVariables = value; }
        }
        #endregion

        public SelectVariables()
        {
            InitializeComponent();
        }

        public SelectVariables(ObservableCollection<Variable> variables) : this()
        {
            allVariables = new List<Variable>();
            SelectedVariables = new List<Variable>();
            foreach (Variable v in variables)
            {
                allVariables.Add(v);
                leftVariables.Items.Add(v.Name);
            }
            
        }
        /// <summary>
        /// Добавление переменных в выбор
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addVariable_Click(object sender, RoutedEventArgs e)
        {
            int selVar = leftVariables.SelectedIndex;
            // Переносим все иначе только выделенный
            if (selVar == -1)
            {
                // Идем с конца, чтобы не было переполнения
                for (int i = 0; i < leftVariables.Items.Count; i++)
                {
                    rightVariables.Items.Add(leftVariables.Items[i].ToString());
                }
                leftVariables.Items.Clear();
            }
            else
            {
                // Иначе добавляем и удаляем выделенный
                rightVariables.Items.Add(leftVariables.Items[selVar].ToString());
                leftVariables.Items.RemoveAt(selVar);
            }
        }
        /// <summary>
        /// Удаление переменных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeVariable_Click(object sender, RoutedEventArgs e)
        {
            int selVar = rightVariables.SelectedIndex;
            // Переносим все иначе только выделенный
            if (selVar == -1)
            {
                // Идем с конца, чтобы не было переполнения
                for (int i = 0; i < rightVariables.Items.Count; i++)
                {
                    leftVariables.Items.Add(rightVariables.Items[i].ToString());
                }
                rightVariables.Items.Clear();
            }
            else
            {
                // Иначе добавляем и удаляем выделенный
                leftVariables.Items.Add(rightVariables.Items[selVar].ToString());
                rightVariables.Items.RemoveAt(selVar);
            }
        }
        /// <summary>
        /// Двойной клик на добавление
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void leftVariables_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            addVariable_Click(sender, e);
        }

        private void rightVariables_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            removeVariable_Click(sender, e);
        }
        /// <summary>
        /// Успешно выбраны переменные
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            // Если переменных нет
            if (rightVariables.Items.Count == 0)
            {
                MessageBox.Show("Вы не выбрали переменные","Ошибка",MessageBoxButton.OK,MessageBoxImage.Asterisk);
                return;
            }
            // Формирование переменных на выход
            selectedVariables.Clear();
            foreach (string name in rightVariables.Items)
            {
                Variable variable = findByName(name);
                if (variable != null)
                {
                    selectedVariables.Add(variable);
                }
            }
            DialogResult = true;
            Close();
        }

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private Variable findByName(string name)
        {
            foreach (Variable v in allVariables)
            {
                if (v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }

    }
}
