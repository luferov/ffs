﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для CreateMultiForecast.xaml
    /// </summary>
    public partial class CreateMultiForecast : Window
    {
        private string nameMultiForecast;
        private string method;

        private Dictionary<string, string> methods;

        public string NameMultiForecast { get => nameMultiForecast;}
        public string Method { get => method;}

        public CreateMultiForecast()
        {
            InitializeComponent();
            methods = new Dictionary<string, string> {
                { "Нечеткая когнитивно продукцонная карта", "FuzzyCognitiveMap" }
            };
            foreach (KeyValuePair<string, string> m in methods)
            {
                ForecastModel.Items.Add(m.Key);
            }
        }

        private void Create_Click(object sender, RoutedEventArgs e) {
            if (ForecastName.Text.Length == 0)
            {
                MessageBox.Show("Имя не задано", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!methods.ContainsKey(ForecastModel.Text))
            {
                MessageBox.Show("Модель не выбрана", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            nameMultiForecast = ForecastName.Text;
            method = methods[ForecastModel.Text];
            DialogResult = true;
            Close();
        }

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

    }
}
