﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SelectVariable.xaml
    /// </summary>
    public partial class SelectVariable : Window
    {
        private ObservableCollection<Variable> variables;
        private Variable selectVariable;

        public Variable SelectVariableOutput
        {
            get { return selectVariable; }
            set { selectVariable = value; }
        }

        public SelectVariable(ObservableCollection<Variable> vs)
        {
            InitializeComponent();
            variables = vs;
            OkButton.IsEnabled = false;
            foreach (Variable v in vs)
            {
                listVariables.Items.Add(v.Name);
            }
            listVariables.SelectionChanged += ListVariables_SelectionChanged;
        }
        /// <summary>
        /// Доступна кнопка или нет
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListVariables_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listVariables.SelectedIndex == -1)
            {
                OkButton.IsEnabled = false;
            }else
            {
                OkButton.IsEnabled = true;
            }
        }
        /// <summary>
        /// Выбрать переменную
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (listVariables.SelectedIndex == -1)
            {
                MessageBox.Show("Нужно выбрать переменную");
                return;
            }
            Variable sv = variables.FirstOrDefault(x => x.Name == listVariables.Items[listVariables.SelectedIndex].ToString());
            if (sv == null) {
                MessageBox.Show("Переменная не найдена");
                return;
            }
            selectVariable = sv;
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Отменить выбор
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
