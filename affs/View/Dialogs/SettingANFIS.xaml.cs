﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.ViewModel.SettingForecast;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SettingANFIS.xaml
    /// </summary>
    public partial class SettingANFIS : Window
    {
        private SettingANFISViewModel setting;
        private Model.ForecastANFIS fanfis;

        public SettingANFIS(Model.ForecastANFIS fm)
        {
            InitializeComponent();
            fanfis = fm;
            setting = new SettingANFISViewModel(fanfis);
            DataContext = setting;
        }

        #region Свойства
        /// <summary>
        /// Свойства модели
        /// </summary>
        public SettingANFISViewModel Setting
        {
            get { return setting;}
        }

        #endregion

        /// <summary>
        /// Применение новых свойств
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Отмена применение новых свойств
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {

            DialogResult = false;
            Close();
        }
        #region Функция применения параметров модели
        /// <summary>
        /// Функция применения параметров, заданных в настройке
        /// </summary>
        /// <param name="fm"></param>
        public void setSettings()
        {
            fanfis.AcceptRatio = setting.AcceptRatio;
            fanfis.AndMethod = setting.AndMethod;
            fanfis.Nu = setting.Nu;
            fanfis.NuStep = setting.NuStep;
            fanfis.OrMethod = setting.OrMethod;
            fanfis.Radii = setting.Radii;
            fanfis.RejectRatio = setting.RejectRatio;
            fanfis.SqshFactor = setting.SqshFactor;
        }
        #endregion

    }
}
