﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для WaitBar.xaml
    /// </summary>
    public partial class WaitBar : Window
    {
        private delegate void HideBar();

        Thread reader;
        private List<Variable> dataFromExcel;
        public string pathExcel;

        public List<Variable> DataFromExcel
        {
            get { return dataFromExcel; }
        }

        public WaitBar()
        {
            InitializeComponent();
        }
        public WaitBar(string fileName)
        {
            InitializeComponent();
            pathExcel = fileName;
            Loaded += WaitBar_Loaded;
            loadStatus.Value = 0;
        }

        private void WaitBar_Loaded(object sender, RoutedEventArgs e)
        {
            dataFromExcel = new List<Variable>();
            reader = new Thread(new ParameterizedThreadStart(readDataFormExcel));
            reader.Priority = ThreadPriority.Normal;
            reader.IsBackground = false;    // Не фоновый
            reader.Start(pathExcel);
        }

        private void readDataFormExcel(object path) {
            string filePath = (string)path;
            if (filePath == null) return;
            Excel ex = new Excel(filePath);
            ex.LoadStatus = status;
            List<Variable> vs = ex.readToVariable();  
            ex.Dispose();
            // Если прочитанные есть
            if (vs != null)
            {
                foreach (Variable v in vs)
                {
                    v.upDate();
                    DataFromExcel.Add(v);
                }
            }
            // Вызываем поток удаление окна
            Application.Current.Dispatcher.Invoke(() => {
                Close();
            });
        }

        private void status(int current, int max)
        {
            Application.Current.Dispatcher.Invoke(() => {
                loadStatus.Value = current;
                loadStatus.Maximum = max;
            });
        }
        
        
    }
}
