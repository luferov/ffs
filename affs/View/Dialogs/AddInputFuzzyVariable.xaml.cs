﻿using System.Windows;
using affs.Common;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для AddInputFuzzyVariable.xaml
    /// </summary>
    public partial class AddInputFuzzyVariable : Window
    {
        private string variableName;            // Имя переменной
        private double variableMax;             // Максимальное значение переменной
        private double variableMin;             // Минимальное значение переменной

        private RelayCommand addVariable;       // Добавляем переменную


        public AddInputFuzzyVariable()
        {
            InitializeComponent();
            variableMax = 1;
            variableMin = 0;
            VariableName = string.Empty;
            DataContext = this;
        }

        public string VariableName { get => variableName; set => variableName = value; }
        public double VariableMax { get => variableMax; set => variableMax = value; }
        public double VariableMin { get => variableMin; set => variableMin = value; }
        public RelayCommand AddVariable => addVariable ?? (addVariable = new RelayCommand(obj => {
            DialogResult = true;
            this.Close();
        }, obj => VariableName.Length >= 2 && VariableMax > VariableMin));
    }
}
