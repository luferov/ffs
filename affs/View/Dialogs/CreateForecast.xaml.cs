﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для CreateForecast.xaml
    /// </summary>
    public partial class CreateForecast : Window
    {
        private string nameForecast;
        private string typeOfMethod;
        private string trendMethod;
        private string residuleMethod;
        /// <summary>
        /// Список доступных моделей
        /// </summary>
        Dictionary<string, string> models;
        /// <summary>
        /// Имя прогноза
        /// </summary>
        public string NameForecast
        {
            get => nameForecast; 
        }
        /// <summary>
        /// Тип способа прогнозирования
        /// </summary>
        public string TypeOfMethod
        {
            get => typeOfMethod; 
        }
        /// <summary>
        /// Выбранный метод для прогнозирования трендовой составляющей
        /// </summary>
        public string TrendMethod
        {
            get => trendMethod;
        }
        /// <summary>
        /// Выбранный метод для прогнозирования остаточной составляющей
        /// </summary>
        public string ResiduleMethod
        {
            get => residuleMethod; 
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public CreateForecast()
        {
            InitializeComponent();
            models = new Dictionary<string, string>
            {
                { "На основе искусственной нейронной сети", "FANN" },
                { "На основе адаптивной нейро-нечеткой системы вывода (anfis)", "ANFIS" },
                { "Не основе декомпозиции временных рядов", "DECOMPOSITION" }
            };
            Loaded += CreateForecast_Loaded;
        }

        /// <summary>
        /// Событие при загрузки формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateForecast_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (KeyValuePair<string, string> elem in models)
            {
                ForecastModel.Items.Add(elem.Key);
                if (elem.Value != "DECOMPOSITION") {
                    ForecastModelTrend.Items.Add(elem.Key);
                    ForecastModelResidule.Items.Add(elem.Key);
                }
            }
        }
        /// <summary>
        /// Создание модели
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Create_Click(object sender, RoutedEventArgs e)
        {
            if (ForecastName.Text.Length == 0)
            {
                MessageBox.Show("Имя не задано", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (!models.ContainsKey(ForecastModel.Text))
            {
                MessageBox.Show("Модель не выбрана", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            // Если выбрали метод декомпозиции ряда
            if (models[ForecastModel.Text] == "DECOMPOSITION")
            {
                if (!models.ContainsKey(ForecastModelTrend.Text))
                {
                    MessageBox.Show("Модель для прогнозирования трендовой составляющей не выбрана", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!models.ContainsKey(ForecastModelResidule.Text))
                {
                    MessageBox.Show("Модель для прогнозирования остаточной составляющей не выбрана", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                trendMethod = models[ForecastModelTrend.Text];
                residuleMethod = models[ForecastModelResidule.Text];
            }

            nameForecast = ForecastName.Text;
            typeOfMethod = models[ForecastModel.Text];
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Закрытие диалогового окна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
        
        private void ForecastModel_Selected(object sender, RoutedEventArgs e)
        {
            var comboItem = ForecastModel.SelectedItem;
            switch (models[comboItem.ToString()])
            {
                case "FANN":
                case "ANFIS":
                    Height = 200;
                    DecompositonParametrs.Visibility = Visibility.Collapsed;
                    break;
                case "DECOMPOSITION":
                    Height = 300;
                    DecompositonParametrs.Visibility = Visibility.Visible;
                    break;
                default:
                    MessageBox.Show("Выбранная модель не найдена");
                    break;
            }
        }
    }
}
