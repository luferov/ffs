﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для Select2Variables.xaml
    /// </summary>
    public partial class Select2Variables : Window
    {

        private List<Variable> allVariables;        // Список всех переменных  
        private List<Variable> selectedX;           // Список выбранных переменных оси абсцисс
        private List<Variable> selectedY;           // Список выбранных переменных оси ординат
        private int countX = 0;                     // Количество выбранных переменных оси абсцисс
        private int countY = 0;                     // Количество выбранных переменных оси ординат

        #region Свойства
        /// <summary>
        /// Все переменные
        /// </summary>
        public List<Variable> AllVariables
        {
            get { return allVariables; }
            set { allVariables = value; }
        }
        /// <summary>
        /// Выбранные переменные по оси абсцисс
        /// </summary>
        public List<Variable> SelectedX
        {
            get { return selectedX; }
            set { selectedX = value; }
        }
        /// <summary>
        /// Выбранные переменные по оси ординат
        /// </summary>
        public List<Variable> SelectedY
        {
            get { return selectedY; }
            set { selectedY = value; }
        }
        /// <summary>
        /// Количество требуемых выбранных переменных по оси абсцисс
        /// 0 - нет ограничений
        /// </summary>
        public int CountX
        {
            get { return countX; }
            set { countX = value; }
        }
        /// <summary>
        /// Количество требуемых выбранных переменных по оси ординат
        /// </summary>
        public int CountY
        {
            get { return countY; }
            set { countY = value; }
        }
#endregion
        public Select2Variables()
        {
            InitializeComponent();
            SelectedX = new List<Variable>();
            SelectedY = new List<Variable>();
        }

        public Select2Variables(ObservableCollection<Variable> variables):this()
        {
            AllVariables = new List<Variable>();
            foreach(Variable v in variables)
            {
                leftVariables.Items.Add(v.Name);
                AllVariables.Add(v);
            }
        }
        /// <summary>
        /// Закрытие формы в выбором
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (countX != rightVariablesX.Items.Count)
            {
                MessageBox.Show("Количество переменных оси абсцисс не равно: " + countX.ToString(),
                    "Ошибка формирования переменных",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            if (rightVariablesY.Items.Count == 0)
            {
                MessageBox.Show("Количество переменных оси ординат должно быть больше 0",
                    "Ошибка формирования переменных",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            selectedX.Clear();
            selectedY.Clear();
            foreach (string item in rightVariablesX.Items)
            {
                Variable v = findByName(item);
                if (v != null)
                {
                    selectedX.Add(v);
                }
            }

            foreach (string item in rightVariablesY.Items)
            {
                Variable v = findByName(item);
                if (v != null)
                {
                    selectedY.Add(v);
                }

            }
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Закрытие формы без выбора
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        /// <summary>
        /// Добавление переменных в ось абсцисс
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addX_Click(object sender, RoutedEventArgs e)
        {
            int li = leftVariables.SelectedIndex;
            // Если выбран
            if (li != -1)
            {
                if (!rightVariablesX.Items.Contains(leftVariables.Items[li].ToString()))
                {
                    rightVariablesX.Items.Add(leftVariables.Items[li].ToString());
                }
                leftVariables.SelectedIndex = -1;
            }
        }
        /// <summary>
        /// Добавление переменных в ось ординат
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addY_Click(object sender, RoutedEventArgs e)
        {
            int li = leftVariables.SelectedIndex;
            // Если выбран
            if (li != -1)
            {
                if (!rightVariablesY.Items.Contains(leftVariables.Items[li].ToString()))
                {
                    rightVariablesY.Items.Add(leftVariables.Items[li].ToString());
                }
                leftVariables.SelectedIndex = -1;
                
            }
            else
            {
                // Переносим всех, кроме тех, что в х
                foreach (string item in leftVariables.Items)
                {
                    // Если нет в х
                    if (!rightVariablesX.Items.Contains(item))
                    {
                        rightVariablesY.Items.Add(item);
                    }
                }
            }
        }
        /// <summary>
        /// Удаление переменных из оси абсцисс
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeX_Click(object sender, RoutedEventArgs e)
        {
            int li = rightVariablesX.SelectedIndex;
            if (li == -1)
            {
                rightVariablesX.Items.Clear();
            }
            else
            {
                rightVariablesX.Items.RemoveAt(li);
            }
        }
        /// <summary>
        /// Удаление переменных из оси ординат
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeY_Click(object sender, RoutedEventArgs e)
        {
            int li = rightVariablesY.SelectedIndex;
            if (li == -1)
            {
                rightVariablesY.Items.Clear();
            }
            else
            {
                rightVariablesY.Items.RemoveAt(li);
            }
        }

        private Variable findByName(string name)
        {
            foreach (Variable v in AllVariables)
            {
                if (v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }

    }
}
