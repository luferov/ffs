﻿using affs.Common;
using affs.ViewModel;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для AddictionMF.xaml
    /// </summary>
    public partial class AddictionMF : Window
    {
        AddictionMFViewModel amf;
        public AddictionMF(IMultiForecast method, ObservableCollection<IMultiForecast> methods)
        {
            InitializeComponent();
            amf = new AddictionMFViewModel(method, methods);
            DataContext = amf;
        }
        private void Ok_Click(object sender, RoutedEventArgs e)
        {

            // inputRegression = amf.BuildRegression();
            Close();
        }
    }
}
