﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Model;
using System.Collections.ObjectModel;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для AddRandomVariable.xaml
    /// </summary>
    public partial class AddRandomVariable : Window
    {
        private Variable variable;
        private double _start = 0.0;
        private double _end = 100.0;
        private int countAfterDot = 0;
        private int _count = 10;

        /// <summary>
        /// Сгенерированная переменная
        /// </summary>
        public Variable Variable
        {
            get { return variable; }
            set { variable = value; }
        }
        /// <summary>
        /// Начало диапазона
        /// </summary>
        public double Start
        {
            get { return _start; }
            set { _start = value;}
        }
        /// <summary>
        /// Конец диапазона
        /// </summary>
        public double End
        {
            get { return _end; }
            set { _end = value; }
        }
        /// <summary>
        /// Количество элементов после запятой
        /// </summary>
        public int CountAfterDot
        {
            get { return countAfterDot; }
            set { countAfterDot = value; }
        }
        /// <summary>
        /// Количество элементов в ряде
        /// </summary>
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        public AddRandomVariable()
        {
            InitializeComponent();
            DataContext = this;
        }
        /// <summary>
        /// Создание переменной в заданном диапазоне
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            ObservableCollection<double> v = new ObservableCollection<double>();
            variable = new Variable();
            for (int i = 0; i < Count; i++)
            {
                double k = Start + (End - Start) * rnd.NextDouble();
                v.Add(Math.Round(k, CountAfterDot));
            }
            variable.Range = v;
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Отмена создания
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            Variable = null;
            DialogResult = false;
            Close();
        }
    }
}
