﻿using affs.Common;
using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для Forecasting.xaml
    /// </summary>
    public partial class Forecasting : Window
    {
        private Thread thr;
        private Forecast f;
        private int letHorizont;
        private bool useNormalization;
        private ObservableCollection<Variable> forecastVariables;

        #region Свойства
        public ObservableCollection<Variable> ForecastVariables
        {
            get { return forecastVariables; }
        }

        public bool UseNormalization
        {
            get { return useNormalization; }
        }

        public Forecast F
        {
            get { return f; }
        }
        #endregion

        #region Конструктор
        public Forecasting(Forecast _fm, bool _useNormalization, int _horizont)
        {
            InitializeComponent();
            f = _fm;
            letHorizont = _horizont;
            forecastingStatus.Maximum = letHorizont;
            useNormalization = _useNormalization;
            Loaded += Forecasting_Loaded;

        }
        #endregion

        private void Forecasting_Loaded(object sender, RoutedEventArgs e)
        {
            F.DataPreparation.buildTrainData();         // Строим зависимость
            forecastVariables = new ObservableCollection<Variable>();
            foreach (KeyValuePair<Variable, List<int>> elem in F.DataPreparation.OutputRegress)
            {
                Variable va = new Variable(elem.Key.Name);
                forecastVariables.Add(va);
            }

            ///
            /// Если система расчитана на декомпозицию временного рада
            /// То необходимо при каждом запуске обнулять ретроспективную базу
            /// Специфика метода декомпозиции временного ряда
            ///
            if (F.ForecastMethod is ForecastCombine)
            {
                ((ForecastCombine)F.ForecastMethod).ResetForecastData();
            }
            ///
            /// Выполняем прогноз
            ///
            //DoForecasting();
            thr = new Thread(DoForecasting);
            thr.Priority = ThreadPriority.Highest;
            thr.Start();
        }


        private void DoForecasting() {

            while (letHorizont > 0)
            {                   // Пока горизонт есть
                double[] runData;
                // Используем ли нормализацию
                if (UseNormalization)
                {
                    runData = F.DataPreparation.getNDataForForecating();
                }
                else
                {
                    runData = F.DataPreparation.getDataForForecasting();
                }
                // Если нет следующей обучающей выборки, то выходим из цикла
                if (runData == null)
                {
                    break;
                }

                // Получаем результат
                double? resultFm = F.ForecastMethod.Run(runData)[0];    // Получаем значение
                                                                        // Если следующего значения нет, то выбрасываем
                if (resultFm == null)
                {
                    break;
                }
                // Получаем имя первой переменной
                double fr;
                string nameFR = forecastVariables[0].Name;
                if (UseNormalization)
                {
                    // Добавляем нормированные данные
                    F.DataPreparation.addNVariableRange(
                        F.DataPreparation.Normalization.findByName(nameFR),
                        (double)resultFm
                    );
                    // Если используем нормировку, необходимо преобразовывать данные
                    fr = F.DataPreparation.Normalization.getNumberByNormalization(
                        F.DataPreparation.Normalization.findByName(nameFR),
                        (double)resultFm
                    );
                }
                else
                {
                    // Если нормализация не используется
                    F.DataPreparation.addVariableRange(F.DataPreparation.Normalization.findByName(nameFR), (double)resultFm);
                    fr = (double)resultFm;
                }
                // Добавляем ряд
                forecastVariables[0].Range.Add(Math.Round(fr, Properties.Settings.Default.Digits));
                letHorizont--;
                Application.Current.Dispatcher.Invoke(()=> {
                    forecastingStatus.Value++;
                });
            }

            ///
            /// Прогноз закончен
            ///

            foreach (Variable v in forecastVariables)
            {
                v.upDate();
            }
            Application.Current.Dispatcher.Invoke(()=> {
                Close();
            });
        }
    }
}
