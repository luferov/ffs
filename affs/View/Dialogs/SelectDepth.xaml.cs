﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SelectDepth.xaml
    /// </summary>
    public partial class SelectDepth : Window
    {
        public int Depth { get; set; }
        private int maximumAutoCorrelation;

        public SelectDepth(int s = 3, int maxAC = 10)
        {
            InitializeComponent();
            // Назнчаем после генерации, чтобы не было ошибки
            coeffAC.Text = s.ToString();
            coeffAC.TextChanged += coeffAC_TextChanged;
            Depth = s;
            maximumAutoCorrelation = maxAC;
        }
        

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void coeffAC_TextChanged(object sender, TextChangedEventArgs e)
        {
            int s;
            if (int.TryParse(coeffAC.Text, out s))
            {
                if (maximumAutoCorrelation > s && s > 1)
                {
                    Ok_Button.IsEnabled = true;
                    Depth = s;
                }
                else
                {
                    Ok_Button.IsEnabled = false;
                }
            }
            else
            {
                Ok_Button.IsEnabled = false;
            }
        }
    }
}
