﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Common;
using affs.ViewModel.SettingForecast;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для MFCorrelationView.xaml
    /// </summary>
    public partial class MFCorrelationView : Window
    {
        MFCorrelationViewModel mfv;
        public MFCorrelationView(IMultiForecast imf, ObservableCollection<IMultiForecast> Methods)
        {
            InitializeComponent();
            mfv = new MFCorrelationViewModel(imf, Methods);
            DataContext = mfv;

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (Window w in mfv.ChildrenWindow)
            {
                w.Close();
            }
        }
    }
}
