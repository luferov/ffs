﻿using affs.Common;
using affs.Model;
using FuzzyLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SettingCombine.xaml
    /// </summary>
    public partial class SettingCombine : Window
    {
        private int fs;
        private double[] dataOutput;
        private Model.ForecastCombine forecast;

        public int Fs
        {
            get { return fs; }
        }
        

        public SettingCombine(double[][] trainDataOutput, ForecastCombine f)
        {
            InitializeComponent();
            forecast = f;
            fs = f.FuzzyStep;
            fuzzyStep.Text = f.FuzzyStep.ToString();

            dataOutput = new double[trainDataOutput.Length];
            for (int i = 0; i < dataOutput.Length; i++)
            {
                dataOutput[i] = trainDataOutput[i][0];
            }
            drawChart();
            FTApproximation(fs);
            fuzzyStep.TextChanged += fuzzyStep_TextChanged;
        }

        private void drawChart() {
            chart.ChartAreas.Add(new ChartArea("default"));
            chart.Series.Clear();
            string name = "graphs";
            chart.Series.Add(name);
            chart.Series[name].LegendText = "Выходные данные";
            chart.Series[name].BorderWidth = 3;
            chart.Series[name].ChartType = SeriesChartType.Spline;
            for (int j = 0; j < dataOutput.Length; j++)
            {
                chart.Series[name].Points.AddXY(j, dataOutput[j]);
            }
        }

        private void trend_Click(object sender, RoutedEventArgs e)
        {
            settingForecastSystem(forecast.TrendForecast);
        }

        private void residule_Click(object sender, RoutedEventArgs e)
        {
            settingForecastSystem(forecast.ResidualForecast);
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
        /// <summary>
        /// Изменения значения нечеткого разбиения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fuzzyStep_TextChanged(object sender, TextChangedEventArgs e)
        {
            int _fs;
            if (int.TryParse(fuzzyStep.Text, out _fs))
            {
                if (_fs > 0)
                {
                    OkButton.IsEnabled = true;
                    fs = _fs;
                    ClearApproximation();
                    FTApproximation(fs);
                }
                else
                {
                    OkButton.IsEnabled = false;
                }
            }else
            {
                OkButton.IsEnabled = false;
            }
        }

        private void FTApproximation(int cn)
        {
            string prefix = "approx_";
            // Выполняем разбиение
            double[] x, y;                                      // Значения x и y
            List<Series> ApproximationSeries = new List<Series>();
            foreach (Series series in chart.Series)
            {
                string nameApprox = prefix + series.Name;       // Имя аппроксимации
                x = new double[series.Points.Count];
                y = new double[series.Points.Count];
                for (int i = 0; i < x.Length; i++)
                {
                    DataPoint point = series.Points[i];
                    x[i] = point.XValue;
                    y[i] = point.YValues[0];
                }

                
                FuzzyTransform ft = new FuzzyTransform(y, cn);
                ft.Run();
                Array.Sort(x);
                x.Distinct();
                Series s = new Series(nameApprox);
                s.LegendText = string.Empty;
                s.IsVisibleInLegend = false;
                s.BorderWidth = 3;
                s.Tag = "ApproximationLine";
                s.ChartType = SeriesChartType.Spline;
                s.Color = System.Drawing.Color.Black;
                s.BorderDashStyle = ChartDashStyle.Dash;
                for (int i = 0; i < x.Length; i++)
                {
                    s.Points.AddXY(x[i], ft.TimeSeriesInverse[i]);
                }
                ApproximationSeries.Add(s);
            }
            foreach (Series _s in ApproximationSeries)
            {
                chart.Series.Add(_s);
            }
        }
        /// <summary>
        /// Удаление всех аппроксимаций
        /// </summary>
        private void ClearApproximation()
        {
            List<Series> removeSeries = new List<Series>();
            foreach (Series series in chart.Series)
            {
                if ((string)series.Tag == "ApproximationLine")
                {
                    removeSeries.Add(series);
                }
            }
            foreach (Series series in removeSeries)
            {
                chart.Series.Remove(series);
            }
        }
        /// <summary>
        /// Настройки системы прогнозирования
        /// </summary>
        /// <param name="fm"></param>
        private void settingForecastSystem(IForecast fm)
        {
            if (fm is ForecastANN)
            {
                List<Layer> l = new List<Layer>(((ForecastANN)fm).Layers);
                SettingANN setting = new SettingANN(l);
                if (setting.ShowDialog() == true)
                {
                    ((ForecastANN)fm).Layers = setting.Layers;
                }
            }
            else if (fm is ForecastANFIS)
            {
                // Настройка системы на основе ANFIS
                SettingANFIS setting = new SettingANFIS((ForecastANFIS)fm);
                if (setting.ShowDialog() == true)
                {
                    setting.setSettings();
                }
            }
            else
            {
                MessageBox.Show("Метод прогнозирования не найден.");
                return;
            }
        }
    }
}
