﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для Addition.xaml
    /// </summary>
    public partial class Addiction : Window
    {
        private Dictionary<Variable, List<int>> inputAutoRegression;
        private Dictionary<Variable, List<int>> outputAutoRegression;
        private ViewModel.AddictionViewModel avm;

        public Addiction(ObservableCollection<Variable> _variables,
                        Dictionary<Variable,List<int>> ir,
                        Dictionary<Variable, List<int>> or)
        {
            InitializeComponent();
            avm = new ViewModel.AddictionViewModel(_variables, ir, or);
            DataContext = avm;
        }

        #region Свойства
        /// <summary>
        /// Входные параметры авторегрессии
        /// </summary>
        public Dictionary<Variable, List<int>> InputAutoRegression
        {
            get { return inputAutoRegression; }
        }
        /// <summary>
        /// Выходные параметры авторегрессии
        /// </summary>
        public Dictionary<Variable, List<int>> OutputAutoRegression
        {
            get { return outputAutoRegression; }
        }

        #endregion
        /// <summary>
        /// Применение свойств
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            string msg;
            if (avm.Validate(out msg))
            {
                avm.BuildAutoRegression();
                inputAutoRegression = avm.AutoregressInput;
                outputAutoRegression = avm.AutoregressOutput;
                DialogResult = true;
                Close();
            }
            else
            {
                MessageBox.Show(msg,"Ошибка заполнения зависимсоти",MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }
        /// <summary>
        /// Закрытие окна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

    }
}
