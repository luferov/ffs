﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Common;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для InputName.xaml
    /// </summary>
    public partial class InputName : Window
    {
        private string chosenName;
        private RelayCommand chose;
        public InputName()
        {
            InitializeComponent();
            DataContext = this;
            ChosenName = string.Empty;
        }

        public InputName(string oldName) : base()
        {
            ChosenName = oldName;
        }

        public string ChosenName { get => chosenName; set => chosenName = value; }
        public RelayCommand Chose => chose ?? (chose = new RelayCommand(obj => {
            DialogResult = true;
            Close();
        }, obj => ChosenName.Length > 3));
    }
}
