﻿using FuzzyLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ShowSugenoVariables.xaml
    /// </summary>
    public partial class ShowSugenoVariables : Window
    {
        public ShowSugenoVariables(List<FuzzyVariable> fvs, SugenoVariable sv)
        {
            InitializeComponent();
            Title = "Лингвистические термы нечеткой переменной " + sv.Name;
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Имя терма"));
            dt.Columns.Add(new DataColumn("Зависимость"));
            foreach (ISugenoFunction sf in sv.Functions)
            {
                DataRow dr = dt.NewRow();
                dr[0] = sf.Name;
                dr[1] = getDependency(fvs, sf);
                dt.Rows.Add(dr);
            }
            sugenoVariables.ItemsSource = dt.DefaultView;
        }
        /// <summary>
        /// Выстраивание зависимостей
        /// </summary>
        /// <param name="fvs"></param>
        /// <param name="sf"></param>
        /// <returns></returns>
        private string getDependency(List<FuzzyVariable> fvs, ISugenoFunction sf)
        {
            string dependency = string.Empty;
            if (sf is LinearSugenoFunction)
            {
                LinearSugenoFunction lsf = sf as LinearSugenoFunction;
                // Константа
                dependency += Math.Round(lsf.ConstValue,Properties.Settings.Default.Digits).ToString();
                // Остальные коэффициенты
                foreach(FuzzyVariable fv in fvs)
                {
                    double coeff = lsf.GetCoefficient(fv);
                    if (coeff > 0)
                    {
                        dependency += "+" + Math.Round(coeff,Properties.Settings.Default.Digits) + "*" + fv.Name;
                    }else
                    {
                        dependency += Math.Round(coeff, Properties.Settings.Default.Digits) + "*" + fv.Name;
                    }
                }
            }
            return dependency;
        }
        /// <summary>
        /// Закрыть окно
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
