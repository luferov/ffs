﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Model;
using System.Collections.ObjectModel;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для Iterator.xaml
    /// </summary>
    public partial class Iterator : Window
    {
        private Variable variable;
        private double _start = 0.0;
        private double _step = 0.0;
        private double _end = 0.0;

        /// <summary>
        /// Начало
        /// </summary>
        public double Start
        {
            get { return _start; }
            set { _start = value; }
        }
        /// <summary>
        /// Шаг
        /// </summary>
        public double Step
        {
            get { return _step; }
            set { _step = value; }
        }
        /// <summary>
        /// Конец итераци
        /// </summary>
        public double End
        {
            get { return _end; }
            set { _end = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Variable Variable
        {
            get { return variable; }
            set { variable = value; }
        }

        public Iterator()
        {
            InitializeComponent();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if (!double.TryParse(start.Text, out _start)) {
                MessageBox.Show("Начальное значение должно быть числом!");
                return;
            }
            if (!double.TryParse(step.Text, out _step)) {
                MessageBox.Show("Значение шага должно быть числом!");
                return;
            }
            if (!double.TryParse(end.Text, out _end))
            {
                MessageBox.Show("Конечное значение должно быть числом!");
                return;
            }
            if (_step == 0) {
                MessageBox.Show("Шаг не может быть нулевым!");
                return;
            }
            if (_step > 0 && _end < _start)
            {
                MessageBox.Show("При положительном шаге начальное значение не может быть больше конечного!");
                return;
            }
            if (_step < 0 && _end > _start)
            {
                MessageBox.Show("При отрицательном шаге начальное значение не может быть меньше конечного!");
                return;
            }
            ObservableCollection<double> range = new ObservableCollection<double>();
            if (_step > 0)
            {
                for (double i = _start; i <= _end;)
                {
                    range.Add(i);
                    i += _step;
                }
            }else
            {
                for (double i = _start; i>= _end;)
                {
                    range.Add(i);
                    i += _step;
                }
            }
            variable = new Variable();
            variable.Range = range;
            DialogResult = true;
            this.Close();
        }

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }
        
       
        
    }
}
