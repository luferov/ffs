﻿using affs.Common;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Model;
using FuzzyLogic;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ClusterAnalysis.xaml
    /// </summary>
    public partial class ClusterAnalysis : Window
    {
        public ClusterAnalysis(IMultiForecast method)
        {
            InitializeComponent();
            DataContext = new ViewModel.FuzzySystems.ClusterAnalysisViewModel(method);
        }
    }
}
