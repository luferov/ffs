﻿using affs.Model;
using FANNCSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для SettingANN.xaml
    /// </summary>
    public partial class SettingANN : Window
    {
        private List<Layer> layres = new List<Layer>();
        private ViewModel.SettingForecast.SettingANNViewModel sannvm;
        public SettingANN(List<Layer> layers)
        {
            InitializeComponent();
            sannvm = new ViewModel.SettingForecast.SettingANNViewModel(layers);
            DataContext = sannvm;
        }

        public List<Layer> Layers
        {
            get { return layres; }
        }
        /// <summary>
        /// Применение параметров ИНС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {

            layres = sannvm.buildLayer();
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Отмена редактирования слоев ИНС
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
        
    }
}
