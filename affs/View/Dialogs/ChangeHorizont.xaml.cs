﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangeHorizont.xaml
    /// </summary>
    public partial class ChangeHorizont : Window
    {
        private int h;
        private int _horizont;
        /// <summary>
        /// Горизонт прогнозирования
        /// </summary>
        public int Horizont
        {
            get { return _horizont; }
        }

        public ChangeHorizont(int _h)
        {
            InitializeComponent();
            h = _h;
            if (h == -1)
            {
                tbHorizont.Text = "10";
            }else
            {
                tbHorizont.Text = h.ToString();
            }
            // Обработчик
            tbHorizont.TextChanged += tbHorizont_TextChanged;
        }
        /// <summary>
        /// Применить
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            int letHorizont;
            if (!int.TryParse(tbHorizont.Text, out letHorizont))
            {
                MessageBox.Show("Горизонт прогнозирования должен быть целым числом");
                return;
            }
            if (h == -1 && letHorizont <= 0)
            {
                MessageBox.Show("Горизонт прогнозирования должен быть положительным");
                return;
            }
            if ((1 > letHorizont || letHorizont > h) && h > 0)
            {
                MessageBox.Show(
                    string.Format("Горизонт прогнозирования должен быть в пределах:[{0},{1}]", 1, h)
                );
                return;
            }
            _horizont = letHorizont;
            DialogResult = true;
            Close();
        }
        /// <summary>
        /// Закрытие окна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void tbHorizont_TextChanged(object sender, TextChangedEventArgs e)
        {
            int hor;
            if (int.TryParse(tbHorizont.Text, out hor)) {
                if (hor > 0) {
                    OkButton.IsEnabled = true;
                }
                else {
                    OkButton.IsEnabled = false;
                }
            }
            else
            {
                OkButton.IsEnabled = false;
            }
        }
    }
}
