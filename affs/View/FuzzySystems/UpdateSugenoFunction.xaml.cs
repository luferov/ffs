﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FuzzyLogic;
using affs.Common;
using affs.Model;
using System.Collections.ObjectModel;

namespace affs.View.FuzzySystems
{
    /// <summary>
    /// Логика взаимодействия для UpdateSugenoFunction.xaml
    /// </summary>
    public partial class UpdateSugenoFunction : Window
    {
        private SugenoFuzzySystem sugeno;
        private LinearSugenoFunction linearSugenoFunction;

        private double constValue;
        private ObservableCollection<FuzzyCoeff> coeffs;

        private string action;
        private string actionButton;

        private string nameMF;
        private string oldName;

        private RelayCommand saveMF;

        public UpdateSugenoFunction(SugenoFuzzySystem sugeno)
        {
            InitializeComponent();
            Sugeno = sugeno;
            nameMF = "";
            oldName = "";
            action = "Добавление функции принадлежности";
            actionButton = "Добавить функцию принадлежности";
            Coeffs = new ObservableCollection<FuzzyCoeff>();
            constValue = 0.0;                                               // Константа по умолчанию равна 0           
            foreach (FuzzyVariable fv in Sugeno.Input)
            {
                Coeffs.Add(new FuzzyCoeff(fv));                                        // По умолчанию 0
            }
            DataContext = this;
        }

        public UpdateSugenoFunction(SugenoFuzzySystem sugeno, LinearSugenoFunction sf)
        {
            InitializeComponent();
            LinearSugenoFunction = sf;
            nameMF = sf.Name;
            oldName = nameMF;
            Sugeno = sugeno;
            action = "Изменение функции принадлежности";
            actionButton = "Изменить функцию принадлежности";
            Coeffs = new ObservableCollection<FuzzyCoeff>();
            ConstValue = sf.ConstValue;
            foreach (FuzzyVariable fv in Sugeno.Input)
            {
                Coeffs.Add(new FuzzyCoeff(fv, sf.GetCoefficient(fv)));
            }
            DataContext = this;
        }

        #region Свойства
        public SugenoFuzzySystem Sugeno { get => sugeno; set => sugeno = value; }
        public string Action { get => action; set => action = value; }
        public string ActionButton { get => actionButton; set => actionButton = value; }
        public string OldName { get => oldName; set => oldName = value; }
        public double ConstValue { get => constValue; set => constValue = value; }
        public ObservableCollection<FuzzyCoeff> Coeffs { get => coeffs; set => coeffs = value; }
        public Dictionary<FuzzyVariable, double> CoeffsDictionary
        {
            get
            {
                Dictionary<FuzzyVariable, double> result = new Dictionary<FuzzyVariable, double>();
                foreach (FuzzyCoeff fc in Coeffs)
                {
                    result.Add(fc.FuzzyVariable, fc.Value);
                }
                return result;
            }
        }
        public LinearSugenoFunction LinearSugenoFunction { get => linearSugenoFunction; set => linearSugenoFunction = value; }
        public string NameMF { get => nameMF; set => nameMF = value; }
        #endregion
        #region Методы
        /// <summary>
        /// Добавляем или сохраняем функцию принадлежности
        /// </summary>
        public RelayCommand SaveMF => saveMF ?? (saveMF = new RelayCommand(obj => {
            DialogResult = true;
            this.Close();
        }, obj => NameMF.Length >= 3));
        #endregion
    }
}
