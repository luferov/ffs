﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.Model;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FuzzyLogic;
using affs.ViewModel.FuzzySystems;

namespace affs.View.FuzzySystems
{
    /// <summary>
    /// Логика взаимодействия для EditFuzzyVariable.xaml
    /// </summary>
    public partial class EditFuzzyVariable : Window
    {

        private readonly string oldName;

        public EditFuzzyVariable(FuzzyVariable fv)
        {
            InitializeComponent();
            oldName = fv.Name;
            DataContext = new EditFuzzyVariableViewModel(fv);
        }

        /// <summary>
        /// Старое название переменной
        /// </summary>
        public string OldName => oldName;
    }
}
