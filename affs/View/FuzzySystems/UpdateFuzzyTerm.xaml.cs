﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.Common;
using FuzzyLogic;

namespace affs.View.FuzzySystems
{
    /// <summary>
    /// Логика взаимодействия для UpdateFuzzyTerm.xaml
    /// </summary>
    public partial class UpdateFuzzyTerm : Window
    {
        private readonly string action;
        private readonly string actionButton;

        private IMembershipFunction imf;

        private string nameTerm;

        private readonly string oldName;
        private readonly bool canChangeName;

        private Dictionary<string, string> memberShipFunctions = new Dictionary<string, string>() {
            { "Треугольная функция принадлежности", "TriangularMembershipFunction"},
            { "Трапециевидная функция принадлежности", "TrapezoidMembershipFunction" },
            { "Гауссовская функция принадлежности", "NormalMembershipFunction"},
            // { "Сигмоидная функция принадлежности", "SigmoidalMembershipFunction"}
        };
        KeyValuePair<string, string> choseMemberShipFunction;
        private string choseMfs;


        private string mfsParameters;

        private bool check = false;

        private RelayCommand saveTerm;

        public UpdateFuzzyTerm()
        {
            InitializeComponent();
            action = "Добавление терма";
            actionButton = "Добавить терм";
            canChangeName = true;
            DataContext = this;
        }

        public UpdateFuzzyTerm(FuzzyTerm ft)
        {
            InitializeComponent();
            action = "Изменение терма";
            actionButton = "Изменить терм";

            canChangeName = false;
            oldName = ft.Name;
            NameTerm = ft.Name;
            if (ft.MembershipFunction is TriangularMembershipFunction trmf)
            {
                ChoseMfs = "TriangularMembershipFunction";
                MfsParameters = string.Format("{0} {1} {2}", trmf.X1, trmf.X2, trmf.X3).Replace(',', '.');
            }
            if (ft.MembershipFunction is TrapezoidMembershipFunction tpmf)
            {
                ChoseMfs = "TrapezoidMembershipFunction";
                MfsParameters = string.Format("{0} {1} {2} {3}", tpmf.X1, tpmf.X2, tpmf.X3, tpmf.X4).Replace(',', '.');
            }
            if (ft.MembershipFunction is NormalMembershipFunction nmf)
            {
                ChoseMfs = "NormalMembershipFunction";
                MfsParameters = string.Format("{0} {1}", nmf.B, nmf.Sigma).Replace(',', '.');
            }
            /**
             * if (ft.MembershipFunction is SigmoidalMembershipFunction smf)
             * {
             *     ChoseMfs = "SigmoidalMembershipFunction";
             *     MfsParameters = string.Format("{0} {1}", smf.A, smf.C).Replace(',', '.');
             * }
             */
            foreach (KeyValuePair<string, string> kv in memberShipFunctions)
            {
                if (kv.Value == choseMfs)
                {
                    choseMemberShipFunction = kv;
                    break;
                }
            }
            DataContext = this;
        }

        /// <summary>
        /// Сохраняем терм
        /// </summary>
        public RelayCommand SaveTerm => saveTerm ?? (saveTerm = new RelayCommand(obj => {
            string[] parametes = MfsParameters.Split(' ');
            double[] x = new double[parametes.Length];
            //
            // Проверка
            //
            bool flag = true;
            for (int i = 0; i < parametes.Length; i++)
            {
                flag &= double.TryParse(parametes[i].Replace('.', ','), out x[i]);
            }
            if (!flag)
            {
                MessageBox.Show("Возможно, числа указаны неверно.", "Ошибка приведения", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            //
            // Формирование функций принадлежности
            //
            try
            {
                switch (ChoseMfs)
                {
                    case "TriangularMembershipFunction":
                        Imf = new TriangularMembershipFunction(x[0], x[1], x[2]);
                        break;
                    case "TrapezoidMembershipFunction":
                        Imf = new TrapezoidMembershipFunction(x[0], x[1], x[2], x[3]);
                        break;
                    case "NormalMembershipFunction":
                        Imf = new NormalMembershipFunction(x[0], x[1]);
                        break;
                    // case "SigmoidalMembershipFunction":
                    //     Imf = new SigmoidalMembershipFunction(x[0], x[1]);
                    //     break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DialogResult = true;
            Close();
        }, obj => Check && NameTerm.Length >= 3));
        /// <summary>
        /// Проверяем правильность заполнения параметров
        /// </summary>
        /// <returns></returns>
        private bool Validate()
        {
            string[] parametrs = MfsParameters.Split(' ');
            double val;
            for (int i = 0; i < parametrs.Length; i++)
            {
                if (!double.TryParse(parametrs[i].Replace('.', ','), out val)) return false;
            }
            switch (ChoseMfs)
            {
                case "TriangularMembershipFunction":
                    if (parametrs.Length != 3) return false;
                    return true;
                case "TrapezoidMembershipFunction":
                    if (parametrs.Length != 4) return false;
                    return true;
                case "NormalMembershipFunction":
                case "SigmoidalMembershipFunction":
                    if (parametrs.Length != 2) return false;
                    return true;
                default:
                    return false;
            }
        }
        public string Action => action;
        public string ActionButton => actionButton;
        public bool CanChangeName => canChangeName;
        public string NameTerm { get => nameTerm; set => nameTerm = value; }
        public Dictionary<string, string> MemberShipFunctions { get => memberShipFunctions; set => memberShipFunctions = value; }
        public KeyValuePair<string, string> ChoseMemberShipFunction
        {
            get => choseMemberShipFunction;
            set
            {
                choseMemberShipFunction = value;
                ChoseMfs = value.Value;
            }
        }
        public string MfsParameters
        {
            get => mfsParameters;
            set
            {
                mfsParameters = value;
                Check = Validate();
            }
        }
        public IMembershipFunction Imf { get => imf; set => imf = value; }
        public bool Check { get => check; set => check = value; }
        public string ChoseMfs { get => choseMfs; set => choseMfs = value; }
        public string OldName => oldName;
    }
}
