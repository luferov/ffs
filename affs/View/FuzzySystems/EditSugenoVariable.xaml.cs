﻿using System.Windows;
using affs.ViewModel.FuzzySystems;
using FuzzyLogic;

namespace affs.View.FuzzySystems
{
    /// <summary>
    /// Логика взаимодействия для EditSugenoVariable.xaml
    /// </summary>
    public partial class EditSugenoVariable : Window
    {
        private readonly string oldName;
        public EditSugenoVariable(SugenoVariable sugenoVariable, SugenoFuzzySystem sugeno)
        {
            InitializeComponent();
            oldName = sugenoVariable.Name;
            DataContext = new EditSugenoVariableViewModel(sugenoVariable, sugeno);
        }

        public string OldName => oldName;
    }
}
