﻿using affs.ViewModel.FuzzySystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FuzzyLogic;

namespace affs.View.FuzzySystems
{
    /// <summary>
    /// Логика взаимодействия для SettingsFuzzySystems.xaml
    /// </summary>
    public partial class SettingsFuzzySystems : Window
    {
        private string oldName;
        public SettingsFuzzySystems(SugenoFuzzySystem fm)
        {
            InitializeComponent();
            oldName = "Модель";
            DataContext = new SettingsFuzzyModelViewModel(fm);
        }

        public string OldName { get => oldName; set => oldName = value; }
    }
}
