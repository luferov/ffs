﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using affs.ViewModel;
using System.Threading;

namespace affs.View
{
    /// <summary>
    /// Логика взаимодействия для Train.xaml
    /// Диалоговое окно обучения и прогнозирование нейронечеткой системы
    /// Логика обучения лежит во ViewModel
    /// В качестве входного параметра принимается модель прогнозирования
    /// </summary>
    public partial class Train : Window
    {
        private Model.Forecast F;
        Thread train;
        List<double> errorTrain;

        public Train(Model.Forecast f)
        {
            InitializeComponent();
            F = f;
            errorTrain = new List<double>();
            PrBar.Maximum = f.ForecastMethod.Epochs;
            Loaded += Train_Loaded;
        }

        private void Train_Loaded(object sender, RoutedEventArgs e)
        {
            tbValid.FontWeight = FontWeights.Bold;
            F.DataPreparation.buildTrainData();
            // Выбор обучеющей выборки
            if (F.UseNoralization)
            {
                F.DataPreparation.Normalization.setK(F.K);
                // Если мы используем нормализацию
                F.ForecastMethod.TrainDataInput = F.DataPreparation.TrainDataNInput;
                F.ForecastMethod.TrainDataOutput = F.DataPreparation.TrainDataNOutput;
            }
            else
            {
                // Если мы не импользуем нормировку
                F.ForecastMethod.TrainDataInput = F.DataPreparation.TrainDataInput;
                F.ForecastMethod.TrainDataOutput = F.DataPreparation.TrainDataOutput;
            }
            
            F.ForecastMethod.PrintTrainReport = trainReport;

            if (F.ForecastMethod.Validate(out string msg))
            {
                tbInit.FontWeight = FontWeights.Bold;
                F.ForecastMethod.Init();
                // Инициализируем поток обучения
                train = new Thread(trainForecastModel);
                train.Priority = ThreadPriority.Highest;
                tbTrain.FontWeight = FontWeights.Bold;
                train.Start();
            }
            else
            {
                MessageBox.Show(msg, "Ошибка обучения", MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            }
        }
        /// <summary>
        /// Поток обучения модели
        /// </summary>
        /// <param name="fm"></param>
        private void trainForecastModel()
        {
            F.ForecastMethod.Train();
            F.ErrorTrain = errorTrain.ToArray();
            F.IsTrain = true;
            Application.Current.Dispatcher.Invoke(() =>
            {
                Close();
            });
        }
        /// <summary>
        /// Делегат обучения
        /// </summary>
        /// <param name="error"></param>
        /// <param name="epoch"></param>
        /// <param name="maxEpochs"></param>
        private void trainReport(double error, int epoch, int maxEpochs)
        {
            errorTrain.Add(error);
            Application.Current.Dispatcher.Invoke(() => {
                tbEpoch.Text = string.Format("{0}/{1}",epoch,maxEpochs);
                PrBar.Value = epoch;
                tbError.Text = string.Format("{0:R}",error);
            });
        }
    }
}
