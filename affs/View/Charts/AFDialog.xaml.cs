﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Charts
{
    /// <summary>
    /// Логика взаимодействия для ApproximationFunction.xaml
    /// </summary>
    public partial class AFDialog : Window
    {
        public AFDialog(Dictionary<string,string> afs)
        {
            InitializeComponent();
            Title = "Аппроксимационные функции";
            DataTable dt = new DataTable();
            DataColumn elName = new DataColumn("Имя функции");
            DataColumn elRange = new DataColumn("Аппроксимационная функция");
            dt.Columns.Add(elName);
            dt.Columns.Add(elRange);
            foreach (KeyValuePair<string,string> af in afs)
            {
                DataRow row = dt.NewRow();
                row[elName] = af.Key;
                row[elRange] = af.Value;
                dt.Rows.Add(row);
            }
            gridApproximation.ItemsSource = dt.DefaultView;
        }

        public AFDialog(Model.DataPreparation dp)
        {
            InitializeComponent();
            Title = "Обучающая выборка";
            DataTable dt = new DataTable();
            DataColumn autoIncrement = new DataColumn("#");
            autoIncrement.AutoIncrement = true;
            autoIncrement.AutoIncrementSeed = 1;
            autoIncrement.AutoIncrementStep = 1;
            dt.Columns.Add(autoIncrement);
            foreach (KeyValuePair<Model.Variable, List<int>> elem in dp.OutputRegress)
            {
                foreach (int r in elem.Value)
                {
                    DataColumn dc = new DataColumn(elem.Key.Name + "(" + Properties.Settings.Default.defaultCharDependenct + "+" + r + ")", typeof(double));
                    dt.Columns.Add(dc);
                }
            }
            foreach (KeyValuePair<Model.Variable,List<int>> elem in dp.InputRegress)
            {
                foreach (int r in elem.Value) {
                    string nameColumn;
                    if (r == 0)
                    {
                        nameColumn = elem.Key.Name + "(" + Properties.Settings.Default.defaultCharDependenct + ")";
                    }else if (r < 0)
                    {
                        nameColumn = elem.Key.Name + "(" + Properties.Settings.Default.defaultCharDependenct + r + ")";
                    }else
                    {
                        nameColumn = elem.Key.Name + "(" + Properties.Settings.Default.defaultCharDependenct + "+" + r + ")";
                    }
                    DataColumn dc = new DataColumn(nameColumn, typeof(double));
                    dt.Columns.Add(dc);
                }
            }
            

            for (int i = 0; i < dp.TrainDataCount; i++)
            {
                DataRow dr = dt.NewRow();
                for (int j = 0; j < dp.TrainDataOutput[i].Length; j++)
                {
                    dr[j + 1 ] = dp.TrainDataOutput[i][j];
                }
                for (int j = 0; j < dp.TrainDataInput[i].Length;j++)
                {
                    dr[j + 1 + dp.TrainDataOutput[i].Length] = dp.TrainDataInput[i][j];
                }
                dt.Rows.Add(dr);
            }
            gridApproximation.ItemsSource = dt.DefaultView;
        }

        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
