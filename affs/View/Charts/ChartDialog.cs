﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using FuzzyLogic;
using affs.Model;

namespace affs.View.Charts
{
    public partial class ChartDialog : Form
    {
        string prefix = "Approx_";                          // Префикс для функции аппроксимации

        Dictionary<string, SeriesChartType> ChartTypeList = new Dictionary<string, SeriesChartType>();
        Dictionary<string, int> ApproximationType = new Dictionary<string, int>();
        Dictionary<string, int> ThinklessLine = new Dictionary<string, int>();
        Dictionary<string, string> ApproximationFunction = new Dictionary<string, string>();

        #region Конструкторы
        private ChartDialog()
        {
            InitializeComponent();
            Init();
        }

        #region График по массиву Double
        public ChartDialog(string name, double[] _x, int start = 0) : this()
        {
            base.Text = "График " + name;
            chart.Series.Clear();
            chart.Series.Add(name);
            chart.Series[name].LegendText = name;
            chart.Series[name].BorderWidth = 3;
            chart.Series[name].ChartType = SeriesChartType.Spline;
            for (int j = 0; j < _x.Length; j++)
            {
                chart.Series[name].Points.AddXY(j + start, _x[j]);
            }
            setMinMaxY(1, -1);
        }
        #endregion
        #region График по переменной
        /// <summary>
        /// Строим график по переменной
        /// </summary>
        /// <param name="v"></param>
        public ChartDialog(Model.Variable v) : this()
        {
            base.Text = "График " + v.Name;
            chart.Series.Clear();
            chart.Series.Add(v.Name);
            chart.Series[v.Name].LegendText = v.Name;
            chart.Series[v.Name].BorderWidth = 3;
            chart.Series[v.Name].ChartType = SeriesChartType.Spline;
            for (int j = 0; j < v.Range.Count; j++)
            {
                chart.Series[v.Name].Points.AddXY(j, v.Range[j]);
            }

            setMinMaxY(v.Max, v.Min);
        }
        #endregion
        #region График по таблице
        /// <summary>
        /// Построение графика по таблице
        /// </summary>
        /// <param name="dt"></param>
        public ChartDialog(DataTable dt) : this()
        {
            
            chart.Series.Clear();
            if (dt.Columns.Count == 1)      // Если колонка одна
            {
                DataColumn column = dt.Columns[0];
                base.Text = "График " + column.ColumnName;
                chart.Series.Add(column.ColumnName);
                chart.Series[column.ColumnName].LegendText = column.ColumnName;
                chart.Series[column.ColumnName].BorderWidth = 3;
                chart.Series[column.ColumnName].ChartType = SeriesChartType.Spline;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    chart.Series[column.ColumnName].Points.AddXY(i, (double)dt.Rows[i][column]);
                }
            }else if (dt.Columns.Count > 1) // Если колонок больше чем одна, берем первую как основную
            {
                if (dt.Columns.Count == 2)
                {
                    base.Text = "График ";
                }else
                {
                    base.Text = "Графики переменных: ";
                }
                
                // Первую колонку берем как ось х
                DataColumn columnX = dt.Columns[0];
                for (int i = 1; i < dt.Columns.Count; i++)
                {
                    base.Text += dt.Columns[i].ColumnName;
                    if (i != dt.Columns.Count - 1) base.Text += ", ";
                    chart.Series.Add(dt.Columns[i].ColumnName);
                    chart.Series[dt.Columns[i].ColumnName].LegendText = dt.Columns[i].ColumnName;
                    chart.Series[dt.Columns[i].ColumnName].BorderWidth = 3;
                    chart.Series[dt.Columns[i].ColumnName].ChartType = SeriesChartType.Spline;
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        var x = dt.Rows[j][dt.Columns[0]];
                        var y = dt.Rows[j][dt.Columns[i]];
                        if (y != null && x != null)
                        {
                            chart.Series[dt.Columns[i].ColumnName].Points.AddXY(x, y);
                        }
                    }
                }
            }
        }
        #endregion
        #region График переменных
        public ChartDialog(List<Variable> variablesX, List<Variable> variablesY) : this()
        {
            chart.Series.Clear();
            if (variablesX.Count != 1)
            {
                MessageBox.Show("По оси абсцисс не заданы переменные");
                this.Close();
                return;
            }
            Variable x = variablesX[0]; // Забираем переменую
            if (variablesY.Count == 1)
            {
                base.Text = "График ";
            }
            else
            {
                base.Text = "Графики переменных: ";
            }
            double minY = variablesY[0].Min, maxY = variablesY[0].Max;
            for (int i = 0; i < variablesY.Count; i++)
            {
                base.Text += variablesY[i].Name;
                if (i != variablesY.Count - 1) base.Text += ", ";
                chart.Series.Add(variablesY[i].Name);
                chart.Series[variablesY[i].Name].LegendText = variablesY[i].Name;
                chart.Series[variablesY[i].Name].BorderWidth = 3;
                chart.Series[variablesY[i].Name].ChartType = SeriesChartType.Spline;
                int count = Math.Min(x.Range.Count, variablesY[i].Range.Count);
                for (int j = 0; j < count; j++)
                {
                    var _x = x.Range[j];
                    var _y = variablesY[i].Range[j];
                    chart.Series[variablesY[i].Name].Points.AddXY(_x, _y);
                }
                minY = Math.Min(minY, variablesY[i].Min);
                maxY = Math.Min(maxY, variablesY[i].Max);
            }
            //setMinMaxY(maxY, minY);

        }
        #endregion
        #endregion
        #region Инициализация
        /// <summary>
        /// Инициализация окна
        /// </summary>
        private void Init()
        {
            //
            // Типы графиков
            //
            ChartTypeList.Add("Сплайн", SeriesChartType.Spline);
            ChartTypeList.Add("Точки", SeriesChartType.Point);
            ChartTypeList.Add("Линия", SeriesChartType.Line);
            ChartTypeList.Add("Стопка столбцов", SeriesChartType.StackedColumn);
            ChartTypeList.Add("Ступенчатый график", SeriesChartType.StepLine);
            UnCheckedChildren(ChartTypes);
            Splines.CheckState = CheckState.Checked;
            foreach (ToolStripMenuItem elemChartType in ChartTypes.DropDownItems)
            {
                elemChartType.Click += ElemChartType_Click;
            }
            //
            // Аппроксимация
            //
            ApproximationType.Add(ApproxNot.Name, 0);
            ApproximationType.Add(Approx1.Name, 1);
            ApproximationType.Add(Approx2.Name, 2);
            ApproximationType.Add(Approx3.Name, 3);
            ApproximationType.Add(Approx4.Name, 4);
            ApproximationType.Add(Approx5.Name, 5);
            ApproximationType.Add(Approx6.Name, 6);
            UnCheckedChildren(Approximation);
            ApproxNot.CheckState = CheckState.Checked;
            foreach (ToolStripMenuItem elemApproximation in Approximation.DropDownItems)
            {
                if (elemApproximation != showFunctionsPolinom && elemApproximation != FTApproximation)
                {
                    elemApproximation.Click += ElemApproximation_Click;
                }
            }
            showFunctionsPolinom.Click += ShowFunctionsPolinom_Click;
            FTApproximation.Click += FTApproximation_Click;

            //
            // Толщина линии
            //
            ThinklessLine.Add(Thinkless1.Name, 1);
            ThinklessLine.Add(Thinkless2.Name, 2);
            ThinklessLine.Add(Thinkless3.Name, 3);
            ThinklessLine.Add(Thinkless4.Name, 4);
            ThinklessLine.Add(Thinkless5.Name, 5);
            ThinklessLine.Add(Thinkless6.Name, 6);
            ThinklessLine.Add(Thinkless7.Name, 7);
            UnCheckedChildren(Thinkless);
            Thinkless3.CheckState = CheckState.Checked; // По умолчанию 3
            foreach (ToolStripMenuItem elemThinkless in Thinkless.DropDownItems)
            {
                elemThinkless.Click += ElemThinkless_Click;
            }

        }
        #endregion

        #region Выбор типа графика
        private void ElemChartType_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            UnCheckedChildren(ChartTypes);
            item.CheckState = CheckState.Checked;
            setChartType(item.Text);
        }
        /// <summary>
        /// Убараем галки со всех детей
        /// </summary>
        /// <param name="m"></param>
        private void UnCheckedChildren(ToolStripMenuItem m)
        {
            foreach (ToolStripMenuItem item in m.DropDownItems)
            {
                item.CheckState = CheckState.Unchecked;
            }

        }
        /// <summary>
        /// Установить тип графика
        /// </summary>
        /// <param name="type"></param>
        private void setChartType(string type)
        {
            if (ChartTypeList.ContainsKey(type))
            {
                SeriesChartType seriesChartType = ChartTypeList[type];
                foreach (Series series in chart.Series)
                {
                    if ((string)series.Tag != "ApproximationLine")
                    {
                        series.ChartType = seriesChartType;
                    }
                }
            }
        }

        #endregion

        #region Применение аппроксимации
        /// <summary>
        /// Событие при изменении значения аппроксимации
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ElemApproximation_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            UnCheckedChildren(Approximation);
            item.CheckState = CheckState.Checked;
            buildApproximation(item.Name);
        }
        /// <summary>
        /// Построение аппроксимации
        /// </summary>
        /// <param name="name">Имя</param>
        private void buildApproximation(string name)
        {
            if (!ApproximationType.ContainsKey(name)) return;   // Если не найдено, то выбрасываемся
            int polinom = ApproximationType[name];              // Получаем степень полинома
            double[] x, y;                                      // Значения x и y
            ApproximationFunction.Clear();
            List<Series> ApproximationSeries = new List<Series>();
            ClearApproximation();
            foreach (Series series in chart.Series)
            {
                string nameApprox = prefix + series.Name;       // Имя аппроксимации
                if (polinom == 0)
                {
                    if ((string)series.Tag == "ApproximationLine")
                    {

                        chart.Series.Remove(series);
                    }
                } else {
                    x = new double[series.Points.Count];
                    y = new double[series.Points.Count];
                    
                    for (int i = 0; i < x.Length; i++)
                    {
                        DataPoint point = series.Points[i];
                        x[i] = point.XValue;
                        y[i] = point.YValues[0];
                    }
                    Matrix Y = new Matrix(y.Length, 1);
                    Matrix X = new Matrix(x.Length, polinom + 1);
                    for (int i = 0; i < x.Length; i++)
                    {
                        Y[i, 0] = y[i];
                        for (int j = polinom; j >= 0; j--)
                        {
                            if (j == 0)
                            {
                                X[i,j] = 1;
                            }
                            else
                            {
                                X[i,j] = Math.Pow(x[i], j);
                            }
                        }
                    }
                    Matrix C = X.PInverse() * Y;    // Находим коэффициенты
                    ApproximationFunction.Add(series.Name,buildFunction(C));
                    double min = 0, max = 0;
                    findMaxMin(x, ref max, ref min);
                    double step = (max - min) / 10;
                    List<double> _x = new List<double>();
                    for(double i = min; i <= max + step / 2;)
                    {
                        _x.Add(i);
                        i += step;
                    }
                    x = _x.ToArray();
                    double[] yApprox = evalFunction(C,x);
                    Series s = new Series(nameApprox);
                    s.LegendText = string.Empty;
                    s.IsVisibleInLegend = false;
                    s.BorderWidth = 3;
                    s.Tag = "ApproximationLine";
                    s.ChartType = SeriesChartType.Spline;
                    s.Color = Color.Black;
                    s.BorderDashStyle = ChartDashStyle.Dash;
                    for (int i = 0; i < x.Length; i++)
                    {
                        s.Points.AddXY(x[i], yApprox[i]);
                    }
                    ApproximationSeries.Add(s);
                }
            }
            foreach (Series _s in ApproximationSeries) {
                chart.Series.Add(_s);
            }
        }

        private void findMaxMin(double[] x,ref double max,ref double min)
        {
            max = min = x[0];
            foreach (double _x in x)
            {
                max = Math.Max(max,_x);
                min = Math.Min(min, _x);
            }
        }
        /// <summary>
        /// Нечеткое Ft преобразование
        /// </summary>
        private void FTApproximation_Click(object sender, EventArgs e)
        {
            // Устанавливаем чек
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            UnCheckedChildren(Approximation);
            item.CheckState = CheckState.Checked;
            // Выполняем разбиение
            double[] x, y;                                      // Значения x и y
            ApproximationFunction.Clear();
            List<Series> ApproximationSeries = new List<Series>();
            ClearApproximation();
            bool queryToNodes = false;                          // Спрашиваем 1 раз
            int cn = 3;                                         // В 3 раза меньше чем есть
            foreach (Series series in chart.Series)
            {
                string nameApprox = prefix + series.Name;       // Имя аппроксимации
                x = new double[series.Points.Count];
                y = new double[series.Points.Count];
                for (int i = 0; i < x.Length; i++)
                {
                    DataPoint point = series.Points[i];
                    x[i] = point.XValue;
                    y[i] = point.YValues[0];
                }

                if (!queryToNodes)
                {
                    NodesDialog nd = new NodesDialog(cn);   // Количество узлов
                    if (nd.ShowDialog() == true)
                    {
                        cn = nd.StepFuzzy;
                    }
                    queryToNodes = true;
                }
                FuzzyTransform ft = new FuzzyTransform(y, cn);
                ft.Run();
                Array.Sort(x);
                x.Distinct();
                Series s = new Series(nameApprox);
                s.LegendText = string.Empty;
                s.IsVisibleInLegend = false;
                s.BorderWidth = 3;
                s.Tag = "ApproximationLine";
                s.ChartType = SeriesChartType.Spline;
                s.Color = Color.Black;
                s.BorderDashStyle = ChartDashStyle.Dash;
                for (int i = 0; i < x.Length; i++)
                {
                    s.Points.AddXY(x[i], ft.TimeSeriesInverse[i]);
                }
                ApproximationSeries.Add(s);
            }
            foreach (Series _s in ApproximationSeries)
            {
                chart.Series.Add(_s);
            }
        }
        /// <summary>
        /// Удаление всех аппроксимаций
        /// </summary>
        private void ClearApproximation()
        {
            List<Series> removeSeries = new List<Series>();
            foreach (Series series in chart.Series)
            {
                if ((string)series.Tag == "ApproximationLine")
                {
                    removeSeries.Add(series);
                }
            }
            foreach (Series series in removeSeries) {
                chart.Series.Remove(series);
            }
        }

        /// <summary>
        /// Построение текстового изображения функции
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        private string buildFunction(Matrix c)
        {
            string result = "y = ";
            int digits = affs.Properties.Settings.Default.Digits;
            for (int i = c.N - 1; i > 0; i--)
            {
                if (c[i, 0] >= 0)
                {
                    if (i == c.N - 1)
                    {
                        result += c[i, 0].ToString() + "*x^" + i.ToString();
                    }
                    else {
                        result += "+" + c[i, 0].ToString() + "*x^" + i.ToString();
                    }
                    
                }else
                {
                    result += c[i, 0].ToString() + "*x^" + i.ToString();
                }
                
            }

            // Добавляем постоянную константу
            if (c[0, 0] > 0)
            {
                result += "+" + c[0, 0].ToString();
            }
            else {
                result += c[0, 0].ToString();
            }
            return result;
        }
        /// <summary>
        /// Показываем список аппроксимационных функций
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowFunctionsPolinom_Click(object sender, EventArgs e)
        {
            if (ApproximationFunction.Count != 0){
                AFDialog af = new AFDialog(ApproximationFunction);
                af.ShowDialog();
            }
        }

        /// <summary>
        /// Вычисление значений функций
        /// </summary>
        /// <param name="c">Матрица коэффициентов</param>
        /// <param name="x">параметры x</param>
        /// <returns></returns>
        private double[] evalFunction(Matrix c, double[] x)
        {
            double[] result = new double[x.Length];
            for (int i = 0; i< x.Length; i++)
            {
                double summ = c[0,0];
                for (int j = 1; j < c.N; j++)
                {
                    summ += c[j, 0] * Math.Pow(x[i], j);
                }
                result[i] = summ;
            }
            return result;
        }

        #endregion

        #region Толщина линии
        private void ElemThinkless_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = sender as ToolStripMenuItem;
            if (item == null) return;
            UnCheckedChildren(Thinkless);
            item.CheckState = CheckState.Checked;
            if (ThinklessLine.ContainsKey(item.Name))
            {
                setThinkless(ThinklessLine[item.Name]);
            }
        }

        private void setThinkless(int v)
        {
            foreach (Series series in chart.Series)
            {
                if ((string)series.Tag != "ApproximationLine")
                {
                    series.BorderWidth = v;
                    series.MarkerSize = v;
                }
            }
        }
        #endregion


        #region Устновка минимума и максимума
        /// <summary>
        /// Установление минимума и максимума
        /// </summary>
        /// <param name="max"></param>
        /// <param name="min"></param>
        private void setMinMaxY(double max, double min)
        {
            ChartArea ca = chart.ChartAreas.FirstOrDefault();
            if (ca.AxisY.Maximum != double.NaN)         //  Если не NaN
            {
                if (max > 0)
                {
                    ca.AxisY.Maximum = Math.Round(max * 1.05);
                }
                else if (max < 0)
                {
                    ca.AxisY.Maximum = Math.Round(max / 1.05);
                }

                if (min > 0)
                {
                    ca.AxisY.Minimum = Math.Round(min / 1.05);
                }
                else if (min < 0)
                {
                    ca.AxisY.Minimum = Math.Round(min * 1.05);
                }
            }
        }

        #endregion
    }

}
