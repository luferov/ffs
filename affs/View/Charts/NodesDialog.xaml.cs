﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace affs.View.Charts
{
    /// <summary>
    /// Логика взаимодействия для NodesDialog.xaml
    /// </summary>
    public partial class NodesDialog : Window
    {
        private int _stepFuzzy = 0;
        /// <summary>
        /// Шаг нечеткого разбиения
        /// </summary>
        public int StepFuzzy
        {
            get { return _stepFuzzy; }
            set{ _stepFuzzy = value; }
        }

        public NodesDialog()
        {
            InitializeComponent();
        }

        public NodesDialog(int cn)
        {
            InitializeComponent();
            stepFuzzy.Text = cn.ToString();
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            int cn;
            if (!int.TryParse(stepFuzzy.Text, out cn))
            {
                MessageBox.Show("Пожалуйства, введите число!",
                    "Ошибка ввода",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            if (cn < 2)
            {
                MessageBox.Show("Количество узлов должно быть больше двух!",
                    "Ошибка ввода",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            _stepFuzzy = cn;
            DialogResult = true;
            Close();
        }
        private void Cansel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
