﻿using System.Windows.Forms.DataVisualization.Charting;

namespace affs.View.Charts
{
    partial class ChartDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChartDialog));
            this.mainLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.menu = new System.Windows.Forms.MenuStrip();
            this.ChartTypes = new System.Windows.Forms.ToolStripMenuItem();
            this.Splines = new System.Windows.Forms.ToolStripMenuItem();
            this.Points = new System.Windows.Forms.ToolStripMenuItem();
            this.Lines = new System.Windows.Forms.ToolStripMenuItem();
            this.StackedColumn = new System.Windows.Forms.ToolStripMenuItem();
            this.StepLine = new System.Windows.Forms.ToolStripMenuItem();
            this.Approximation = new System.Windows.Forms.ToolStripMenuItem();
            this.showFunctionsPolinom = new System.Windows.Forms.ToolStripMenuItem();
            this.ApproxNot = new System.Windows.Forms.ToolStripMenuItem();
            this.Approx1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Approx2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Approx3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Approx4 = new System.Windows.Forms.ToolStripMenuItem();
            this.Approx5 = new System.Windows.Forms.ToolStripMenuItem();
            this.Approx6 = new System.Windows.Forms.ToolStripMenuItem();
            this.FTApproximation = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless1 = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless2 = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless3 = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless4 = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless5 = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless6 = new System.Windows.Forms.ToolStripMenuItem();
            this.Thinkless7 = new System.Windows.Forms.ToolStripMenuItem();
            this.mainLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainLayoutPanel
            // 
            this.mainLayoutPanel.ColumnCount = 1;
            this.mainLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Controls.Add(this.chart, 0, 1);
            this.mainLayoutPanel.Controls.Add(this.menu, 0, 0);
            this.mainLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainLayoutPanel.Name = "mainLayoutPanel";
            this.mainLayoutPanel.RowCount = 2;
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainLayoutPanel.Size = new System.Drawing.Size(982, 453);
            this.mainLayoutPanel.TabIndex = 0;
            // 
            // chart
            // 
            chartArea1.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea1);
            this.chart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Alignment = System.Drawing.StringAlignment.Center;
            legend1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Bottom;
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend";
            this.chart.Legends.Add(legend1);
            this.chart.Location = new System.Drawing.Point(3, 31);
            this.chart.Name = "chart";
            this.chart.Size = new System.Drawing.Size(976, 419);
            this.chart.TabIndex = 0;
            // 
            // menu
            // 
            this.menu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ChartTypes,
            this.Approximation,
            this.Thinkless});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(982, 28);
            this.menu.TabIndex = 1;
            this.menu.Text = "menuStrip1";
            // 
            // ChartTypes
            // 
            this.ChartTypes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Splines,
            this.Points,
            this.Lines,
            this.StackedColumn,
            this.StepLine});
            this.ChartTypes.Name = "ChartTypes";
            this.ChartTypes.Size = new System.Drawing.Size(108, 24);
            this.ChartTypes.Text = "Тип графика";
            // 
            // Splines
            // 
            this.Splines.Checked = true;
            this.Splines.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Splines.Name = "Splines";
            this.Splines.Size = new System.Drawing.Size(227, 26);
            this.Splines.Text = "Сплайн";
            // 
            // Points
            // 
            this.Points.Name = "Points";
            this.Points.Size = new System.Drawing.Size(227, 26);
            this.Points.Text = "Точки";
            // 
            // Lines
            // 
            this.Lines.Name = "Lines";
            this.Lines.Size = new System.Drawing.Size(227, 26);
            this.Lines.Text = "Линия";
            // 
            // StackedColumn
            // 
            this.StackedColumn.Name = "StackedColumn";
            this.StackedColumn.Size = new System.Drawing.Size(227, 26);
            this.StackedColumn.Text = "Стопка столбцов";
            // 
            // StepLine
            // 
            this.StepLine.Name = "StepLine";
            this.StepLine.Size = new System.Drawing.Size(227, 26);
            this.StepLine.Text = "Ступенчатый график";
            // 
            // Approximation
            // 
            this.Approximation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showFunctionsPolinom,
            this.ApproxNot,
            this.FTApproximation,
            this.Approx1,
            this.Approx2,
            this.Approx3,
            this.Approx4,
            this.Approx5,
            this.Approx6});
            this.Approximation.Name = "Approximation";
            this.Approximation.Size = new System.Drawing.Size(135, 24);
            this.Approximation.Text = "Аппроксимация";
            // 
            // showFunctionsPolinom
            // 
            this.showFunctionsPolinom.Name = "showFunctionsPolinom";
            this.showFunctionsPolinom.Size = new System.Drawing.Size(297, 26);
            this.showFunctionsPolinom.Text = "Показать функции полиномов";
            // 
            // ApproxNot
            // 
            this.ApproxNot.Name = "ApproxNot";
            this.ApproxNot.Size = new System.Drawing.Size(297, 26);
            this.ApproxNot.Text = "Нет";
            // 
            // Approx1
            // 
            this.Approx1.Name = "Approx1";
            this.Approx1.Size = new System.Drawing.Size(297, 26);
            this.Approx1.Text = "Полином первого порядка";
            // 
            // Approx2
            // 
            this.Approx2.Name = "Approx2";
            this.Approx2.Size = new System.Drawing.Size(297, 26);
            this.Approx2.Text = "Полином второго порядка";
            // 
            // Approx3
            // 
            this.Approx3.Name = "Approx3";
            this.Approx3.Size = new System.Drawing.Size(297, 26);
            this.Approx3.Text = "Полином третьего порядка";
            // 
            // Approx4
            // 
            this.Approx4.Name = "Approx4";
            this.Approx4.Size = new System.Drawing.Size(297, 26);
            this.Approx4.Text = "Полином четвертого порядка";
            // 
            // Approx5
            // 
            this.Approx5.Name = "Approx5";
            this.Approx5.Size = new System.Drawing.Size(297, 26);
            this.Approx5.Text = "Полином пятого порядка";
            // 
            // Approx6
            // 
            this.Approx6.Name = "Approx6";
            this.Approx6.Size = new System.Drawing.Size(297, 26);
            this.Approx6.Text = "Полином шестого порядка";
            // 
            // FTApproximation
            // 
            this.FTApproximation.Name = "FTApproximation";
            this.FTApproximation.Size = new System.Drawing.Size(297, 26);
            this.FTApproximation.Text = "Нечеткое FT преобразование";
            // 
            // Thinkless
            // 
            this.Thinkless.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Thinkless1,
            this.Thinkless2,
            this.Thinkless3,
            this.Thinkless4,
            this.Thinkless5,
            this.Thinkless6,
            this.Thinkless7});
            this.Thinkless.Name = "Thinkless";
            this.Thinkless.Size = new System.Drawing.Size(132, 24);
            this.Thinkless.Text = "Толщина линий";
            // 
            // Thinkless1
            // 
            this.Thinkless1.Name = "Thinkless1";
            this.Thinkless1.Size = new System.Drawing.Size(159, 26);
            this.Thinkless1.Text = "1 единица";
            // 
            // Thinkless2
            // 
            this.Thinkless2.Name = "Thinkless2";
            this.Thinkless2.Size = new System.Drawing.Size(159, 26);
            this.Thinkless2.Text = "2 единицы";
            // 
            // Thinkless3
            // 
            this.Thinkless3.Name = "Thinkless3";
            this.Thinkless3.Size = new System.Drawing.Size(159, 26);
            this.Thinkless3.Text = "3 единицы";
            // 
            // Thinkless4
            // 
            this.Thinkless4.Name = "Thinkless4";
            this.Thinkless4.Size = new System.Drawing.Size(159, 26);
            this.Thinkless4.Text = "4 единицы";
            // 
            // Thinkless5
            // 
            this.Thinkless5.Name = "Thinkless5";
            this.Thinkless5.Size = new System.Drawing.Size(159, 26);
            this.Thinkless5.Text = "5 единиц";
            // 
            // Thinkless6
            // 
            this.Thinkless6.Name = "Thinkless6";
            this.Thinkless6.Size = new System.Drawing.Size(159, 26);
            this.Thinkless6.Text = "6 единиц";
            // 
            // Thinkless7
            // 
            this.Thinkless7.Name = "Thinkless7";
            this.Thinkless7.Size = new System.Drawing.Size(159, 26);
            this.Thinkless7.Text = "7 единиц";
            // 
            // ChartDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 453);
            this.Controls.Add(this.mainLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.Name = "ChartDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Графики";
            this.mainLayoutPanel.ResumeLayout(false);
            this.mainLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainLayoutPanel;
        public System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem ChartTypes;
        private System.Windows.Forms.ToolStripMenuItem Approximation;
        private System.Windows.Forms.ToolStripMenuItem Thinkless;
        private System.Windows.Forms.ToolStripMenuItem Splines;
        private System.Windows.Forms.ToolStripMenuItem Points;
        private System.Windows.Forms.ToolStripMenuItem Lines;
        private System.Windows.Forms.ToolStripMenuItem StackedColumn;
        private System.Windows.Forms.ToolStripMenuItem StepLine;
        private System.Windows.Forms.ToolStripMenuItem ApproxNot;
        private System.Windows.Forms.ToolStripMenuItem Approx1;
        private System.Windows.Forms.ToolStripMenuItem Approx2;
        private System.Windows.Forms.ToolStripMenuItem Approx3;
        private System.Windows.Forms.ToolStripMenuItem Approx4;
        private System.Windows.Forms.ToolStripMenuItem Approx5;
        private System.Windows.Forms.ToolStripMenuItem Approx6;
        private System.Windows.Forms.ToolStripMenuItem showFunctionsPolinom;
        private System.Windows.Forms.ToolStripMenuItem Thinkless1;
        private System.Windows.Forms.ToolStripMenuItem Thinkless2;
        private System.Windows.Forms.ToolStripMenuItem Thinkless3;
        private System.Windows.Forms.ToolStripMenuItem Thinkless4;
        private System.Windows.Forms.ToolStripMenuItem Thinkless5;
        private System.Windows.Forms.ToolStripMenuItem Thinkless6;
        private System.Windows.Forms.ToolStripMenuItem Thinkless7;
        private System.Windows.Forms.ToolStripMenuItem FTApproximation;
    }
}