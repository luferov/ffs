﻿namespace affs.View.Charts
{
    partial class ChartForecastError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChartForecastError));
            this.tlp = new System.Windows.Forms.TableLayoutPanel();
            this.tab = new System.Windows.Forms.TabControl();
            this.errorTab = new System.Windows.Forms.TabPage();
            this.errorChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.recoveryTab = new System.Windows.Forms.TabPage();
            this.recoveryChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tlp.SuspendLayout();
            this.tab.SuspendLayout();
            this.errorTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorChart)).BeginInit();
            this.recoveryTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.recoveryChart)).BeginInit();
            this.SuspendLayout();
            // 
            // tlp
            // 
            this.tlp.ColumnCount = 1;
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp.Controls.Add(this.tab, 0, 0);
            this.tlp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp.Location = new System.Drawing.Point(0, 0);
            this.tlp.Name = "tlp";
            this.tlp.RowCount = 1;
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp.Size = new System.Drawing.Size(838, 358);
            this.tlp.TabIndex = 0;
            // 
            // tab
            // 
            this.tab.Controls.Add(this.errorTab);
            this.tab.Controls.Add(this.recoveryTab);
            this.tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab.Location = new System.Drawing.Point(3, 3);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(832, 352);
            this.tab.TabIndex = 0;
            // 
            // errorTab
            // 
            this.errorTab.Controls.Add(this.errorChart);
            this.errorTab.Location = new System.Drawing.Point(4, 25);
            this.errorTab.Name = "errorTab";
            this.errorTab.Padding = new System.Windows.Forms.Padding(3);
            this.errorTab.Size = new System.Drawing.Size(824, 323);
            this.errorTab.TabIndex = 0;
            this.errorTab.Text = "Ошибка обучения";
            this.errorTab.UseVisualStyleBackColor = true;
            // 
            // errorChart
            // 
            chartArea1.Name = "ChartArea1";
            this.errorChart.ChartAreas.Add(chartArea1);
            this.errorChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.errorChart.Legends.Add(legend1);
            this.errorChart.Location = new System.Drawing.Point(3, 3);
            this.errorChart.Name = "errorChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.errorChart.Series.Add(series1);
            this.errorChart.Size = new System.Drawing.Size(818, 317);
            this.errorChart.TabIndex = 0;
            this.errorChart.Text = "chart1";
            // 
            // recoveryTab
            // 
            this.recoveryTab.Controls.Add(this.recoveryChart);
            this.recoveryTab.Location = new System.Drawing.Point(4, 25);
            this.recoveryTab.Name = "recoveryTab";
            this.recoveryTab.Padding = new System.Windows.Forms.Padding(3);
            this.recoveryTab.Size = new System.Drawing.Size(824, 323);
            this.recoveryTab.TabIndex = 1;
            this.recoveryTab.Text = "Восстановление функции";
            this.recoveryTab.UseVisualStyleBackColor = true;
            // 
            // recoveryChart
            // 
            chartArea2.Name = "ChartArea1";
            this.recoveryChart.ChartAreas.Add(chartArea2);
            this.recoveryChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend2.Name = "Legend1";
            this.recoveryChart.Legends.Add(legend2);
            this.recoveryChart.Location = new System.Drawing.Point(3, 3);
            this.recoveryChart.Name = "recoveryChart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.recoveryChart.Series.Add(series2);
            this.recoveryChart.Size = new System.Drawing.Size(818, 317);
            this.recoveryChart.TabIndex = 0;
            this.recoveryChart.Text = "chart1";
            // 
            // ChartForecastError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 358);
            this.Controls.Add(this.tlp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ChartForecastError";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ошибка обучения";
            this.tlp.ResumeLayout(false);
            this.tab.ResumeLayout(false);
            this.errorTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorChart)).EndInit();
            this.recoveryTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.recoveryChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp;
        private System.Windows.Forms.TabControl tab;
        private System.Windows.Forms.TabPage errorTab;
        private System.Windows.Forms.DataVisualization.Charting.Chart errorChart;
        private System.Windows.Forms.TabPage recoveryTab;
        private System.Windows.Forms.DataVisualization.Charting.Chart recoveryChart;
    }
}