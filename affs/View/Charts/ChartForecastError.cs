﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace affs.View.Charts
{
    public partial class ChartForecastError : Form
    {
        Forecast f;

        public Forecast F
        {
            get { return f;  }
            set { f = value; }
        }

        public ChartForecastError(Forecast _f)
        {
            InitializeComponent();
            f = _f;
            Load += ChartForecastError_Load;
        }
        /// <summary>
        /// Загрузка формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChartForecastError_Load(object sender, EventArgs e)
        {
            drawErrorChart();
            drawRecoveryChart();
        }
        /// <summary>
        /// Отрисовка ошибки
        /// </summary>
        private void drawRecoveryChart()
        {
            errorChart.Series.Clear();
            string name = "Ошибка обучения";
            errorChart.Series.Add(name);
            errorChart.Series[name].LegendText = name;
            errorChart.Series[name].BorderWidth = 3;
            errorChart.Series[name].ChartType = SeriesChartType.Spline;
            for (int j = 0; j < F.ErrorTrain.Length; j++)
            {
                errorChart.Series[name].Points.AddXY(j * F.ForecastMethod.Report, F.ErrorTrain[j]);
            }
        }
        /// <summary>
        /// Отрисовка сосстановления
        /// </summary>
        private void drawErrorChart()
        {
            recoveryChart.Series.Clear();
            // Исходная функция
            string name = "Исходная функция";
            recoveryChart.Series.Add(name);
            recoveryChart.Series[name].LegendText = name;
            recoveryChart.Series[name].BorderWidth = 10;
            recoveryChart.Series[name].MarkerSize = 5;
            recoveryChart.Series[name].ChartType = SeriesChartType.Point;
            for (int j = 0; j < F.ForecastMethod.TrainDataOutput.Length; j++)
            {
                recoveryChart.Series[name].Points.AddXY(j, F.ForecastMethod.TrainDataOutput[j][0]);
            }

            // Восстановление функции
            name = "Восстановительная функция";
            recoveryChart.Series.Add(name);
            recoveryChart.Series[name].LegendText = name;
            recoveryChart.Series[name].BorderWidth = 3;
            recoveryChart.Series[name].ChartType = SeriesChartType.Spline;
            // Особеность метода прогнозирования
            if (F.ForecastMethod is ForecastCombine)
            {
                ((ForecastCombine)F.ForecastMethod).ResetForecastData();
            }
            for (int j = 0; j < F.ForecastMethod.TrainDataOutput.Length; j++)
            {
                double r = F.ForecastMethod.Run(F.ForecastMethod.TrainDataInput[j])[0];
                recoveryChart.Series[name].Points.AddXY(j, r);
            }
            
        }


 
    }
}
