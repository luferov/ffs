﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace affs.View.Charts
{
    public partial class ChartLinguisticTerms : Form
    {
        public ChartLinguisticTerms(FuzzyLogic.FuzzyVariable fv)
        {
            InitializeComponent();
            base.Text = "Лингвистические термы "+ fv.Name;
            drawMembershipFunction(fv);
        }

        private void drawMembershipFunction(FuzzyLogic.FuzzyVariable fv)
        {
            chart.Series.Clear();
            double step = 0.01;
            foreach (FuzzyLogic.FuzzyTerm term in fv.Terms)
            {
                
                chart.Series.Add(term.Name);
                chart.Series[term.Name].LegendText = term.Name;
                chart.Series[term.Name].BorderWidth = 3;
                chart.Series[term.Name].ChartType = SeriesChartType.Spline;
                FuzzyLogic.IMembershipFunction mf = term.MembershipFunction;
                for (double j = fv.Min; j < fv.Max;)
                {
                    chart.Series[term.Name].Points.AddXY(j, Math.Round(mf.GetValue(j), Properties.Settings.Default.Digits));
                    j += step;
                }
            }

        }
    }
}
