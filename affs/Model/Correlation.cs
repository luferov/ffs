﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Model
{
    /// <summary>
    /// Класс устанавливающий соотношение между переменнами
    /// </summary>
    public class Correlation
    {
        private Variable v1;        // Переменная 1
        private Variable v2;        // Переменная 2
        private int n;          // Количество элементов сравнения
        /// <summary>
        ///         1               y1i - y2i
        /// mape = --- * Summ(Abs(-----------)) * 100
        ///         N                  y1i
        /// </summary>
        private double mape;        // Средняя абсолютная процентная ошибка
        /// <summary>
        ///        Summ((y1i - y2i)^2)
        /// mse = ---------------------
        ///                 N
        /// </summary>
        private double mse;         // Среднеквадратичная ошибка модели
        /// <summary>
        ///         Summ(Abs(y1i - y2i))
        /// mae = ------------------------
        ///                  N
        /// </summary>
        private double mae;         // Сребняя абсолютная ошибка
        /// <summary>
        ///         Summ(y1i - y2i)
        /// me = --------------------
        ///               N
        /// </summary>
        private double me;          // Средняя ошибка
        

        #region Свойства
        /// <summary>
        /// Первая переменная
        /// </summary>
        public Variable V1
        {
            get { return v1; }
        }
        /// <summary>
        /// Вторая переменная
        /// </summary>
        public Variable V2
        {
            get { return v2; }
        }
        /// <summary>
        /// Количество элементов сравнения
        /// </summary>
        public int N
        {
            get { return n; }
        }
        /// <summary>
        /// Средняя абсолютная процентная ошибка
        /// </summary>
        public double Mape
        {
            get { return mape; }
        }
        /// <summary>
        /// Среднеквадратичная ошибка модели
        /// </summary>
        public double Mse
        {
            get { return mse; }
        }
        /// <summary>
        /// Сребняя абсолютная ошибка
        /// </summary>
        public double Mae
        {
            get { return mae; }
        }
        /// <summary>
        /// Средняя ошибка
        /// </summary>
        public double Me
        {
            get { return me; }
        }
        /// <summary>
        /// Вывод информации о сравнении
        /// </summary>
        public string CorrelationVariables {
            get { return v1.Name + " -> " + v2.Name; }
        }
        #endregion
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_v1">Оригинал функции</param>
        /// <param name="_v2">Прогнозное знечение</param>
        public Correlation(Variable _v1, Variable _v2)
        {
            v1 = _v1;
            v2 = _v2;
            n = Math.Min(_v1.Range.Count, _v2.Range.Count);
            CalculateMape();
            CalculateMae();
            CalculateMse();
            CalculateMe();
        }

        #region Вычисление ошибок
        /// <summary>
        /// Вычисление Me
        /// </summary>
        private void CalculateMe()
        {
            double summ = 0;
            for (int i = 0; i < N; i++)
            {
                summ += v1.Range[i] - v2.Range[i];
            }
            me = summ / N;
        }
        /// <summary>
        /// Вычисление Mape
        /// </summary>
        private void CalculateMape()
        {
            double summ = 0;
            for (int i = 0; i< N; i++)
            {
                summ += Math.Abs((v1.Range[i] - v2.Range[i]) / v1.Range[i]);
            }
            mape = (summ / N) * 100;
        }
        /// <summary>
        /// Вычисление Mae
        /// </summary>
        private void CalculateMae()
        {
            double summ = 0;
            for (int i = 0; i< N; i++)
            {
                summ += Math.Abs(v1.Range[i] - v2.Range[i]);
            }
            mae = summ / N;
        }
        /// <summary>
        /// Вычисление Mse
        /// </summary>
        private void CalculateMse()
        {
            double summ = 0;
            for (int i = 0; i< N; i++)
            {
                summ += Math.Pow(v1.Range[i] - v2.Range[i], 2);
            }
            mse = summ / N;
        }
        #endregion
    }
}
