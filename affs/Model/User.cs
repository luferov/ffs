﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Model
{
    /// <summary>
    /// Интерфейс пользователя
    /// </summary>
    public interface IUser
    {
        /// <summary>
        /// Имя пользователя
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Логин пользователя
        /// </summary>
        string Login { get; set; }
        /// <summary>
        /// Пароль пользователя
        /// </summary>
        string Password { get; set; }
    }

    /// <summary>
    /// Класс пользователя
    /// </summary>
    public class User : ViewModel.ViewModelBase, IUser
    {
        private string name;
        private string login;
        private string password;
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Name
        {
            get { return name; }
            set {
                if (name != value)
                {
                    name = value;
                    OnProperyChanged("Name");
                }   
            }
        }
        /// <summary>
        /// Логин пользователя
        /// </summary>
        public string Login
        {
            get { return login; }
            set {
                if (login != value)
                {
                    login = value;
                    OnProperyChanged("Login");
                }
            }
        }
        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public string Password
        {
            get { return password; }
            set {
                if (password != value)
                {
                    password = value;
                    OnProperyChanged("Password");
                }
            }
        }
    }
}
