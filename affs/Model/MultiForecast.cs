﻿using affs.Common;
using System.Collections.ObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Model
{
    /// <summary>
    /// Модель многомерного прогнозирования
    /// </summary>
    public class MultiForecast : ViewModel.ViewModelBase
    {
        private string name;                                                        // Наименование модели
        private ObservableCollection<IMultiForecast> methods;                                              // Модель прогнозироания для каждой переменной


        public MultiForecast(string name)
        {
            Name = name;
            Methods = new ObservableCollection<IMultiForecast>();
        }

        #region Свойства модели
        /// <summary>
        /// Метод обучения
        /// </summary>
        public ObservableCollection<IMultiForecast> Methods { get => methods; set { methods = value; OnProperyChanged(); } }

        public string Name { get => name; set { name = value; OnProperyChanged(); } }

        /// <summary>
        /// Выполняем прогнозирования
        /// </summary>
        void Run()
        {

        }
        #endregion
    }
}
