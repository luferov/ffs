﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MsExcel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Data;

namespace affs.Model
{
    /// <summary>
    /// Класс для удобной работы с Excel файлами
    /// </summary>
    public class Excel : IDisposable
    {
        private string path;                // Имя файла
        private MsExcel.Application app;    // Файл приложения
        private MsExcel.Workbook book;      // Рабочая книга
        private MsExcel.Worksheet sheet;    // Лист
        private MsExcel.Range range;        // Область

        public delegate void LoadStatusDelegate(int current, int max);
        private LoadStatusDelegate loadStatus;

        public Excel(string path)
        {
            this.path = path;
            app = new MsExcel.Application();
            app.DisplayAlerts = false;
            if (File.Exists(path))
            {
                book = app.Workbooks.Open(path);    // Если файл есть, открываем
            }
            else
            {
                book = app.Workbooks.Add();         // Если файла нет, то добавляем
            }
            selectSheet();
            range = sheet.UsedRange;
        }

        #region Свойства
        /// <summary>
        ///  Фильтр для диалога
        /// </summary>
        static public string Filter = "Excel Files(.xlsx)|*.xlsx|Excel Files(.xls)|*.xls|Excel Files(*.xlsm)|*.xlsm";
        /// <summary>
        /// Файл Excel
        /// </summary>
        public string Path
        {
            get { return path; }
        }
        /// <summary>
        /// Приложение
        /// </summary>
        public MsExcel.Application App
        {
            get { return app;}
        }
        /// <summary>
        /// Книга
        /// </summary>
        public MsExcel.Workbook Book
        {
            get { return book; }
        }
        /// <summary>
        /// Лист Excel
        /// </summary>
        public MsExcel.Worksheet Sheet
        {
            get { return sheet; }
        }
        /// <summary>
        /// Рабочая область
        /// </summary>
        public MsExcel.Range Range
        {
            get { return range; }
        }

        public LoadStatusDelegate LoadStatus
        {
            get { return loadStatus; }
            set { loadStatus = value; }
        }
        #endregion

        #region Основные функции для работы с Excel
        public DataTable readToDataTable()
        {
            DataTable dt = new DataTable();
            // Генерируем колонку
            for (int Cnum = 1; Cnum <= range.Columns.Count; Cnum++)
            {
                DataColumn column = new DataColumn(getCellText(1, Cnum));
                dt.Columns.Add(column);
            }
            // Заносим строки
            for (int Cnum = 1; Cnum <= range.Columns.Count; Cnum++)
            {
                DataRow row = dt.NewRow();
                for(int Rnum = 2; Rnum <= range.Rows.Count; Rnum++)
                {
                    string elem = getCellText(Rnum, Cnum);
                    double delem;
                    if (double.TryParse(elem, out delem))
                    {
                        row[dt.Columns[Cnum]] = delem;
                    }
                }
                dt.Rows.Add(row);
            }
            return dt;
        }
        /// <summary>
        /// Сохранение таблицы в Excel
        /// </summary>
        /// <param name="dt"></param>
        public void saveFromDataTable(DataTable dt)
        {
            sheet.Cells.Clear();
            // Задаем имена колонок
            for (int i = 1; i <= dt.Columns.Count; i++)
            {
                sheet.Cells[1, i] = dt.Columns[i - 1].ColumnName.ToString();
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    sheet.Cells[i + 2, j + 1] = row[j];                    
                }
            }
            book.SaveAs(Path);
        }
        /// <summary>
        /// Чтение файла excel в набор переменных
        /// </summary>
        /// <returns></returns>
        public List<Variable> readToVariable()
        {
            List<Variable> vs = new List<Variable>();
            int max = range.Columns.Count * range.Rows.Count;
            int currentElement = 0;

            for (int Cnum = 1; Cnum <= range.Columns.Count; Cnum++)
            {
                bool readVar = true;
                Variable v = new Variable();
                for (int Rnum = 1; Rnum <= range.Rows.Count; Rnum++)   // Перебираем по столбцам
                {
                    loadStatus?.Invoke(++currentElement, max);
                    if (Rnum == 1)
                    {
                        v.Name = range.Cells[Rnum, Cnum].Text;
                        if (v.Name.Trim() == string.Empty) {
                            readVar = false;
                            currentElement += (range.Rows.Count - Rnum) - 1;
                            break;
                        }
                        continue;
                    }
                    string elem = range.Cells[Rnum, Cnum].Text;
                    double elemDouble;
                    if (double.TryParse(elem.Replace('.',','), out elemDouble))
                    {
                        v.Range.Add(elemDouble);
                    }
                }
                if (readVar)
                {
                    vs.Add(v);
                }
            }
            return vs;
        }
        /// <summary>
        /// Сохранение данных в Excel 
        /// </summary>
        /// <param name="vs"></param>
        public void saveFromVariables(List<Variable> vs)
        {
            sheet.Cells.Clear();
            int max = 0;
            // Задаем имена колонок
            for (int i = 0; i < vs.Count; i++)
            {
                sheet.Cells[1, i + 1] = vs[i].Name.ToString();
                max = Math.Max(max, vs[i].Range.Count);         //Максимальное число элементов
            }
            for (int i = 0; i < max; i++)
            {
                for (int j = 0; j < vs.Count; j++)
                {
                    if (i < vs[j].Range.Count)  // Если заходит в массив
                    {
                        sheet.Cells[i + 2, j + 1] = vs[j].Range[i].ToString();
                    }
                    else
                    {
                        break;
                    }
                }
            }
            book.SaveAs(Path);
        }


        #endregion

        #region Вспомогательные методы
        private void selectSheet(int s = 1)
        {
            try
            {
                sheet = book.Sheets.Item[s];
            }catch
            {
                throw new Exception("Невозможно открыть выбранную книгу");
            }
        }
        private string getCellText(int r,int c)
        {
            return range.Cells[r, c].Text;
        }

        public void Dispose()
        {
            if (book != null)
            {
                book.Save();
                book.Close(false, Type.Missing, Type.Missing);
            }
            if (app != null)
            {
                app.Quit();
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(app);
            }
            GC.Collect();
        }
        #endregion
    }
}
