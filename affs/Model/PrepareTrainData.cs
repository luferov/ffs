﻿using FuzzyLogic;

namespace affs.Model
{
    internal class PrepareTrainData
    {
        private double[][] trainData;           // Исходные данные
        private int n = 3;                      // Шаг нечеткого разбиения
        private double[][] trainDataTrend;      // Обучающая выборка тренда
        private double[][] trainDataResidure;   // Обучающая выборка остаточной составляющей

        public PrepareTrainData(double[][] td, int _n = 3) {
            trainData = td;
            n = _n;
        }

        #region Свойства класса
        /// <summary>
        /// Исходные 
        /// </summary>
        public double[][] TrainData
        {
            get { return trainData; }
        }
        /// <summary>
        /// Шаг нечеткого разбиения
        /// </summary>
        public int N
        {
            get { return n; }
            set { n = value; }
        }
        /// <summary>
        /// Тренд обучения
        /// </summary>
        public double[][] TrainDataTrend
        {
            get { return trainDataTrend; }
        }
        
        public double[][] TrainDataResidure
        {
            get { return trainDataResidure; }
            set { trainDataResidure = value; }
        }

        #endregion
        /// <summary>
        /// Разбиения данных обучения на трендовую и остаточную составляющую
        /// </summary>
        public void fuzzyTransform() {
            double[][] td = transposing(trainData);
            double[][] tdt = new double[td.Length][];
            double[][] tdr = new double[td.Length][];
            for (int i = 0; i< td.Length; i++)
            {
                FuzzyTransform ft = new FuzzyTransform(td[i], N);
                ft.Run();
                tdt[i] = ft.TimeSeriesInverse;
                tdr[i] = ft.TimeSeriesRemainder;
            }
            trainDataTrend = transposing(tdt);
            trainDataResidure = transposing(tdr);
        }


        /// <summary>
        /// Транспонирование матрицы
        /// </summary>
        /// <param name="m">Исходная матрица</param>
        /// <returns>Транспонированная матрица</returns>
        private double[][] transposing(double[][] m)
        {
            double[][] mt = new double[m[0].Length][];
            // Заполняем массив нулями
            for (int i = 0; i < m[0].Length; i++)
            {
                mt[i] = new double[m.Length];
            }
            // Переносим элементы
            for (int i = 0; i < m.Length; i++)
            {
                for (int j = 0; j < m[i].Length; j++)
                {
                    mt[j][i] = m[i][j];
                }
            }
            return mt;
        }

    }
}
