﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Model
{
    public class CSV
    {
        private string path;                    // Путь файла
        private readonly char delimiter;        // Разделитель
        private static readonly string filter = "CSV файлы (*.csv)|*.csv|Все файлы (*.*)|*.*";
        public delegate void LoadStatusDelegate(int current, int max);
        private LoadStatusDelegate loadStatus;
        public CSV(string path, char delimiter = ';')
        {
            this.path = path;
            this.delimiter = delimiter;
        }
        public string Path { get => path; set => path = value; }

        public char Delimiter => delimiter;
        /// <summary>
        /// Делегат загрузки данных
        /// </summary>
        public LoadStatusDelegate LoadStatus { get => loadStatus; set => loadStatus = value; }

        public static string Filter => filter;

        #region Основные функции для работы с файлами
        public List<Variable> ReadToVariable()
        {
            if (!File.Exists(Path))
            {
                throw new Exception("Файл " + Path + " не найден");
            }
            List<Variable> vs = new List<Variable>();
            int currentPosition = 0;
            int max = File.ReadLines(Path).Count();
            StreamReader sr = new StreamReader(Path);
            // Читаем шапку
            string[] header = sr.ReadLine().Split(delimiter);
            loadStatus?.Invoke(++currentPosition, max);
            foreach(string h in header)
            {
                vs.Add(new Variable(h));
            }
            // Читаем данные
            string line;
            while((line = sr.ReadLine()) != null){
                string[] ds = line.Split(delimiter);
                for (int i = 0; i < ds.Length; i++)
                {
                    if (double.TryParse(ds[i].Replace('.',','), out double el))
                    {
                        vs[i].Range.Add(el);
                    }
                }
                loadStatus?.Invoke(++currentPosition, max);
            }

            return vs;
        }

        #endregion
    }
}
