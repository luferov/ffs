﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace affs.Model
{
    /// <summary>
    /// Класс, который готовит данные для модели
    /// </summary>
    public class DataPreparation : ViewModel.ViewModelBase
    {
        private ObservableCollection<Variable> _variables;          // Переменные
        private List<Variable> fvariables;                          // Переменные для прогнозирования
        private Dictionary<Variable, List<int>> _inputRegress;      // Параметры регрессии
        private Dictionary<Variable, List<int>> _outputRegress;     // Параметры регрессии
        private Dictionary<Variable, List<int>> _ir;                // Параметры регрессии
        private Dictionary<Variable, List<int>> _or;                // Параметры регрессии
        private int countInput;                                     // Количество входных переменных
        private int countOutput;                                    // Количество выходных переменных
        private double[][] trainDataInput;                          // Выходные значения обучающей выборки
        private double[][] trainDataOutput;                         // Выходные значения обучающей выборки
        private int startTrainData;                                 // Откуда начинать отбор
        private int trainDataCount;                                 // Количество обучающих выборок
        private int horizont;                                       // Горизонт прогнозирования

        /// <summary>
        /// Нормализованные данные
        /// </summary>
        private Normalization normalization;                        // Класс нормализации
        private double k = 1.2;                                           // Коэффициент нормализации
        private double[][] trainDataNInput;                         // Нормализованные входные значения ОВ
        private double[][] trainDataNOutput;                        // Нормализованные выходные знечения ОВ



        #region Конструкторы
        public DataPreparation()
        {
            Variables = new ObservableCollection<Variable>();
            InputRegress = new Dictionary<Variable, List<int>>();
            OutputRegress = new Dictionary<Variable, List<int>>();
        }
        #endregion
        #region Свойства
        /// <summary>
        /// Параметры авторегрессии модели входных переменных
        /// </summary>
        public Dictionary<Variable, List<int>> InputRegress
        {
            get { return _inputRegress; }
            set {
                _inputRegress = value;
                normalization = null;
                OnProperyChanged("InputRegress");
            }
        }
        /// <summary>
        /// Параметры авторегресии модели выходных переменных
        /// </summary>
        public Dictionary<Variable, List<int>> OutputRegress
        {
            get { return _outputRegress; }
            set {
                _outputRegress = value;
                normalization = null;
                OnProperyChanged("OutputRegress");
            }
        }
        /// <summary>
        /// Горизонт прогнозирования
        /// </summary>
        public int Horizont
        {
            get { return horizont; }
        }
        /// <summary>
        /// Количество элементов обучающей выборки
        /// </summary>
        public int TrainDataCount
        {
            get { return trainDataCount; }
        }
        /// <summary>
        /// Количество входных переменных
        /// </summary>
        public int CountInput
        {
            get { return countInput; }
        }
        /// <summary>
        /// Количество выходных переменных
        /// </summary>
        public int CountOutput
        {
            get { return countOutput; }
        }
        /// <summary>
        /// Входные значения обучающей выборки
        /// </summary>
        public double[][] TrainDataInput
        {
            get { return trainDataInput; }
        }
        /// <summary>
        /// Выходные значения обучающей выборки
        /// </summary>
        public double[][] TrainDataOutput
        {
            get {return trainDataOutput;}
        }
        /// <summary>
        /// Переменные, которые содержаться в модели
        /// </summary>
        public ObservableCollection<Variable> Variables
        {
            get { return _variables; }
            set {
                _variables = value;
                OnProperyChanged("Variables");         
            }
        }
        /// <summary>
        /// Класс нормализации
        /// </summary>
        public Normalization Normalization
        {
            get { return normalization; }
        }
        /// <summary>
        /// Коэффициент нормализации
        /// </summary>
        public double K {
            get { return k; }
            set {
                if (value >= 1)
                {
                    k = value;
                    if (normalization != null)
                    {
                        normalization.setK(value);
                    }
                }
            }
        }
        /// <summary>
        /// Переменные для прогнозирования
        /// </summary>
        public List<Variable> Fvariables
        {
            get { return fvariables; }
        }
        /// <summary>
        /// Нормализованные входные параметры
        /// </summary>
        public double[][] TrainDataNInput
        {
            get { return trainDataNInput;}
        }
        /// <summary>
        /// Нормализованные выходные параметры
        /// </summary>
        public double[][] TrainDataNOutput
        {
            get{ return trainDataNOutput; }
        }
        #endregion

        #region Основной функционал
        /// <summary>
        /// Инициализация модели
        /// </summary>
        public void Init()
        {
            // Клонируем переменные
            fvariables = new List<Variable>();
            foreach (Variable v in Variables)
            {
                fvariables.Add((Variable)v.Clone());
            }
            normalization = new Normalization(fvariables);
            _ir = new Dictionary<Variable, List<int>>();
            _or = new Dictionary<Variable, List<int>>();
            // Перегоняем параметры авторегреасси для входных переменных
            foreach (KeyValuePair<Variable, List<int>> elem in InputRegress)
            {
                _ir.Add(findByName(elem.Key), elem.Value);
            }
            foreach (KeyValuePair<Variable, List<int>> elem in OutputRegress)
            {
                _or.Add(findByName(elem.Key), elem.Value);
            }
        }
        /// <summary>
        /// Определение границ обучающей выборки
        /// </summary>
        public void detectBorder()
        {
            // Если класса нормализации не существует, создаем
            if (normalization == null)
            {
                Init();
            }
            countInput = 0;
            countOutput = 0;
            foreach (Variable v in fvariables)
            {
                if (_ir.ContainsKey(v))
                {
                    countInput += _ir[v].Count;
                }
                if (_or.ContainsKey(v))
                {
                    countOutput += _or[v].Count;
                }
            }
            // Сливаем параметры авторегрессии
            Dictionary<Variable, List<int>> autoRegression = new Dictionary<Variable, List<int>>();
            foreach (Variable v in fvariables)
            {
                List<int> regress = new List<int>();
                // Если есть в входящих переменных
                if (_ir.ContainsKey(v)) {
                    foreach (int ar in _ir[v])
                    {
                        if (!regress.Contains(ar))
                        {
                            regress.Add(ar);
                        }
                    }
                }
                // Если есть в выходящих переменных
                if (_or.ContainsKey(v))
                {
                    foreach (int ar in _or[v])
                    {
                        if (!regress.Contains(ar))
                        {
                            regress.Add(ar);
                        }
                    }
                }
                // Если регрессия есть
                if (regress.Count != 0)
                {
                    autoRegression.Add(v, regress);
                }
            }
            int start = 0;
            int end = 0;
            foreach (KeyValuePair<Variable, List<int>> v in autoRegression) {
                int f = findMin(v.Value);
                if (f <= 0)
                {
                    start = Math.Min(start, f);
                }
                end = Math.Max(end, v.Key.Range.Count);
            }
            int _horizont = end;
            foreach (KeyValuePair<Variable, List<int>> v in autoRegression) {
                int f = findMax(v.Value);
                if (f >= 0)
                {
                    end = Math.Min(end, v.Key.Range.Count - f);
                    // Если нет в выходных данных
                    if (!_or.ContainsKey(v.Key))
                    {
                        _horizont = Math.Min(_horizont, v.Key.Range.Count - f);
                    }
                }
            }
            startTrainData = start;
            trainDataCount = start + end;
            horizont = _horizont - end;
            // Если только авт  орегрессия, то приравниваем горизонт к 1
            if (_ir.Count == 1 && _or.Count == 1)
            {
                if (_ir.Count == _or.Count && _ir.Keys.First() == _or.Keys.First())
                {
                    horizont = -1;
                }
            }
        }
        /// <summary>
        /// Формируем данные для обучения
        /// </summary>
        public void buildTrainData()
        {
            detectBorder();
            trainDataInput = new double[trainDataCount][];
            trainDataOutput = new double[trainDataCount][];
            trainDataNInput = new double[trainDataCount][];
            trainDataNOutput = new double[trainDataCount][];

            int start = Math.Abs(startTrainData);

            // Формируем обучающую выборку
            for (int i = 0; i< trainDataCount; i++)
            {
                int numberColumn = 0;
                trainDataInput[i] = new double[CountInput];
                trainDataNInput[i] = new double[CountInput];
                foreach (KeyValuePair<Variable,List<int>> elem in _ir)
                {
                    for (int j = 0; j < elem.Value.Count; j++)
                    {
                        trainDataInput[i][numberColumn] = normalization.findByName(elem.Key).Range[i + start + elem.Value[j]];
                        trainDataNInput[i][numberColumn] = normalization.findNoramlByName(elem.Key).Range[i + start + elem.Value[j]];
                        numberColumn++;
                    }
                }
            }

            for (int i = 0; i< trainDataCount; i++)
            {
                int numberColumn = 0;
                trainDataOutput[i] = new double[CountOutput];
                trainDataNOutput[i] = new double[CountOutput];
                foreach (KeyValuePair<Variable,List<int>> elem in _or)
                {
                    for (int j = 0; j < elem.Value.Count; j++)
                    {
                        trainDataOutput[i][numberColumn] = normalization.findByName(elem.Key).Range[i + start + elem.Value[j]];
                        trainDataNOutput[i][numberColumn] = normalization.findNoramlByName(elem.Key).Range[i + start + elem.Value[j]];
                        numberColumn++;
                    }
                }
            }
        }

        /// <summary>
        /// Получение данных для следующего прогноза +1
        /// </summary>
        /// <returns></returns>
        public double[] getDataForForecasting()
        {
            double[] dataForForecasting = new double[CountInput];
            int start = Math.Abs(startTrainData) + trainDataCount; // Получаем следующие от обучающей выборки
            int numberVariable = 0;
            // Оборачиваем в скобки
            try
            {
                // Пробегаемся по входным переменным
                // elem.Key -> Variable
                // elem.Value -> List<int>
                foreach (KeyValuePair<Variable, List<int>> elem in InputRegress)
                {
                    for (int j = 0; j < elem.Value.Count; j++)
                    {
                        dataForForecasting[numberVariable] = normalization.findByName(elem.Key).Range[start + elem.Value[j]];
                        numberVariable++;
                    }
                }
            }
            catch
            {
                return null;
            }
            return dataForForecasting;
        }
        /// <summary>
        /// Нормированне данные для прогнозирования
        /// </summary>
        /// <returns></returns>
        public double[] getNDataForForecating()
        {
            double[] dataForForecasting = new double[CountInput];
            int start = Math.Abs(startTrainData) + trainDataCount; // Получаем следующие от обучающей выборки
            int numberVariable = 0;
            // Оборачиваем в скобки
            try
            {
                foreach (KeyValuePair<Variable, List<int>> elem in _ir)
                {
                    for (int j = 0; j < elem.Value.Count; j++)
                    {
                        dataForForecasting[numberVariable] = normalization.findNoramlByName(elem.Key).Range[start + elem.Value[j]];
                        numberVariable++;
                    }
                }
            }
            catch
            {
                return null;
            }
            return dataForForecasting;

        }

        /// <summary>
        /// Добавлениие элемента к переменной
        /// </summary>
        /// <param name="v"></param>
        /// <param name="elem"></param>
        public void addVariableRange(Variable v, double elem)
        {
            if (fvariables.Contains(v))
            {
                normalization.addRange(v, elem);
                // Пересобираем систему
                buildTrainData();
            }
        }
        public void addNVariableRange(Variable v,double elem)
        {
            if (fvariables.Contains(v))
            {
                // Добавляем элемент в конец
                normalization.addNormalRange(normalization.findByName(v), elem);
                buildTrainData();
            }
        }
        /// <summary>
        /// Обнуление нормализации
        /// </summary>
        public void removeNormalization() {
            normalization = null;
        }
        #endregion
        #region Вспомогательные методы
        /// <summary>
        /// Нахождение максимума
        /// </summary>
        /// <param name="ar"></param>
        /// <returns></returns>
        private int findMax(List<int> ar)
        {
            if (ar.Count == 0) return 0;
            int a = ar[0];
            foreach (int _a in ar)
            {
                a = Math.Max(a, _a);
            }
            return a;
        }
        /// <summary>
        /// Нахождение минимума
        /// </summary>
        /// <param name="ar"></param>
        /// <returns></returns>
        private int findMin(List<int> ar)
        {
            if (ar.Count == 0) return 0;
            int a = ar[0];
            foreach (int _a in ar) {
                a = Math.Min(a, _a);
            }
            return a;
        }
        /// <summary>
        /// Получение переменной по имени
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        private Variable findByName(Variable v)
        {
            return fvariables.FirstOrDefault(x => x.Name == v.Name);
        }
        private Variable findByName(string name)
        {
            return fvariables.FirstOrDefault(x => x.Name == name);
        }
        #endregion
        #region Установление записимости
        public string model()
        {
            string t = Properties.Settings.Default.defaultCharDependenct;
            string m = string.Empty;
            // Выполняем перебор по выходным переменным
            if (_outputRegress.Count == 0 || _inputRegress.Count == 0)
            {
                return "Зависимость не задана";
            }
            int i = 0;
            foreach (KeyValuePair<Variable,List<int>> ar in _outputRegress)
            {
                for (int j = 0; j< ar.Value.Count; j++)
                {
                    if (ar.Value[j] > 0)
                    {
                        m += ar.Key.Name + "(" + t + "+" + ar.Value[j].ToString() + ")";
                    } else if (ar.Value[j] == 0)
                    {
                        m += ar.Key.Name + "(" + t  + ")";
                    }
                    else
                    {
                        m += ar.Key.Name + "(" + t + ar.Value[j].ToString() + ")";
                    }
                    if (j != ar.Value.Count - 1)
                    {
                        m += ", ";
                    }
                    
                }
                if (i != _outputRegress.Count - 1)
                {
                    m += ", ";
                }
                i++;
            }
            m += " = f(";
            i = 0;
            foreach (KeyValuePair<Variable, List<int>> ar in _inputRegress)
            {
                for (int j = 0; j < ar.Value.Count; j++)
                {
                    if (ar.Value[j] > 0)
                    {
                        m += ar.Key.Name + "(" + t + "+" + ar.Value[j].ToString() + ")";
                    } else if (ar.Value[j]<0) {
                        m += ar.Key.Name + "(" + t + ar.Value[j].ToString() + ")";
                    } else {
                        m += ar.Key.Name + "(" + t + ")";
                    }
                    if (j != ar.Value.Count - 1)
                    {
                        m += ", ";
                    }

                }
                if (i != _inputRegress.Count - 1)
                {
                    m += ", ";
                }
                i++;
            }
            m += ")";
            return m;
        }
        #endregion
    }
}
