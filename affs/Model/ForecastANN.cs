﻿using affs.Common;
using FANNCSharp;
using FANNCSharp.Double;
using System.Collections.Generic;

namespace affs.Model
{
    /// <summary>
    /// Прогнозирование с помощью искусственных нейронных сетей
    /// Artificial neural networks
    /// Используется бесплатная библиотека FANNCSharp
    /// </summary>
    public class ForecastANN : IForecast
    {
        // Общие параметры модели прогнозирования
        private double[][] trainDataInput;      // Входные данные обучающей выборки
        private double[][] trainDataOutput;     // Выходные дынне обучающей выборки
        private int report = 10;                // Отчет каждых report раз
        private int epochs = 1000;              // Максимальное количество эпох обучения
        private double error = 0.0;             // Требуемая ошибка
        private PrintTrain printTrain;          // Делегат вывода отчета об обучении
        
        // Частные параметры модели прогнозирования
        private TrainingData trainData;         // Класс обучения
        private List<Layer> layers;             // Слои
        private NeuralNet net;                  // Нейронная сеть

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_layers">Число слоев</param>
        /// <param name="_trainInput">Входные данные обучающей выборки</param>
        /// <param name="_trainOutput">Выходные данные обучеющей выборки</param>
        public ForecastANN(){
            layers = new List<Layer>();
        }
        #region Свойства модели
        /// <summary>
        /// Входные данные для обучения
        /// </summary>
        public double[][] TrainDataInput
        {
            get { return trainDataInput; }
            set { trainDataInput = value; }
        }
        /// <summary>
        /// Выходные данные для обучения
        /// </summary>
        public double[][] TrainDataOutput
        {
            get { return trainDataOutput; }
            set { trainDataOutput = value; }
        }
        /// <summary>
        /// Количество входных переменных
        /// </summary>
        public int CountInput
        {
            get { return (int)net.InputCount; }
        }
        /// <summary>
        /// Количество выходных переменых
        /// </summary>
        public int CountOutput
        {
            get { return (int)net.OutputCount; }
        }
        /// <summary>
        /// Максимальное количество эпох обучения
        /// </summary>
        public int Epochs
        {
            get { return epochs; }
            set { epochs = value; }
        }
        /// <summary>
        /// Требуемая ошибка системы
        /// </summary>
        public double Error
        {
            get { return error; }
            set { error = value; }
        }
        /// <summary>
        /// Выводить отчет об обучении между эпохами обучения
        /// </summary>
        public int Report
        {
            get { return report; }
            set { report = value; }
        }
        /// <summary>
        /// Параметры обучения
        /// </summary>
        public PrintTrain PrintTrainReport
        {
            get { return printTrain; }
            set { printTrain = value; }
        }

        public List<Layer> Layers
        {
            get { return layers; }
            set{ layers = value; }
        }
        #endregion
        #region Функционал модели
        /// <summary>
        /// Валидация модели
        /// </summary>
        /// <param name="msg">Сообщение, содержащее ошибки валидации</param>
        /// <returns>Успешная валидация или нет</returns>
        public bool Validate(out string msg)
        {
            msg = string.Empty;
            if (layers.Count < 2)
            {
                msg = "Количество слоев искусственной нейронной сети не может быть меньше двух";
                return false;

            }
            // Параметры обучающей выборки
            if (TrainDataInput.Length == 0 || TrainDataOutput.Length == 0)
            {
                msg = "Входные или выходные данные не указаны";
                return false;
            }
            if (TrainDataInput.Length != TrainDataOutput.Length)
            {
                msg = "Входные и выходные данные обучающей выборки не согласованы";
                return false;
            }
            for (int i = 0; i < TrainDataInput.Length - 1; i++)
            {
                if (TrainDataInput[i].Length != TrainDataInput[i + 1].Length)
                {
                    msg = "Параметры обучающей выборки не согласованы";
                    return false;
                }
            }
            // Количество слоев
            if (Layers.Count < 2)
            {
                msg = "Минимальное количество слоев 2";
                return false;
            }
            return true;
        }
        /// <summary>
        /// Инициализация модели
        /// </summary>
        public void Init()
        {
            List<uint> _layers = new List<uint>();
            foreach (Layer layer in Layers)
            {
                _layers.Add(layer.CountNeural);
            }
            // Инициализация сети
            net = new NeuralNet(NetworkType.LAYER, _layers);
            // Устанавливаем функции активации
            for (int i = 0; i < Layers.Count; i++)
            {
                net.SetActivationFunctionLayer(Layers[i].Af, i);
            }
            trainData = new TrainingData();
            trainData.SetTrainData(TrainDataInput, TrainDataOutput);
            net.InitWeights(trainData);
            net.SetCallback(PrintCallBack, new object[] { });
        }
        /// <summary>
        /// Обучение сети
        /// </summary>
        public void Train()
        {
            net.TrainOnData(trainData, (uint)Epochs, (uint)Report, (uint)Error);
        }
        /// <summary>
        /// Получение результата работы системы прогнозирования
        /// </summary>
        /// <param name="d">Вектор входных данных</param>
        /// <returns></returns>
        public double[] Run(double[] d)
        {
            //  Если входные данные не согласованы
            if (d.Length != CountInput)
            {
                return null;
            }
            return net.Run(d);
        }
        #endregion
        #region CallBack функция обучения
        /// <summary>
        /// Делегат возвращающий параметры обучения для системы
        /// </summary>
        /// <param name="net">Нейронечеткая система</param>
        /// <param name="data">Данные для обучения</param>
        /// <param name="maxEpochs">Максимальное количество эпох</param>
        /// <param name="ebr"></param>
        /// <param name="de"></param>
        /// <param name="ud">Данные пользователя</param>
        /// <returns></returns>
        public int PrintCallBack(NeuralNet net, TrainingData data, uint maxEpochs, uint ebr, float de, uint eps, object ud)
        {
            printTrain?.Invoke(net.MSE, (int)eps, (int)maxEpochs);
            return 0;
        }
        #endregion
    }

    #region Класс слоя искусственной нейронной сети
    public class Layer
    {
        private uint countNeural;           // Количество нейронов
        private ActivationFunction af;      // Функция активации
        private Dictionary<ActivationFunction, string> afd;

        /// <summary>
        /// Количество нейронов
        /// </summary>
        public uint CountNeural
        {
            get { return countNeural; }
            set {
                if (value > 0)
                countNeural = value;
            }
        }
        /// <summary>
        /// Функция активации
        /// </summary>
        public ActivationFunction Af
        {
            get { return af; }
            set { af = value; }
        }
        /// <summary>
        /// Текстовое описание функции принадлежности
        /// </summary>
        public string TextAf
        {
            get {return afd[af];}
        }

        public Layer()
        {
            afd = new Dictionary<ActivationFunction, string>();
            afd.Add(ActivationFunction.SIGMOID, "Сигмоида");
            afd.Add(ActivationFunction.LINEAR, "Линейная");
            afd.Add(ActivationFunction.SIGMOID_STEPWISE, "Шаговая");
            afd.Add(ActivationFunction.SIGMOID_SYMMETRIC, "Тангенс гиперболический");
            CountNeural = 5;
            af = ActivationFunction.SIGMOID;
        }
        /// <summary>
        /// Создание слоя
        /// </summary>
        /// <param name="cn">Колчиество нейронов</param>
        /// <param name="activationFunction">Функция активации</param>
        public Layer(uint cn, ActivationFunction activationFunction)
        {
            CountNeural = cn;
            Af = activationFunction;
        }
    }
    #endregion
}
