﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.ViewModel;
using System.Collections.ObjectModel;

namespace affs.Model
{
    /// <summary>
    /// Переменная ряда
    /// </summary>
    public class Variable : ViewModelBase, ICloneable
    {
        private string name;                        // Имя ряда
        private ObservableCollection<double> range; // Ряд
        private double max;                         // Максимальное значение ряда
        private double min;                         // Минимальное значение ряда
        private double average;                     // Среднее значение

        public Variable()
        {
            range = new ObservableCollection<double>();
        }

        public Variable(string name) : this()
        {
            this.name = name;
        }

        #region Свойства
        /// <summary>
        /// Имя ряда
        /// </summary>
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnProperyChanged("Name");
                }
            }
        }
        /// <summary>
        /// Временной ряд
        /// </summary>
        public ObservableCollection<double> Range
        {
            get { return range; }
            set
            {
                if (range != value) {
                    range = value;
                    OnProperyChanged("Range");
                    upDate();
                }
            }
        }
        
        /// <summary>
        /// Максимальный предел ряда
        /// </summary>
        public double Max
        {
            get { return max; }
            set {
                max = value;
                OnProperyChanged("Max");
            }
        }
        /// <summary>
        /// Минимальный предел ряда
        /// </summary>
        public double Min
        {
            get { return min; }
            set {
                min = value;
                OnProperyChanged("Min");
            }
        }
        /// <summary>
        /// Среднее значение ряда
        /// </summary>
        public double Average
        {
            get { return average; }
            set {
                average = value;
                OnProperyChanged("Average");
            }
        }
        #endregion
        #region Методы
        /// <summary>
        /// Нахождение максимального значения
        /// </summary>
        /// <param name="rs">Ряд</param>
        /// <returns>Максимальные значение</returns>
        private double findMax() {
            if (range.Count == 0)
            {
                return 0;
            }
            double result = range[0];
            foreach (double r in range) {
                result = Math.Max(r,result);
            }
            return result;
        }
        /// <summary>
        /// Нахождение минимального значения
        /// </summary>
        /// <returns></returns>
        private double findMin() {
            if (range.Count == 0)
            {
                return 0;
            }
            double result = range[0];
            foreach (double r in range)
            {
                result = Math.Min(result, r);
            }
            return result;
        }

        private double findAverage()
        {
            if (range.Count == 0)
            {
                return 0;
            }
            double result = 0.0;
            foreach (double r in range)
            {
                result += r;
            }
            return result / range.Count();
        }

        public void upDate()
        {
            Max = findMax();
            Min = findMin();
            Average = findAverage();
            if (Name != null)
            {
                Name = Name.Trim('.');
            }
        }
        #endregion

        #region Интерфейс глубокого клонирования

        public object Clone()
        {
            Variable v = new Variable();
            v.Name = name;
            foreach (double r in range)
            {
                v.range.Add(r);
            }
            v.upDate();
            return v;
        }
        #endregion
    }
}
