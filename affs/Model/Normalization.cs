﻿using System;
using System.Collections.Generic;
using affs.Common;
using System.Collections.ObjectModel;
using System.Linq;

namespace affs.Model
{

    /// <summary>
    /// Нормализация осуществляется по формуле 
    ///           P[i]
    /// Pн = -------------
    ///         Pmax * K
    /// </summary>
    public class Normalization : INormalization
    {
        private double _k = 1.2;                    // Коэффициент нормализации по умолчанию k > 1.0
        private Dictionary<Variable, double> k;     // Коэффициент номрализации для каждой переменной

        // Общие параметры модели
        private List<Variable> nvariables;          // Нормализованные переменные
        private List<Variable> variables;           // Исходные переменные
        private Dictionary<Variable, double> min;  // Минимальные значения переменных
        private Dictionary<Variable, double> max;   // Максимальные значения переменных

        
        /// <summary>
        /// Исходные переменные
        /// </summary>
        /// <param name="vs">Список исходных переменных</param>
        public Normalization(List<Variable> vs)
        {
            variables = vs;
            nvariables = new List<Variable>();
            k = new Dictionary<Variable, double>();
            // Заносим при инициализации значения
            max = new Dictionary<Variable, double>();
            min = new Dictionary<Variable, double>();
            foreach (Variable v in variables)
            {
                max.Add(v, v.Max);
                min.Add(v, v.Min);
                k.Add(v, _k);
            }
            ReductionNormalization();
        }
        /// <summary>
        /// Установление коэффициента нормализации по умолчанию
        /// </summary>
        /// <param name="_k"></param>
        public void setK(double _k) {
            if (_k >= 1.0)
            {
                k.Clear();
                foreach (Variable v in variables)
                {
                    k.Add(v, _k);
                }
                ReductionNormalization();
            }
        }
        /// <summary>
        /// Получение коэффициента нормализации
        /// </summary>
        /// <returns></returns>
        public double getK()
        {
            return _k;
        }
        /// <summary>
        /// Установление коэффициента нормализации
        /// </summary>
        /// <param name="v">Переменная</param>
        /// <param name="_k">Параметр коэффициента</param>
        public void setK(Variable v, double _k)
        {
            if (_k < 1)
            {
                throw new ArgumentOutOfRangeException("Значение коэффициента нормализации должно быть больше 1.");
            }
            if (k.ContainsKey(v))
            {
                k[v] = _k;
            }
            ReductionNormalization();
        }

        /// <summary>
        /// Коэффициенты нормализации
        /// </summary>
        public Dictionary<Variable, double> K
        {
            get { return k; }
            set { k = value; }
        }
        /// <summary>
        /// Нормализованные переменные
        /// </summary>
        public List<Variable> NVariables
        {
            get { return nvariables; }
        }
        /// <summary>
        /// Исходные данные
        /// </summary>
        public List<Variable> Variables
        {
            get { return variables; }
        }
        /// <summary>
        /// Возвращение максимумов
        /// </summary>
        public Dictionary<Variable, double> Max
        {
            get { return max; }
            
        }
        /// <summary>
        /// Возвращение минимумов
        /// </summary>
        public Dictionary<Variable, double> Min
        {
            get { return min; }
        }
        

        /// <summary>
        /// Функция нормализции
        /// </summary>
        public void ReductionNormalization()
        {
            nvariables.Clear();
            foreach (Variable v in variables)
            {
                Variable nv = new Variable(v.Name);
                ObservableCollection<double> vrange = new ObservableCollection<double>();
                foreach (double range in v.Range)
                {
                    vrange.Add(range / (max[v] * k[v]));
                }
                nv.Range = vrange;
                nvariables.Add(nv);
            }
        }

        /// <summary>
        /// Добавление элемента к переменной
        /// </summary>
        /// <param name="v"></param>
        /// <param name="r"></param>
        public void addRange(Variable v, double r)
        {
            if (variables.Contains(v))
            {
                variables.Find(elem => elem == v).Range.Add(r);
                nvariables.Find(elem => elem.Name == v.Name)
                    .Range
                    .Add(r / (max[variables.Find(elem => elem.Name == v.Name)] * k[variables.Find(elem => elem.Name == v.Name)]));
            }
        }

        public void addNormalRange(Variable v, double r)
        {
            if (variables.Contains(v))
            {
                variables.Find(elem => elem.Name == v.Name)
                    .Range
                    .Add(r * max[variables.Find(elem => elem.Name == v.Name)] * k[variables.Find(elem => elem.Name == v.Name)]);
                nvariables.Find(elem => elem.Name == v.Name)
                    .Range
                    .Add(r);
            }
        }
        /// <summary>
        /// Конвертирование нормализованного числа в денормализованное
        /// </summary>
        /// <returns></returns>
        public double getNumberByNormalization(Variable v,double elem)
        {
            return elem * max[variables.Find(el => el.Name == v.Name)] * k[variables.Find(el => el.Name == v.Name)];
        }
        #region Вспомогательные методы
        /// <summary>
        /// Поиск по имени
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <returns>Возвращение переменной</returns>
        public Variable findByName(string name)
        {
            return variables.FirstOrDefault(x => x.Name == name);
        }
        /// <summary>
        /// Поиск по имени переменной
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Variable findByName(Variable v)
        {
            return variables.FirstOrDefault(x => x.Name == v.Name);
        }
        /// <summary>
        /// Поиск нормализованной переменной по имени
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Variable findNoramlByName(Variable v)
        {
            return nvariables.FirstOrDefault(x => x.Name == v.Name);
        }
        /// <summary>
        /// Поиск по имени нормализованной переменной
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Variable findNoramlByName(string name)
        {
            return nvariables.FirstOrDefault(x => x.Name == name);
        }

        #endregion
    }
}
