﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.Common;
using FANNCSharp;
using FANNCSharp.Double;
using FuzzyLogic;

namespace affs.Model
{
    /// <summary>
    /// Прогнозирование с помощью искусственных нейронных сетей
    /// Комбинированный метод прогнозирования на основе декомпозиции временного ряда
    /// Может быть комбинация:
    /// 1 - ANFIS - ANN
    /// 2 - ANFIS - ANFIS
    /// 3 - ANN - ANFIS
    /// 4 - ANN - ANN
    /// </summary>
    public class ForecastCombine : IForecast
    {
        private double[][] trainDataInput;              // Входящие данные обучающей выборки
        private double[][] trainDataOutput;             // Выходящие данные обучающей выборки
        private int epochs = 1000;                      // Количество эпох обучения ИНС
        private double error = 0;                       // Требуемая ошибка
        private int report = 100;                       // Отчет между обучем
        private PrintTrain printTrain;                  // Делегат отчетности обучения функции трендовой составляющей
        private PrintTrain printTrainResidual;          // Делегат отчетности 


        // Свойства присущие ForecastCombine
        private IForecast trendForecast;                // Модель прогнозирования трендовой составляющей
        private IForecast residualForecast;             // Модель прогнозирования остаточной составляющей

        private double[][] forecastData;                // Данные для прогнозирования и выделения трендовой составляющей
        private double[][] startForecastData;           // Данные для выделения тренда изначальные
        private int fuzzyStep = 3;                      // Шаг нечеткого разбиения

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="tf"></param>
        /// <param name="rf"></param>
        /// <param name="tdi"></param>
        /// <param name="tdo"></param>
        public ForecastCombine(IForecast tf, IForecast rf, int fs) {
            trendForecast = tf;
            residualForecast = rf;
            fuzzyStep = fs;
        }

        #region Свойства
        /// <summary>
        /// Входные данные для обучения
        /// </summary>
        public double[][] TrainDataInput
        {
            get { return trainDataInput; }
            set { trainDataInput = value; }
        }
        /// <summary>
        /// Выходные данные для обчения
        /// </summary>
        public double[][] TrainDataOutput
        {
            get { return trainDataOutput; }
            set { trainDataOutput = value; }
        }
        /// <summary>
        /// Количество эпох обучения для нейронной сети
        /// </summary>
        public int Epochs
        {
            get { return epochs; }
            set { epochs = value; }
        }
        /// <summary>
        /// Требуемая ошибка
        /// </summary>
        public double Error
        {
            get { return error; }
            set { error = value; }
        }
        /// <summary>
        /// Отчет между эпохами
        /// </summary>
        public int Report
        {
            get { return report; }
            set { report = value; }
        }
        /// <summary>
        /// Делегат вывода информации об обучении
        /// </summary>
        public PrintTrain PrintTrainReport
        {
            get { return printTrain; }
            set { printTrain = value; }
        }
        /// <summary>
        /// Модель для прогнозирования трендовой составляющей
        /// </summary>
        public IForecast TrendForecast
        {
            get { return trendForecast; }
            set { trendForecast = value; }
        }
        /// <summary>
        /// Модель для прогнозирования остаточной составляющей
        /// </summary>
        public IForecast ResidualForecast
        {
            get { return residualForecast; }
            set { residualForecast = value; }
        }
        /// <summary>
        /// Прогнозные данные для выделения тренда
        /// </summary>
        public double[][] ForecastData
        {
            get { return forecastData; }
            set { forecastData = value; }
        }
        /// <summary>
        /// Количество выходных данных
        /// </summary>
        public int CountInput
        {
            get { return forecastData[0].Length; }
        }
        public int CountOutput {
            get { return 1; }
        }

        public PrintTrain PrintTrainResidual
        {
            get { return printTrainResidual; }
            set { printTrainResidual = value; }
        }
        /// <summary>
        /// Шаг нечеткого разбиения
        /// </summary>
        public int FuzzyStep
        {
            get { return fuzzyStep; }
            set
            {
                if (value > 0)
                {
                    fuzzyStep = value;
                }
            }
        }

        #endregion
        #region Основной функционал
        /// <summary>
        /// Валидация модели
        /// </summary>
        /// <param name="msg">Сообщение, содержащее ошибки валидации</param>
        /// <returns>Успешная валидация или нет</returns>
        public bool Validate(out string msg)
        {
            msg = string.Empty;
            // Параметры обучающей выборки
            if (TrainDataInput.Length == 0 || TrainDataOutput.Length == 0)
            {
                msg = "Входные или выходные данные не указаны";
                return false;
            }
            if (TrainDataInput.Length != TrainDataOutput.Length)
            {
                msg = "Входные и выходные данные обучающей выборки не согласованы";
                return false;
            }
            for (int i = 0; i < TrainDataInput.Length - 1; i++)
            {
                if (TrainDataInput[i].Length != TrainDataInput[i + 1].Length)
                {
                    msg = "Параметры обучающей выборки не согласованы";
                    return false;
                }
            }
            // Валидность моделей

            PrepareTrainData ptdInput = new PrepareTrainData(trainDataInput, FuzzyStep);
            PrepareTrainData ptdOutput = new PrepareTrainData(trainDataOutput, FuzzyStep);
            ptdInput.fuzzyTransform();
            ptdOutput.fuzzyTransform();
            // Загоняем обучающую выборку для трендовой составляющей
            trendForecast.TrainDataInput = ptdInput.TrainDataTrend;
            trendForecast.TrainDataOutput = ptdOutput.TrainDataTrend;
            // Загоняем обучающую выборку для остаточной составляющей
            residualForecast.TrainDataInput = ptdInput.TrainDataResidure;
            residualForecast.TrainDataOutput = ptdOutput.TrainDataResidure;
            string msg1 = string.Empty, msg2 = string.Empty;
            if (!trendForecast.Validate(out msg1) || !residualForecast.Validate(out msg2))
            {
                msg = msg1 + Environment.NewLine + msg2;
                return false;
            }
            return true;
        }
        /// <summary>
        /// Инициализация модели
        /// </summary>
        public void Init()
        {
            forecastData = trainDataInput;  // при инициализации выборка
            startForecastData = forecastData;
            // Процесс инициализации
            trendForecast.Init();           // Инициализируем первую модель
            trendForecast.PrintTrainReport = PrintCallBack;
            residualForecast.Init();        // Инициализируем вторую модель
            residualForecast.PrintTrainReport = PrintCallBackResidule;
        }
        /// <summary>
        /// Обучение модели
        /// </summary>
        public void Train()
        {
            // Обучаем сеть для прогнозирования трендовой составляющей
            Task TrainTrendForecast = new Task(() => {
                trendForecast.Train();
            });
            // Обучаем сеть для прогнозирования остаточной составляющей
            Task TrainResidualForecast = new Task(() => {
                residualForecast.Train();
            });
            TrainTrendForecast.Start();                                 // Стартуем поток обучения тренда
            TrainResidualForecast.Start();                              // Стартуем поток обучения остаточной
            Task.WaitAll(TrainTrendForecast, TrainResidualForecast);    // Ожидаем конец обучения
        }
        /// <summary>
        /// Получение данных модели
        /// </summary>
        /// <param name="d">Входной вектор</param>
        /// <returns>Ответ модели</returns>
        public double[] Run(double[] d)
        {
            Array.Resize(ref forecastData, forecastData.Length + 1);    // Увеличиваем ряд на 1
            forecastData[forecastData.Length - 1] = d;                  // Загоняем ряд в массив
            // Убираем первый элемент
            // Метод зависит от ретроспективных данных, длинна ретроспективных данных
            // При обучении не меняется
            // Удаление первого элемента
            double[][] tmpfd = new double[forecastData.Length - 1][];
            for (int i = 0; i < tmpfd.Length; i++)
            {
                tmpfd[i] = forecastData[i + 1];
            }
            forecastData = tmpfd;

            // Выделяем трендовую составляющую и остаточную
            PrepareTrainData ptd = new PrepareTrainData(forecastData, FuzzyStep);
            ptd.fuzzyTransform();
            // Забираем последние данные
            double[] trendData = ptd.TrainDataTrend[ptd.TrainDataTrend.Length - 1];
            double[] residureData = ptd.TrainDataResidure[ptd.TrainDataResidure.Length - 1];
            Task<double> ft = new Task<double>(()=> {
                double[] forecastTrend = trendForecast.Run(trendData);
                return forecastTrend == null ? 0.0 : forecastTrend[0];
            });
            Task<double> fr = new Task<double>(()=> {
                double[] forecastResidure = residualForecast.Run(residureData);
                return forecastResidure == null ? 0.0 : forecastResidure[0];
            });
            ft.Start();
            fr.Start();

            Task.WaitAll(ft,fr);
            double result = ft.Result + fr.Result;
            return new double[1] { result };
        }


        #endregion
        #region CallBack функции показывающие данные обучения
        /// <summary>
        /// Функция обучения
        /// </summary>
        /// <param name="_error"></param>
        /// <param name="_epoch"></param>
        /// <param name="_epochs"></param>
        private void PrintCallBack(double _error, int _epoch, int _epochs)
        {
            printTrain?.Invoke(_error, _epoch, _epochs);
        }
        /// <summary>
        /// Функция обучения
        /// </summary>
        /// <param name="_error"></param>
        /// <param name="_epoch"></param>
        /// <param name="_epochs"></param>
        private void PrintCallBackResidule(double _error, int _epoch, int _epochs)
        {
            printTrainResidual?.Invoke(_error, _epoch, _epochs);
        }

        #endregion
        #region Вспомогательные методы
        /// <summary>
        /// Сброс данных для выделения трендовой составляющей
        /// </summary>
        public void ResetForecastData()
        {
            forecastData = startForecastData;
        }
        /// <summary>
        /// Транспонирование матрицы
        /// </summary>
        /// <param name="m">Исходная матрица</param>
        /// <returns>Транспонированная матрица</returns>
        private double[][] transposing(double[][] m)
        {
            double[][] mt = new double[m[0].Length][];
            // Заполняем массив нулями
            for (int i = 0; i < m[0].Length; i++)
            {
                mt[i] = new double[m.Length];
            }
            // Переносим элементы
            for (int i = 0; i < m.Length; i++)
            {
                for (int j = 0; j < m[i].Length; j++)
                {
                    mt[j][i] = m[i][j];
                }
            }
            return mt;
        }
        #endregion

    }

}
