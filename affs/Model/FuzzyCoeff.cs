﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.ViewModel;
using FuzzyLogic;

namespace affs.Model
{
    public class FuzzyCoeff : ViewModelBase
    {
        private FuzzyVariable fuzzyVariable;
        private double _value;

        public FuzzyCoeff() { }
        public FuzzyCoeff(FuzzyVariable fv)
        {
            fuzzyVariable = fv;
            _value = 0.0;
        }
        public FuzzyCoeff(FuzzyVariable fv, double _value)
        {
            fuzzyVariable = fv;
            Value = _value;
        }

        public double Value { get => _value; set { _value = value; OnProperyChanged("Value"); } }
        public FuzzyVariable FuzzyVariable { get => fuzzyVariable; set { fuzzyVariable = value; OnProperyChanged("FuzzyVariable"); } }
        public string Name => FuzzyVariable.Name;
    }
}
