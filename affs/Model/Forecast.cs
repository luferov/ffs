﻿using affs.Common;
using System.Collections.ObjectModel;

namespace affs.Model
{
    /// <summary>
    /// Модель прогнозирования
    /// </summary>
    public class Forecast : ViewModel.ViewModelBase
    {
        private string name;                                        // Имя прогнозной модели
        private ObservableCollection<Variable> forecastVariables;   // Переменные прогнозные модели
        private IForecast forecastMethod;                           // Способ прогнозирования временных рядов
        private DataPreparation dataPreparation;                    // Подготовка данных
        private bool useNoralization = true;                               // Используем нормализацию или нет
        private double k = 1.2;                                     // Коэффициент нормализации
        private double[] errorTrain;                 // Ошибка обучения
        private bool isTrain = false;                               // Обучена ли модель


        public Forecast(string _name, IForecast fm) {
            name = _name;
            forecastMethod = fm;
            forecastVariables = new ObservableCollection<Variable>();
            dataPreparation = new DataPreparation();
            errorTrain = new double[] { 0.0 };
        }

        #region Свойства
        /// <summary>
        /// Имя модели
        /// </summary>
        public string Name
        {
            get { return name; }
            set {
                name = value;
                OnProperyChanged("Name");
            }
        }
        /// <summary>
        /// Переменные прогноза
        /// </summary>
        public ObservableCollection<Variable> ForecastVariables
        {
            get { return forecastVariables; }
            set {
                forecastVariables = value;
                OnProperyChanged("ForecastVariables");
            }
        }
        /// <summary>
        /// Способ прогнозирования
        /// </summary>
        public IForecast ForecastMethod
        {
            get { return forecastMethod; }
            set {
                forecastMethod = value;
                OnProperyChanged("ForecastMethod");
            }
        }
        /// <summary>
        /// Подготовка данных к прогнозированию
        /// </summary>
        public DataPreparation DataPreparation
        {
            get { return dataPreparation; }
            set {
                dataPreparation = value;
                OnProperyChanged("DataPreparation");
            }
        }
        /// <summary>
        /// Показавыет обучена ли модель
        /// </summary>
        public bool IsTrain
        {
            get { return isTrain; }
            set {
                isTrain = value;
                OnProperyChanged("IsTrain");
            }
        }
        /// <summary>
        /// Горизонт прогнозирования
        /// 0 - прогноз невозможен или закончен
        /// -1 - прогноз может идти до беспонечности
        /// n = [1,N] прогноз возможен до N элементов
        /// </summary>
        public int Horizont {
            get { return dataPreparation.Horizont; }
        }
        /// <summary>
        /// Прогнозная зависимость
        /// </summary>
        public string DependencyModel {
            get { return dataPreparation != null ? dataPreparation.model() : null; }
        }
        /// <summary>
        /// Ошибка обучения
        /// </summary>
        public double[] ErrorTrain
        {
            get { return errorTrain; }
            set {
                errorTrain = value;
                OnProperyChanged("ErrorTrain");
                OnProperyChanged("Error");
            }
        }
        public double Error {
            get  {
                double e = 0.0;
                foreach(double er in ErrorTrain)
                {
                    e += er;
                }
                return e / ErrorTrain.Length;
            }
        }
        /// <summary>
        /// Коэффициент нормализации
        /// </summary>
        public double K
        {
            get { return k; }
            set {
                if (value >= 1)
                {
                    k = value;
                    OnProperyChanged("K");
                }
            }
        }
        /// <summary>
        /// Используем ли нормализацию
        /// </summary>
        public bool UseNoralization
        {
            get { return useNoralization; }
            set {
                useNoralization = value;
                OnProperyChanged("UseNoralization");
            }
        }

        #endregion
    }
}
