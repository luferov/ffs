﻿using System;
using affs.Common;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Model
{
    public class FuzzyCognitiveMap : ViewModel.ViewModelBase, IMultiForecast
    {
        private double thresholdCorrelation = 0.9;
        private Variable variable;
        private Variable compare;
        private Dictionary<IMultiForecast, List<int>> depts;
        private Forecast forecastModel;

        #region Свойства
        public double K { get => ForecastModel.K; set { ForecastModel.K = value; OnProperyChanged(); } }
        /// <summary>
        /// Порог для определения
        /// </summary>
        public double ThresholdCorrelation { get => thresholdCorrelation; set { thresholdCorrelation = value; OnProperyChanged(); } }
        /// <summary>
        /// Переменная, которую прогнозируем
        /// </summary>
        public Variable Variable { get => variable; set { variable = value; OnProperyChanged(); } }
        /// <summary>
        /// Переменная с которой сравниваем
        /// </summary>
        public Variable Compare { get => compare; set { compare = value; OnProperyChanged(); OnProperyChanged("CompareName"); } }
        public string CompareName => Compare == null ? "Соотношение не указано" : Compare.Name;
        /// <summary>
        /// Класс подготовки данных для прогнозирования
        /// </summary>
        public DataPreparation DataPreparation { get => ForecastModel.DataPreparation; set { ForecastModel.DataPreparation = value; OnProperyChanged(); } }
        /// <summary>
        /// Обучена ли модель
        /// </summary>
        public bool IsTrain => ForecastModel.IsTrain;
        public string IsTrainText => ForecastModel.IsTrain ? "Модель обучена" : "Модель не обучена";
        /// <summary>
        /// Зависимость по переменных
        /// </summary>
        public Dictionary<IMultiForecast, List<int>> Depts { get => depts; set { depts = value; OnProperyChanged(); OnProperyChanged("DependencyModel"); } }
        /// <summary>
        /// Модель функции в строку
        /// </summary>
        public string DependencyModel
        {
            get
            {
                string mdl = string.Empty;
                int i = 0;
                foreach (KeyValuePair<IMultiForecast, List<int>> ar in Depts)
                {
                    for (int j = 0; j < ar.Value.Count; j++)
                    {
                        if (ar.Value[j] == 0)
                        {
                            mdl += string.Format("{0}(t)", ar.Key.Variable.Name);
                        }
                        else if (ar.Value[j] < 0)
                        {
                            mdl += string.Format("{0}(t{1})", ar.Key.Variable.Name, ar.Value[j].ToString());
                        }
                        if (j != ar.Value.Count - 1)
                        {
                            mdl += ", ";
                        }
                    }
                    if (i != Depts.Count - 1)
                    {
                        mdl += ", ";
                    }
                    i++;
                }
                return string.Format("{0}(t+1) = f({1})", Variable.Name, mdl);
            }
        }
        /// <summary>
        /// Ошибка обучения модели
        /// </summary>
        public double[] ErrorTrain { get => ForecastModel.ErrorTrain; set { ForecastModel.ErrorTrain = value; OnProperyChanged(); OnProperyChanged("Error");  } }
        public double Error {
            get {
                if (ErrorTrain == null)
                {
                    return 0;
                }
                double avgError = 0.0;
                foreach (double er in ErrorTrain)
                {
                    avgError += er;
                }
                return avgError / ErrorTrain.Length;
            }
        }
        /// <summary>
        /// Горизонт возможного прогнозирования
        /// </summary>
        public int Horizont => ForecastModel.Horizont;
        /// <summary>
        /// Модель прогнозироуния
        /// </summary>
        public Forecast ForecastModel { get => forecastModel; set { forecastModel = value; OnProperyChanged(); } }
        #endregion


        public FuzzyCognitiveMap(Variable variable, Forecast forecastModel, double thresholdCorrelation, Variable compare = null)
        {
            this.variable = variable;
            this.compare = compare;
            this.thresholdCorrelation = thresholdCorrelation;
            this.forecastModel = forecastModel;
            depts = new Dictionary<IMultiForecast, List<int>>();
        }
        /// <summary>
        /// Инициализация модели
        /// </summary>
        public void Init()
        {
            ForecastModel.ForecastMethod.Init();
        }
        /// <summary>
        /// Прогнозируем на единицу вперед
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public double Run(Dictionary<Variable, double> input)
        {
            return 0;
            
        }
        /// <summary>
        /// Обучаем модель
        /// </summary>
        public void Train()
        {
            Init();
        }

        public bool Validate(out string msg)
        {
            msg = "";
            return false;
        }
    }
}
