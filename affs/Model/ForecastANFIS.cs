﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using affs.Common;
using FuzzyLogic;

namespace affs.Model
{
    /// <summary>
    /// Прогнозирование на основе нечетких временных рядоа
    /// Adaptive neuro-fuzzy inference system
    /// Используется библиотека FuzzyLogic
    /// Для формирования нечетких правил используется горная кластеризация
    /// </summary>
    public class ForecastANFIS : IForecast
    {
        // Свойства присущие модели
        private double[][] trainDataInput;      // Входная выборка обучения
        private double[][] trainDataOutput;     // Выходная выборка обучения
        private int epochs = 3;                // Эпохи
        private double error = 0;               // Ошибка обучения
        private int report = 1;                 // Отчет между эпохами
        private PrintTrain printTrain;          // Делегат вывода отчет об обучения

        // Свойства присущие ANFIS
        private Anfis anfis;                    // Нейронечеткая система прогнозирования
        private double[][] xin;                 // Входные данные обучающей выборки
        private double[] xout;                  // Выходные данные обучающей выборки
        private double nu = 0.1;                // Коэффициент при обучении
        private double nuStep = 0.9;            // Коэффициент изменения nu
        OrMethod orMethod = OrMethod.Max;       // Метод "или"
        AndMethod andMethod = AndMethod.Production;    // Метод "и"
       
        // Параметры кластеризации
        private double radii = 0.95;             // Радиус кластеров
        private double sqshFactor = 1.25;       // Коэффициент подавления
        private double acceptRatio = 0.5;       // Коэффициент принятия
        private double rejectRatio = 0.35;      // Коэффициент отторжения
        
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_radii">Радиус кластеров</param>
        /// <param name="_trainInput">Параметры входа обучающей выборки</param>
        /// <param name="_trainOutput">Параметры выхода обучающей выборки</param>
        public ForecastANFIS(double _radii)
        {
            radii = _radii;
        }

        #region Свойства модели
        /// <summary>
        /// Входные данные обучающей выборки
        /// </summary>
        public double[][] TrainDataInput
        {
            get { return trainDataInput; }
            set { trainDataInput = value; }
        }
        /// <summary>
        /// Выходные данне обучающей выборки
        /// </summary>
        public double[][] TrainDataOutput
        {
            get { return trainDataOutput; }
            set { trainDataOutput = value; }
        }
        /// <summary>
        /// Количество эпох обучения
        /// </summary>
        public int Epochs
        {
            get { return epochs; }
            set { epochs = value; }
        }
        /// <summary>
        /// Отчет между обучением
        /// </summary>
        public int Report
        {
            get { return report; }
            set { report = value; }
        }
        /// <summary>
        /// Требуемая ошибка обучения
        /// </summary>
        public double Error
        {
            get {  return error;  }
            set  { error = value;  }
        }
        /// <summary>
        /// Количество входных переменных
        /// </summary>
        public int CountInput {
            get { return Anfis.CountInput; }
        }
        /// <summary>
        /// Количество выходных переменных
        /// </summary>
        public int CountOutput
        {
            get { return Anfis.CountOutput; }
        }
        /// <summary>
        /// Параметр обучения nu
        /// </summary>
        public double Nu
        {
            get { return nu; }
            set { nu = value; }
        }
        /// <summary>
        /// Коэффициент изменения nu
        /// </summary>
        public double NuStep
        {
            get { return nuStep; }
            set { nuStep = value; }
        }
        /// <summary>
        /// Or метод нейронечеткой системы прогнозирования
        /// </summary>
        public OrMethod OrMethod
        {
            get { return orMethod; }
            set { orMethod = value; }
        }
        /// <summary>
        /// And метод нейронечеткой системы прогнозирования
        /// </summary>
        public AndMethod AndMethod
        {
            get { return andMethod; }
            set { andMethod = value; }
        }
        /// <summary>
        /// Радиус кластера
        /// </summary>
        public double Radii
        {
            get { return radii; }
            set { radii = value; }
        }
        /// <summary>
        /// Коэффициент подавления
        /// </summary>
        public double SqshFactor
        {
            get { return sqshFactor; }
            set { sqshFactor = value; }
        }
        /// <summary>
        /// Коэффициент принятия
        /// </summary>
        public double AcceptRatio
        {
            get { return acceptRatio; }
            set { acceptRatio = value; }
        }
        /// <summary>
        /// Коэффициент отторжения
        /// </summary>
        public double RejectRatio
        {
            get { return rejectRatio; }
            set { rejectRatio = value; }
        }
        /// <summary>
        /// Делегат отчета об обучении
        /// </summary>
        public PrintTrain PrintTrainReport
        {
            get { return printTrain; }
            set { printTrain = value; }
        }
        /// <summary>
        /// Анфис модель
        /// </summary>
        public Anfis Anfis
        {
            get { return anfis; }
        }
        #endregion
        #region Функционал модели
        /// <summary>
        /// Валидация модели
        /// </summary>
        /// <param name="msg">Сообщение, содержащее ошибки валидации</param>
        /// <returns>Успешная валидация или нет</returns>
        public bool Validate(out string msg)
        {
            msg = string.Empty;
            if (TrainDataInput.Length == 0 || TrainDataOutput.Length == 0)
            {
                msg = "Входные или выходные данные не указаны";
                return false;
            }
            // Параметры обучающей выборки
            if (TrainDataInput.Length != TrainDataOutput.Length)
            {
                msg = "Входные и выходные данные обучающей выборки не согласованы";
                return false;
            }
            for (int i = 0; i < TrainDataInput.Length - 1; i++)
            {
                if (TrainDataInput[i].Length != TrainDataInput[i + 1].Length)
                {
                    msg = "Параметры обучающей выборки не согласованы";
                    return false;
                }
            }
            xin = transposing(trainDataInput);
            xout = new double[xin[0].Length];
            for (int i = 0; i < xout.Length; i++)
            {
                xout[i] = trainDataOutput[i][0];
            }
            if (Radii < 0 || Radii > 1)
            {
                msg = "Радиус кластеров находится в диапазоне [0,1].";
                return false;
            }
            return true;
        }
        /// <summary>
        /// Инициализация модели
        /// </summary>
        public void Init()
        {
            anfis = new Anfis(xin, xout, Radii, SqshFactor, AcceptRatio, RejectRatio)
            {
                Epochs = Epochs,
                Error = Error,
                AndMethod = andMethod,
                OrMethod = orMethod,
                Nu = Nu,
                NuStep = NuStep
            };
        }
        /// <summary>
        /// Обучение модели
        /// </summary>
        public void Train() {
            Anfis.Train(PrintCallBack);

        }
        public double[] Run(double[] d) {
            if (d.Length != CountInput)
            {
                return null;
            }
            return new double[1] { Anfis.Calculate(d) };
        }


        #endregion
        #region CallBack функция обучения
        /// <summary>
        /// Функция обратного вызова обучения системы
        /// </summary>
        /// <param name="_epoch">Текущая эпоха</param>
        /// <param name="_epochs">Количество эпох</param>
        /// <param name="_nu">Коэффициент обучения</param>
        /// <param name="_error">Ошибка на этапе обучения</param>
        private void PrintCallBack(int _epoch, int _epochs, double _nu, double _error)
        {
            printTrain?.Invoke(_error, _epoch, _epochs);
        }
        #endregion
        #region Вспомогательные методы
        private double[][] transposing(double[][] m)
        {

            double[][] mt = new double[m[0].Length][];
            // Заполняем массив нулями
            for (int i = 0; i < m[0].Length; i++)
            {
                mt[i] = new double[m.Length];
            }
            // Переносим элементы
            for (int i = 0; i < m.Length; i++)
            {
                for (int j = 0; j < m[i].Length; j++)
                {
                    mt[j][i] = m[i][j];
                }
            }
            return mt;
            
        }
        #endregion
    }
}
