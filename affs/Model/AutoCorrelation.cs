﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Model
{
    /// <summary>
    /// Вычисление автокорреляции переменной
    /// </summary>
    public class AutoCorrelation
    {
        private string name;        // Имя переменной
        private double[] x;         // Ряд переменной
        private int depth;          // Глубина авторегрессии
        private double r;           // Коэффициент автокорреляции -1 <= r <= 1

        /// <summary>
        /// Имя переменной
        /// </summary>
        public string Name
        {
            get { return name; }
        }
        /// <summary>
        /// Ряд
        /// </summary>
        public double[] X
        {
            get { return x; }
            set { x = value; }
        }
        /// <summary>
        /// Глубина автокорреляции
        /// </summary>
        public int Depth
        {
            get { return depth; }
        }
        /// <summary>
        /// Коэффициент авторегрессии
        /// </summary>
        public double R
        {
            get { return r; }
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="name">Имя переменной</param>
        /// <param name="_x">Ряд</param>
        /// <param name="depth">Глубина автокорреляции</param>
        public AutoCorrelation(string _name, double[] _x, int _depth)
        {
            //
            // Подготовка переменных
            //
            name = _name;
            depth = _depth;
            x = _x;
            double[] k = new double[_x.Length - depth];
            double[] y = new double[_x.Length - depth];
            for (int i = 0; i< k.Length; i++)
            {
                k[i] = _x[i];
                y[i] = _x[i + depth];
            }

            double y1Sred = 0;
            double y2Sred = 0;
            for (int i = depth; i < k.Length; i++)
            {
                y1Sred += k[i];
                y2Sred += y[i];
            }
            y1Sred /= k.Length - depth;
            y2Sred /= y.Length - depth;

            //
            // Возможно распаралеливание потоков
            //
            /*double chislitel = 0;
            double znamenatel1 = 0;
            double znamenatel2 = 0;*/
            Task<double> chislitelTask = new Task<double>(() => {
                double c = 0;
                for (int i = depth; i < k.Length; i++)
                {
                    c += (k[i] - y1Sred) * (y[i] - y2Sred);
                }
                return c;
            });
            Task<double> znamenatelTask = new Task<double>(()=> {
                double z1 = 0;
                double z2 = 0;
                for (int i = depth; i < k.Length; i++)
                {
                    z1 += Math.Pow(k[i] - y1Sred, 2);
                    z2 += Math.Pow(y[i] - y2Sred, 2);
                }

                return Math.Sqrt(z1 * z2);
            });
            
            chislitelTask.Start();
            znamenatelTask.Start();
            Task.WaitAll(chislitelTask, znamenatelTask);
            r = chislitelTask.Result / znamenatelTask.Result;

        }
    }
}
