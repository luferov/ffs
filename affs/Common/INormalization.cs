﻿using affs.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Common
{
    /// <summary>
    /// Интерфейс нормализации данных
    /// Потведение чисел в диапазон от 0 до 1
    /// При установки переменных, выбираются параметры модели
    /// Происходит нормализация данных, осуществляются действия
    /// Новые данные заходят в нормализованные значения и 
    /// выполняется обратная операция денормализации
    /// Параметры модели для нормализации описаны в конкретной модели
    /// </summary>
    public interface INormalization
    {
        /// <summary>
        /// Максимальные значения в переменных
        /// </summary>
        Dictionary<Variable, double> Max { get; }
        /// <summary>
        /// Минимальные значения в переменных
        /// </summary>
        Dictionary<Variable, double> Min { get; }
        /// <summary>
        /// Переменные
        /// </summary>
        List<Variable> Variables { get; }
        /// <summary>
        /// Нормализованные данные
        /// </summary>
        List<Variable> NVariables { get; }
        /// <summary>
        /// Функция нормализации ряда
        /// </summary>
        void ReductionNormalization();
        /// <summary>
        /// Добавление нормального значения к переменной
        /// </summary>
        /// <param name="v"></param>
        /// <param name="r"></param>
        void addRange(Variable v, double r);
        /// <summary>
        /// Добавление нормализованного метода
        /// </summary>
        /// <param name="v"></param>
        /// <param name="r"></param>
        void addNormalRange(Variable v, double r);
        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Variable findByName(string name);
        /// <summary>
        /// Поиск нормализованной переменной по имени
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Variable findNoramlByName(string name);
    }
}
