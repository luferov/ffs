﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace affs.Common
{

    /// <summary>
    /// Делегат, возвращающий параметры обучения системы
    /// </summary>
    /// <param name="error">Ошибка MSE</param>
    /// <param name="epoch">Эпоха</param>
    /// <param name="maxEpochs">Максимальное количество эпох</param>
    public delegate void PrintTrain(double error, int epoch, int maxEpochs);

    /// <summary>
    /// Интерфейс прогнозной модели
    /// </summary>
    public interface IForecast
    {
        #region Свойства
        /// <summary>
        /// Входные данны для обучения
        /// </summary>
        double[][] TrainDataInput { get; set; }
        /// <summary>
        /// Выходные данные для обучения
        /// </summary>
        double[][] TrainDataOutput { get; set; }
        /// <summary>
        /// Количество выходов
        /// </summary>
        int CountInput { get; }
        /// <summary>
        /// Количество входов
        /// </summary>
        int CountOutput { get; }
        /// <summary>
        /// Количество эпох обучения
        /// </summary>
        int Epochs { get; set; }
        /// <summary>
        /// Требуемая ошибка обучения
        /// </summary>
        double Error { get; set; }
        /// <summary>
        /// Отчет между эпохами
        /// </summary>
        int Report { get; set; }
        #endregion
        #region Методы
        /// <summary>
        /// Валидация модели
        /// </summary>
        /// <param name="msg">Выходное сообщение</param>
        /// <returns></returns>
        bool Validate(out string msg);
        /// <summary>
        /// Инициализация модели
        /// </summary>
        void Init();
        /// <summary>
        /// Обучение модели
        /// </summary>
        void Train();
        /// <summary>
        /// Получение данных модели
        /// </summary>
        /// <param name="d">Входной вектор</param>
        /// <returns>Выходной вектор</returns>
        double[] Run(double[] d);
        #endregion
        /// <summary>
        /// Отчет об обучении
        /// </summary>
        PrintTrain PrintTrainReport { get; set; }
    }

}
