﻿using affs.Model;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace affs.Common
{
    public interface IMultiForecast
    {
        #region Свойства
        /// <summary>
        /// Коэффициент нормализации, по умолчанию берем 1.20
        /// </summary>
        double K { get; set; }
        /// <summary>
        /// Порог корреляции
        /// </summary>
        double ThresholdCorrelation { get; set; }
        /// <summary>
        /// Переменные, которые участвуют в прогнозе, по умолчанию 0.95
        /// </summary>
        Variable Variable { get; set; }
        /// <summary>
        /// Переменная для сравнения
        /// </summary>
        Variable Compare { get; set; }
        /// <summary>
        /// Подготовка данных
        /// </summary>
        DataPreparation DataPreparation { get; set; }
        /// <summary>
        /// Индикатор обученности сети
        /// </summary>
        bool IsTrain { get; }
        /// <summary>
        /// Глубины переменных для каждой переменной модели переменной
        /// </summary>
        Dictionary<IMultiForecast, List<int>> Depts { get; set; }
        string DependencyModel { get; }
        /// <summary>
        /// Ошибка обчения по каждой модели
        /// </summary>
        double[] ErrorTrain { get; }
        /// <summary>
        /// Модели прогнозирования для каждой переменной
        /// </summary>
        Forecast ForecastModel { get; set; }

        #endregion
        #region Методы
        /// <summary>
        /// Валидация модели
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        bool Validate(out string msg);
        /// <summary>
        /// Иницииализация модели
        /// </summary>
        void Init();
        /// <summary>
        /// Обучене модел
        /// </summary>
        void Train();
        /// <summary>
        /// Выполняем прогнозироване на основе модели
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        double Run(Dictionary<Variable, double> input);
        #endregion
        //PrintTrain PrintTrainReport { get; set; }

    }
}
